/**
 * 
 */
package com.healthqx.AutomationFrame;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.BudgetsReconciliationMainPage;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.HealthQX.PartnerProfileManagementPage;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;

/**
 * @author enebo4f
 *
 */
public class BudgetsReconciliationContractRanking {
	WebDriver driver;
	Environment env;
	BrowserTypes  browser;
	
	DataBaseConnector dbConnector;
	
	
	@BeforeSuite
	@Parameters({"browser","environment"})
	public void beforeTest(String browserType,String environment) {
		
		ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
		
		
		Login signIn = new Login(driver);
		signIn.loginToApp(env.username(), env.password());
		
		//creating DB Conection
		dbConnector= new DataBaseConnector(env.dbEnv(),env.dbUrlDqx());
		dbConnector.openConnection();
		
		
	}
	
	@Test(priority=1,description="Navigate to Budgets and Recon page ")
	public void navigateToBudgetsReconciliation() {
		NavigationTiles navigation = new NavigationTiles(driver);
		NavigationHeader header = new NavigationHeader(driver);
		navigation.navigateToBudgetsReconciliation();
		String expBreadCrumbText= header.getMainModuleBreadCrumb().getText();
		Assert.assertTrue(expBreadCrumbText.contains("Budgets & Reconciliation"));
	}
	
	@Test(priority=2,description="verify user can navigate to PPM Screen upon clicking on Create new Contract")
	public void navigateToPPMScreen() {
		BudgetsReconciliationMainPage budgetAndRecon = new BudgetsReconciliationMainPage(driver);
		PartnerProfileManagementPage ppm = new PartnerProfileManagementPage(driver);
		budgetAndRecon.getCreateNewContractLink().click();
		String title = ppm.getPpmScreenTitle().getText();
		Assert.assertTrue(title.contains("Partner Profile Manager"));
	}
	
	@Test(priority=3,description="Create a new contract")
	public void generalInformation() {
		
		PartnerProfileManagementPage ppm = new PartnerProfileManagementPage(driver);
		String practiceName = "StJohn";
		ppm.getPracticeNameTINInputBox().click();
		ppm.getPracticeNameTINInputBox().sendKeys(practiceName);
	}
	

}
