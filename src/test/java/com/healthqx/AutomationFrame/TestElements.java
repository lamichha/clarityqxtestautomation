/**
 * 
 */
package com.healthqx.AutomationFrame;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityFilter;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPage;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;

/**
 * @author Ashish L.
 *         This class contains test method to start up a browser and test a
 *         valid login
 */
public class TestElements {

	WebDriver driver;

	@BeforeSuite
	public void openApp() {
		// This will launch browser and open application
		driver = StartUpBrowser.StartBrowser("ie", "https://horizon-qa.clarityqx.com"); 
		//driver = StartUpBrowser.StartBrowser("ie", "https://duke-uat.clarityqx.com");
		Login signIn = new Login(driver);
		signIn.loginToApp("ashish.lamichhane@mckesson.com", "Mom01@momq");
	}

	@Test(priority = 1)
	public void navigateToEpisodeOpportunity() {
		NavigationTiles navigate = new NavigationTiles(driver);
		navigate.getEpisodeOpportunityTile().click();
		
	}

	@Test(priority = 2)
	public void verifyBreadCrumb() {
		
		NavigationHeader navigation = new NavigationHeader(driver);
		String homeText = navigation.getHomeBreadCrumb().getText();
		String strategicInsightsText = navigation.getMainModuleBreadCrumb().getText();
		String episodeOpportunityText = navigation.getSubModuleBreadCrumb().getText();

		String ActBreadCrumbText = homeText + strategicInsightsText + episodeOpportunityText;
		String ExpBreadCrumbText = "HomeStrategic InsightsEpisode Opportunity";

		Assert.assertEquals(ActBreadCrumbText, ExpBreadCrumbText);
	}

	@Test(priority = 3)
	public void verifyStudyPeriod() {
		//EpisodeOpportunityMainPage EpisodeOpportunity = new EpisodeOpportunityMainPage(driver);
		NavigationHeader navigation = new NavigationHeader(driver);
		Assert.assertEquals(true, navigation.getStudyPeriodCorporateEntity().isDisplayed());
		Assert.assertEquals(true, navigation.getStudyPeriodTitle().isDisplayed());
	}

	@Test(priority = 4)
	public void verifyQuickLinks() {
		//EpisodeOpportunityMainPage EpisodeOpportunity = new EpisodeOpportunityMainPage(driver);
		NavigationHeader navigation = new NavigationHeader(driver);
		Assert.assertEquals(true, navigation.getBarChartQuickLink().isDisplayed());
		Assert.assertEquals(true, navigation.getTableQuickLink().isDisplayed());
		Assert.assertEquals(true, navigation.getExpandQuickLink().isDisplayed());
	}

	@Test(priority = 5)
	public void verifyChronicPercentage() {
		EpisodeOpportunityMainPage EpisodeOpportunity = new EpisodeOpportunityMainPage(driver);
		System.out.println("Chronic -----> " + EpisodeOpportunity.getChronicTotalCostPercent().getText());
		Assert.assertEquals(true, EpisodeOpportunity.getChronicTotalCostPercent().isDisplayed());
	}

	@Test(priority = 6)
	public void verifyProceduralPercentage() {
		EpisodeOpportunityMainPage EpisodeOpportunity = new EpisodeOpportunityMainPage(driver);
		System.out.println("Procedural ----> " + EpisodeOpportunity.getProceduralTotalCostPercent().getText());
		Assert.assertEquals(true, EpisodeOpportunity.getProceduralTotalCostPercent().isDisplayed());
	}

	@Test(priority = 7)
	public void verifyOtherPercentage() {
		EpisodeOpportunityMainPage episodeOpportunity = new EpisodeOpportunityMainPage(driver);
		System.out.println("Other -----> " + episodeOpportunity.getOtherTotalCostPercent().getText());
		Assert.assertEquals(true, episodeOpportunity.getOtherTotalCostPercent().isDisplayed());
	}

	@Test(priority = 8)
	public void verifyWhatIsThis() {
		EpisodeOpportunityMainPage episodeOpportunity = new EpisodeOpportunityMainPage(driver);
		System.out.println("Let's find out ----> " + episodeOpportunity.getAcuteTotalCostPercent().getText());
		Assert.assertEquals(true, episodeOpportunity.getAcuteTotalCostPercent().isDisplayed());
	}
	
	@Test(priority=9)
	public void verifyBookmarkDropdown() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		String bookmarkText = filter.getBookmarkDropdown().getText();
		System.out.println("Bookmark DropDown Text is ----------->" + bookmarkText);
		
		filter.getBookmarkDropdown().click();
		
	}
	
	@Test(priority=10)
	public void verifyEpisodesSearch() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		filter.getSearchEpisodes().sendKeys("cax");
	}
	
	@Test(priority=11)
	public void verifyAcuteTreeToggle() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		filter.getAcuteTreeToggle().click();
	}

	@Test(priority=12)
	public void verifyAcuteCheckBox() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		filter.getAcuteCheckBox().click();
	}
	
	@Test(priority=13)
	public void verifyAcuteText() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		System.out.println("Acute Text is ---->" + filter.getAcuteText().getText());
	}
	
	@Test(priority=14)
	public void verifySaveBookmarkButton() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		System.out.println("Save Bookmark Text is ---->" + filter.getSaveBookmarkButton().getText());
		filter.getSaveBookmarkButton().click();
		
	}
	
	@Test(priority=15)
	public void enterBookmarkName() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		filter.getSaveBookmarkName().click();
		filter.getSaveBookmarkName().sendKeys("Automation Test");
		
	}
	
	@Test(priority=16)
	public void closeBookmarkPopup() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		filter.getToCloseBookmarkPopup().click();
		
		
		
	}
	
	@Test(priority=17)
	public void riskAdjustText() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		String actText= filter.getRiskAdjustHeaderText().getText();
		String expText = "Risk Adjust";
		Assert.assertEquals(actText, expText);
		
	}
	
	@Test(priority=18)
	public void riskAdjustSwitch() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		filter.getRiskAdjustToggleSwitch().click();
	}
	
	@Test(priority=19)
	public void searchEpisodesFromTable() {
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		mainPage.getSearchEpisodesFromTable().click();
		mainPage.getSearchEpisodesFromTable().sendKeys("CABG &/or Valve Procedures");
	}
	
	@Test(priority=20)
	public void exportToExcel() {
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		mainPage.getExportToExcelIcon().click();
	}
	
	
	
	
	
}
