/**
 * 
 */
package com.healthqx.AutomationFrame;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.AdministrationPage;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;

/**
 * @author enebo4f
 *
 */
public class ClearCache {
	
	WebDriver driver;
	Environment env;
	BrowserTypes  browser;
	
	AdministrationPage admin;
	NavigationHeader navigate;
	
	@BeforeSuite
	@Parameters({"browser","environment"})
public void login(String browserType,String environment) {
	ConfigFactory.setProperty("browser", browserType);
	browser = ConfigFactory.create(BrowserTypes.class);
	ConfigFactory.setProperty("env", environment);
	env = ConfigFactory.create(Environment.class);
	driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
	
	
	Login signIn = new Login(driver);
	signIn.loginToApp(env.username(), env.password());
}
	@Test(priority=1,description="Clear Cache and verify url changed to clear = true")
	public void clearCache() {
		
		navigate = new NavigationHeader(driver);
		admin = new AdministrationPage(driver);
		
		navigate.getAdministration().click();
		admin.getCacheItems().click();
		admin.getClearCache().click();
		Synchronize pause = new Synchronize();
		pause.waitUntilElementPresent(1000);
		String url = driver.getCurrentUrl();
		System.out.println(url);
		Assert.assertTrue(url.contains("clear=true"));
		
	}
	
	@AfterSuite()
	public void tearDown() {
		
		LogOut logout = new LogOut(driver);
		logout.logOff();
	}
}
