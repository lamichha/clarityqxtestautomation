/**
 * 
 */
package com.healthqx.AutomationFrame.NetworkAnalysis;

import java.util.ArrayList;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.LoginPage;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.VerificationMethodListener;
import com.healthqx.AutomationFrame.Utility.Verify;
//import com.sun.jna.platform.unix.X11.Window;

/**
 * @author enebo4f
 *Test case to verify layout of the Network Analysis page
 */
@Listeners(VerificationMethodListener.class)
public class TestNetworkAnalysisMainPageLayout {

	
	WebDriver driver;
	Environment env;
	BrowserTypes  browser;
	
	//@BeforeSuite
	@BeforeClass
	@Parameters({"browser","environment"})
	public void beforeTest(String browserType,String environment) {
		
		ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
		
		Login signIn = new Login(driver);
		signIn.loginToApp(env.username(), env.password());
		
		
	}
	
	@Test(priority=1, description="Navigate To Network Analysis page from Tile", groups= {"Network Analysis MainPage- LayOut"})
	public void navigateToEposodeOpportunityUsingTile() {
		
		NavigationTiles tile = new NavigationTiles(driver);
		NavigationHeader navigation = new NavigationHeader(driver);
		
		tile.navigateToNetworkAnalysis();
		
		
		
		Verify.verifyTrue(navigation.getSubModuleBreadCrumb().getText().contains("Network Analysis"));
	}
	
	@Test(priority=2,description="Navigate back to Main Page by clicking on Home", groups= {"Network Analysis MainPage- LayOut"})
	public void navigateBackToMainPage() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		NavigationHeader navigation = new NavigationHeader(driver);
		
		NavigationTiles tile = new NavigationTiles(driver);
		
		navigation.getHomeBreadCrumb().click();
		
		WebElement allTiles = wait.until(ExpectedConditions.visibilityOf(tile.getAllTiles()));
		
		
		Verify.verifyTrue(allTiles.isDisplayed());
		
		
	}
	
	@Test(priority=3,description="Navigate to Network Analysis page from dropdown", groups= {"Network Analysis MainPage- LayOut"})
	public void navigateToNetworkAnalysisUsingDropDown() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		NavigationHeader navigation = new NavigationHeader(driver);
		
		navigation.navigateToNetworkAnalysisFromDropDown();
		
		WebElement breadCrumb = wait.until(ExpectedConditions.visibilityOf(navigation.getMainModuleBreadCrumb()));
		
		
		Verify.verifyTrue(breadCrumb.isDisplayed());
	}
	
	@Test(priority=4,description="Foot Notes should be displayed", groups= {"Network Analysis MainPage- LayOut"})
	public void verifyFootNote() {
		
		LoginPage page = new LoginPage(driver);
		
		String actCopyRightText = page.getCopyRight().getText();
		String expCopyRightText = "©2017 Change Healthcare Operations LLC and/or its subsidiaries or affiliates. All Rights Reserved. CPT only: CPT copyright 2016 American Medical Association. All Rights Reserved.";
		

		
		
		Verify.verifyEquals(actCopyRightText, expCopyRightText);
		Verify.verifyTrue(page.getTermsOfUse().isDisplayed());
		Verify.verifyTrue(page.getPrivacyNotice().isDisplayed());
		Verify.verifyTrue(page.getContactUs().isDisplayed());
	}
	
	@Test(priority=5,description="When clicked on Terms Of Use link, pop-up should appear with correct headline", groups= {"Network Analysis MainPage- LayOut"})
	public void verifyTermsOfUse() {
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		
		LoginPage page = new LoginPage(driver);
		
		
		page.getTermsOfUse().click();
		
		
		
		WebElement termsOfUseHeader = wait.until(ExpectedConditions.visibilityOf(page.getTermsOfUseHeader()));
		
		String actTermsOfUseHeader = termsOfUseHeader.getText();
		System.out.println("Text for Terms Of Use is ---->"+actTermsOfUseHeader);
		String expTermsOfUseHeader = "Terms of Use";
		
		Verify.verifyEquals(actTermsOfUseHeader, expTermsOfUseHeader);
		
		page.getCloseTermsOfUsePopup().click();
		
		Synchronize sync = new Synchronize();
		sync.waitUntilElementPresent(1000);
		
		
		
	}
	
	@Test(priority=6,description="When clicked on Privacy Notice link, pop-up should appear with correct headline", groups= {"Network Analysis MainPage- LayOut"})
	public void verifyPrivacyNotice() {
		
		LoginPage page = new LoginPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		WebElement privacyNoticeLink = wait.until(ExpectedConditions.visibilityOf(page.getPrivacyNotice()));
		
		
		privacyNoticeLink.click();
		
		WebElement privacyOfNoticeHeader = wait.until(ExpectedConditions.visibilityOf(page.getPrivacyNoticeHeader()));
		
		
		
		String actPrivacyNoticeHeader = privacyOfNoticeHeader.getText();
		System.out.println("Text for Privacy Notice is ---->"+actPrivacyNoticeHeader);
		String expPrivacyNoticeHeader = "Privacy Notice";
		
		Verify.verifyEquals(actPrivacyNoticeHeader, expPrivacyNoticeHeader);
		
		page.getClosePrivacyNoticePopup().click();
		
	}
	
	@Test(priority=7,description="verify header Note present", groups= {"Network Analysis MainPage- LayOut"})
	public void verifyHeader() {
		NavigationHeader header = new NavigationHeader(driver);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		//verify healthqx logo present 
		WebElement healthQxLogo = wait.until(ExpectedConditions.visibilityOf(header.getHealthQXLogo()));
		Verify.verifyTrue(healthQxLogo.isDisplayed());
		
		//verify strategiec insights dropdown present
		WebElement strategicInsightsDropDown = wait.until(ExpectedConditions.visibilityOf(header.getStrategicInsights()));
		Verify.verifyTrue(strategicInsightsDropDown.isDisplayed());
		
		//verify episode configuration dropdown present
		WebElement episodeConfigurationDropdown = wait.until(ExpectedConditions.visibilityOf(header.getEpisodeConfiguration()));
		Verify.verifyTrue(episodeConfigurationDropdown.isDisplayed());
		
		//verify budgets & reconciliation dorpdown present
		WebElement budgetAndReconDropdown = wait.until(ExpectedConditions.visibilityOf(header.getBudgetsReconciliation()));
		Verify.verifyTrue(budgetAndReconDropdown.isDisplayed());  //commented because this module is not in other environment
		
		
		//verify practice/tin search box present
		WebElement practiceSearchBox = wait.until(ExpectedConditions.visibilityOf(header.getSearchBox()));
		Verify.verifyTrue(practiceSearchBox.isDisplayed());
		
		
		//verify practice/tin search button present
		WebElement practiceSearchButton = wait.until(ExpectedConditions.visibilityOf(header.getSearchButton()));
		Verify.verifyTrue(practiceSearchButton.isDisplayed());
		
		
		//verify myHealthQx present
		WebElement myHealthQx = wait.until(ExpectedConditions.visibilityOf(header.getMyHealthQX()));
		Verify.verifyTrue(myHealthQx.isDisplayed());
		
		
		//verify admin present
		WebElement adminGear = wait.until(ExpectedConditions.visibilityOf(header.getAdministration()));
		Verify.verifyTrue(adminGear.isDisplayed());
		
		
		//verify logout present
		WebElement logOutButton = wait.until(ExpectedConditions.visibilityOf(header.getLogOut()));
		Verify.verifyTrue(logOutButton.isDisplayed());
		
		
		//verify help present
		WebElement helpButton = wait.until(ExpectedConditions.visibilityOf(header.getHelp()));
		Verify.verifyTrue(helpButton.isDisplayed());
		
		System.out.println("Verification executes all !! ");
		
	}
	
	
	
	
	@Test(priority=8,description="When clicked on help link, help page should open in new tab", groups= {"Network Analysis MainPage- LayOut"})
	public void verifyHelpPageNewTab() {
		NavigationHeader header = new NavigationHeader(driver);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		WebElement helpLink = wait.until(ExpectedConditions.visibilityOf(header.getHelp()));
		helpLink.click();
		
		ArrayList<String> helpPage = new ArrayList<String>(driver.getWindowHandles());
		
		driver.switchTo().window(helpPage.get(1));
		
		String expHelpPageUrl = env.url()+"/Static/guides/healthqx-user-guide.pdf";
		String actHelpPageUrl = driver.getCurrentUrl();
		
		Verify.verifyEquals(actHelpPageUrl, expHelpPageUrl);
		
		driver.close();
		driver.switchTo().window(helpPage.get(0));
		
	}
	
	
	
	

	
	
	@AfterSuite()
	//@AfterClass
	public void tearDown() {
			
		LogOut logout = new LogOut(driver);
		logout.logOff();
		
	}
}
