/**
 * 
 */
package com.healthqx.AutomationFrame.NetworkAnalysis;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.LoginPage;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisTableHeaders;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisTableValuesRiskOff;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisFilter;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.VerificationMethodListener;
import com.healthqx.AutomationFrame.Utility.Verify;
//import com.sun.jna.platform.unix.X11.Window;

/**
 * @author enebo4f
 *Test case is to perform data validation in Network Analysis main page
 */
@Listeners(VerificationMethodListener.class)
public class TestNetworkAnalysisMainPageAcuteRiskOff {

	
	WebDriver driver;
	Environment env;
	BrowserTypes  browser;
	DataBaseConnector dbConnector;
	private String episodeCategory;
	private String episodeName;
	
	//@BeforeSuite
	@BeforeClass
	@Parameters({"browser","environment"})
	public void beforeTest(String browserType,String environment) {
		
		ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
		
		Login signIn = new Login(driver);
		signIn.loginToApp(env.username(), env.password());
		
		//creating DB Connection
				dbConnector= new DataBaseConnector(env.dbEnv(),env.dbUrlDqx());
				dbConnector.openConnection();
		
		
	}
	
	@Test(priority=1,description="Navigate to Network Analysis page", groups= {"Network Analysis MainPage- Acute"})
	public void navigateToNetworkAnalysisUsingDropDown() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		NavigationHeader navigation = new NavigationHeader(driver);
		
		navigation.navigateToNetworkAnalysisFromDropDown();
		
		WebElement breadCrumb = wait.until(ExpectedConditions.visibilityOf(navigation.getMainModuleBreadCrumb()));
		
		
		Verify.verifyTrue(breadCrumb.isDisplayed());
	}
	
	@Test(priority=2,description="Select PracticeCount = 500, Episode = Any Acute Episode, Min Volume =30", groups= {"Network Analysis MainPage- Acute"})
	//@BeforeTest()
	public void updateFilters() {
		Synchronize waits = new Synchronize();
		NetworkAnalysisFilter filter = new NetworkAnalysisFilter(driver);
		
		
		filter.getPracticeCountFiveHundred().click(); //Practice Count 500
		waits.waitUntilElementPresent(5000);
		
		episodeCategory = filter.getAcuteText().getAttribute("innerText").trim();
		filter.getAcuteTreeToggle().click();
		waits.waitUntilElementPresent(3000);
		episodeName = filter.getFourthEpisodeAcute().getAttribute("innerText").trim();
		filter.getFourthEpisodeAcute().click();
		
		waits.waitUntilElementPresent(5000);
		
		
		filter.getEpisodeVolumeMinFilter().click();
		filter.getEpisodeVolumeMinFilter().sendKeys("30");
		filter.getEpisodeVolumeFilterTextHeader().click();
		waits.waitUntilElementPresent(6000);
		
	}
	
	@Test(priority=3, description = "validate average cost calculation for each rows of data", groups= {"Network Analysis MainPage- Acute"})
	public void verifyAverageCostCalculation() {
		
		NetworkAnalysisTableValuesRiskOff value = new NetworkAnalysisTableValuesRiskOff(driver);
		
		List<NetworkAnalysisTableHeaders> rowList = value.rowValues();
		
		for(NetworkAnalysisTableHeaders row: rowList ) {
			//verify AverageTypicalCost = TotalTypicalCost/EpisodeCount
			Verify.verifyEquals(row.getAverageTypical(), row.getTotalTypicalCost().divide(row.getEpisodeCounts(),0,RoundingMode.HALF_UP), "Average Typical Cost does not match for " + row.getPracticeName());
			//Verify AverageCompCost = TotalCompCost/EpisodeCount
			Verify.verifyEquals(row.getAverageCompCost(), row.getTotalCompCost().divide(row.getEpisodeCounts(),0,RoundingMode.HALF_UP),"Average Comp Cost does not match for " + row.getPracticeName());
			//Verify Average Pac = TotalPac/EpisodeCount
			Verify.verifyEquals(row.getAveragePacCost(), row.getTotalPacCost().divide(row.getEpisodeCounts(),0,RoundingMode.HALF_UP), "Average Pac Cost does not match for " + row.getPracticeName());
			//Verify Average Cost = TotalCost/EpisodeCount
			Verify.verifyEquals(row.getAverageCost(), row.getTotalCost().divide(row.getEpisodeCounts(),0,RoundingMode.HALF_UP), "Average Cost does not match for " + row.getPracticeName());	
		}
		
	}
	
	@Test(priority=3, description = "validate each rows of data against DB", groups= {"Network Analysis MainPage- Acute"})
	public void verifyCostsAgainstDb() {
NetworkAnalysisTableValuesRiskOff networkAnalysisTableValuesRiskOff = new NetworkAnalysisTableValuesRiskOff(driver);
		List<NetworkAnalysisTableHeaders> ntwkAnlysTableValueList = networkAnalysisTableValuesRiskOff.rowValues();
		for(NetworkAnalysisTableHeaders valueList: ntwkAnlysTableValueList ) {
			//Verify Episode Counts
			String episodeCountQuery = "select episodeCount from rpt.NetworkAnalysisUnSplit where EcrRunId = " + env.EcrRunId() + " and levelId= 4 and OutlierTypeId=1 and PracticeDisplayName = '" + valueList.getPracticeName()+ "'" + " and PartnerTin= '" + valueList.getTin()+ "'" + " and episodeCategoryName=" + "'"+episodeCategory+"'"+" and EpisodeName=" +"'"+episodeName+"' ;";		
			BigDecimal episodeCountDb = new BigDecimal(dbConnector.executeQuerySingleValue(episodeCountQuery));
			Verify.verifyEquals(valueList.getEpisodeCounts(),episodeCountDb, "EpisodeCounts do not match with DB for " + valueList.getPracticeName() + " and query is --> " + episodeCountQuery );
			
			//verify AverageTypicalCost = TotalTypicalCost/EpisodeCount
			String averageTypicalQuery = "select AverageTypicalCost from rpt.NetworkAnalysisUnSplit where EcrRunId = " + env.EcrRunId() + " and levelId= 4 and OutlierTypeId=1 and PracticeDisplayName = '" + valueList.getPracticeName()+ "'" + " and PartnerTin= '" + valueList.getTin()+ "'" + " and episodeCategoryName=" + "'"+episodeCategory+"'"+" and EpisodeName=" +"'"+episodeName+"' ;";		
			BigDecimal averageTypicalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageTypicalQuery)).setScale(0, RoundingMode.HALF_UP);			
			Verify.verifyEquals(valueList.getAverageTypical(), averageTypicalCostDb, "Average Typical Cost do not match against Db for " + valueList.getPracticeName() + " and query is --> " + averageTypicalQuery );
			
			//Verify AverageCompCost = TotalCompCost/EpisodeCount
			String averageCompCostQuery = "select AverageTypicalComplicationCost from rpt.NetworkAnalysisUnSplit where EcrRunId = " + env.EcrRunId() + " and levelId= 4 and OutlierTypeId=1 and PracticeDisplayName = '" + valueList.getPracticeName()+ "'" + " and PartnerTin= '" + valueList.getTin()+ "'" + " and episodeCategoryName=" + "'"+episodeCategory+"'"+" and EpisodeName=" +"'"+episodeName+"' ;";	;
			BigDecimal averageCompCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCompCostQuery)).setScale(0, RoundingMode.HALF_UP);
			
			Verify.verifyEquals(valueList.getAverageCompCost(), averageCompCostDb, "Average Comp Cost do not match against Db for " + valueList.getPracticeName() + " and query is --> " + averageCompCostQuery);
			
			//Verify Average Pac = TotalPac/EpisodeCount
			String averagePacCostQuery = "select AveragePacCost from rpt.NetworkAnalysisUnSplit where EcrRunId = " + env.EcrRunId() + " and levelId= 4 and OutlierTypeId=1 and PracticeDisplayName = '" + valueList.getPracticeName()+ "'" + " and PartnerTin= '" + valueList.getTin()+ "'" + " and episodeCategoryName=" + "'"+episodeCategory+"'"+" and EpisodeName=" +"'"+episodeName+"' ;";	;
			BigDecimal averagePacCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averagePacCostQuery)).setScale(0, RoundingMode.HALF_UP);
			
			Verify.verifyEquals(valueList.getAveragePacCost(), averagePacCostDb, "Average Pac Cost do not match against Db for " + valueList.getPracticeName() + " and query is --> " + averagePacCostQuery);
			
			//Verify Average Cost = TotalCost/EpisodeCount
			String averageCostQuery = "select AverageCost from rpt.NetworkAnalysisUnSplit where EcrRunId = " + env.EcrRunId() + " and levelId= 4 and OutlierTypeId=1 and PracticeDisplayName = '" + valueList.getPracticeName()+ "'" + " and PartnerTin= '" + valueList.getTin()+ "'" + " and episodeCategoryName=" + "'"+episodeCategory+"'"+" and EpisodeName=" +"'"+episodeName+"' ;";	;
			BigDecimal averageCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCostQuery)).setScale(0, RoundingMode.HALF_UP);
		
			Verify.verifyEquals(valueList.getAverageCost(), averageCostDb, "Average Cost do not match against Db for " + valueList.getPracticeName() + " and query is --> " + averageCostQuery);
			
			
			 
		}
		
	}

	
	@AfterSuite()
	//@AfterClass
	public void tearDown() {
			
		LogOut logout = new LogOut(driver);
		logout.logOff();
		dbConnector.closeConnection();
		
	}
}
