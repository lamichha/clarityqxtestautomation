/**
 * 
 */
package com.healthqx.AutomationFrame.NetworkAnalysis;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.aeonbits.owner.ConfigFactory;
import org.hamcrest.number.IsCloseTo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityFilter;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.LoginPage;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisTableHeaders;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisTableValuesRiskOff;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisTableValuesRiskOn;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisBenchmarkReportTableHeaders;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisBenchmarkReportTableValues;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisCostBreakDownTableHeaders;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisCostBreakdownTableValues;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisCostPerMemberTableHeaders;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisCostPerMemberTableValues;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisFilter;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisMainPage;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisPacReportTableHeaders;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisPacReportTableValues;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisSubPage;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.VerificationMethodListener;
import com.healthqx.AutomationFrame.Utility.Verify;
import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;
//import com.sun.jna.platform.unix.X11.Window;

/**
 * @author enebo4f Test case is to perform data validation in Network Analysis
 *         main page
 */
@Listeners(VerificationMethodListener.class)
public class TestNetworkAnalysisMainPageAcute {

	WebDriver driver;
	Environment env;
	BrowserTypes browser;
	DataBaseConnector dbConnector;
	private String episodeCategory;
	private String episodeName;
	private String episodeAcronym;
	private String practiceName;
	private String partnerTin;
	private String altarumLevel;
	private BigDecimal episodeCountDb;
	private BigDecimal error = new BigDecimal("1");

	// @BeforeSuite
	@BeforeClass
	@Parameters({ "browser", "environment" })
	public void beforeTest(String browserType, String environment) {

		ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url());

		Login signIn = new Login(driver);
		signIn.loginToApp(env.username(), env.password());

		// creating DB Connection
		dbConnector = new DataBaseConnector(env.dbEnv(), env.dbUrlDqx());
		dbConnector.openConnection();

	}

	@Test(priority = 1, description = "Navigate to Network Analysis page", groups = {
			"Network Analysis MainPage- Acute" })
	public void navigateToNetworkAnalysisUsingDropDownAcute() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		NavigationHeader navigation = new NavigationHeader(driver);

		navigation.navigateToNetworkAnalysisFromDropDown();

		WebElement breadCrumb = wait.until(ExpectedConditions.visibilityOf(navigation.getMainModuleBreadCrumb()));

		Verify.verifyTrue(breadCrumb.isDisplayed());
	}

	@Test(priority = 2, description = "Select PracticeCount = 500, Episode = Any Acute Episode, Min Volume =10", groups = {
			"Network Analysis MainPage- Acute" })
	// @BeforeTest()
	public void updateFiltersAcute() {
		Synchronize waits = new Synchronize();
		NetworkAnalysisFilter filter = new NetworkAnalysisFilter(driver);
		EpisodeOpportunityFilter eFilter = new EpisodeOpportunityFilter(driver);
		WebDriverWait pause = new WebDriverWait(driver, 10);
		filter.getPracticeCountFiveHundred().click(); // Practice Count 500
		waits.waitUntilElementPresent(3000);

		episodeCategory = filter.getAcuteText().getAttribute("innerText").trim();
		filter.getAcuteTreeToggle().click();
		waits.waitUntilElementPresent(3000);
		episodeName = filter.getFourthEpisodeAcute().getAttribute("innerText").trim();
		filter.getFourthEpisodeAcute().click();

		waits.waitUntilElementPresent(3000);

		try {
			WebElement changeLevel = pause
				.until(ExpectedConditions.elementToBeClickable(eFilter.getChangeLevelToAltarumDefault())); // filter.getChangeLevelToAltarumDefault();
			changeLevel.isDisplayed();
			altarumLevel = changeLevel.getAttribute("innerText").replaceAll("Change to level ", "");
			changeLevel.click();
		} catch (Exception e) {
			System.out.println("Altarum level not displayed getting level from level bar ");
			for (int i = 1; i <= 5; i++) {

				if (driver.findElement(By.xpath("//*[@id=\"level-filter\"]/div[1]/div[" + i + "]")).getAttribute("aria-selected").equals("true")) {
					System.out.println("LevelId = " + driver.findElement(By.xpath("//*[@id=\"level-filter\"]/div[1]/div[" + i + "]")).getAttribute("innerText"));
					altarumLevel = driver.findElement(By.xpath("//*[@id=\"level-filter\"]/div[1]/div[" + i + "]")).getAttribute("innerText");
				}
			}
		}
		
		waits.waitUntilElementPresent(3000);
		filter.getEpisodeVolumeMinFilter().click();
		filter.getEpisodeVolumeMinFilter().sendKeys("10");
		filter.getEpisodeVolumeFilterTextHeader().click();
		waits.waitUntilElementPresent(3000);

	}

	@Test(priority = 3, description = "Calculate Average Cost for each practice When Risk On", groups = {
			"Network Analysis MainPage- Acute" })
	public void verifyAvgCostCalculationRiskOnAcute() {

		NetworkAnalysisTableValuesRiskOn value = new NetworkAnalysisTableValuesRiskOn(driver);

		List<NetworkAnalysisTableHeaders> rowList = value.rowValues();

		for (NetworkAnalysisTableHeaders row : rowList) {
			// verify AverageTypicalCost = TotalTypicalCost/EpisodeCount
			assertThat("Average Typical Cost does not match for " + episodeCategory + " " + episodeName + " " + row.getPracticeName(),row.getTotalTypicalCost().divide(row.getEpisodeCounts(), 0, RoundingMode.HALF_UP), closeTo(row.getAverageTypical(),error));
			
			// Verify AverageCompCost = TotalCompCost/EpisodeCount
			assertThat("Average Comp Cost does not match for "  + episodeCategory + " " + episodeName + " " +  row.getPracticeName(), row.getTotalCompCost().divide(row.getEpisodeCounts(), 0, RoundingMode.HALF_UP), closeTo(row.getAverageCompCost(),error));
			
			// Verify Average Pac = TotalPac/EpisodeCount
			assertThat("Average Pac Cost does not match for "  + episodeCategory + " " + episodeName + " " +  row.getPracticeName(), row.getTotalPacCost().divide(row.getEpisodeCounts(), 0, RoundingMode.HALF_UP), closeTo(row.getAveragePacCost(),error));
			
			// Verify Average Cost = TotalCost/EpisodeCount
			
			assertThat("Average Cost does not match for "  + episodeCategory + " " + episodeName + " " +  row.getPracticeName(), row.getTotalCost().divide(row.getEpisodeCounts(), 0, RoundingMode.HALF_UP), closeTo(row.getAverageCost(), error));
		}

	}

	@Test(priority = 4, description = "validate each rows of data against DB When Risk On", groups = {
			"Network Analysis MainPage- Acute" })
	public void verifyCostsAgainstDbRiskOnAcute() {
		NetworkAnalysisTableValuesRiskOn value = new NetworkAnalysisTableValuesRiskOn(driver);

		List<NetworkAnalysisTableHeaders> rowList = value.rowValues();

		for (NetworkAnalysisTableHeaders row : rowList) {
			// Verify Episode Counts
			String episodeCountQuery = "select episodeCount from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			
			
			try {
				 episodeCountDb = new BigDecimal(dbConnector.executeQuerySingleValue(episodeCountQuery));
			}
			catch (java.lang.NumberFormatException e){
				System.out.println("Query returned no result: " + episodeCountQuery);
			}
			
			Verify.verifyEquals(row.getEpisodeCounts(), episodeCountDb, "EpisodeCounts do not match with DB for "
					+ row.getPracticeName() + " and query is --> " + episodeCountQuery);
			
			
			// verify AverageTypicalCost = TotalTypicalCost/EpisodeCount
			String averageTypicalQuery = "select AverageTypicalCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			BigDecimal averageTypicalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageTypicalQuery))
					.setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageTypical(), averageTypicalCostDb,
					"Average Typical Cost do not match against Db for " + row.getPracticeName() + " and query is --> "
							+ averageTypicalQuery);

			// Verify AverageCompCost = TotalCompCost/EpisodeCount
			String averageCompCostQuery = "select AverageTypicalComplicationCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			;
			BigDecimal averageCompCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCompCostQuery)).setScale(0,
					RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getAverageCompCost(), averageCompCostDb,
					"Average Comp Cost do not match against Db for " + row.getPracticeName() + " and query is --> "
							+ averageCompCostQuery);

			// Verify Average Pac = TotalPac/EpisodeCount
			String averagePacCostQuery = "select AveragePacCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			BigDecimal averagePacCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averagePacCostQuery)).setScale(0,
					RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getAveragePacCost(), averagePacCostDb,
					"Average Pac Cost do not match against Db for " + row.getPracticeName() + " and query is --> "
							+ averagePacCostQuery);

			// Verify Average Cost = TotalCost/EpisodeCount
			String averageCostQuery = "select AverageCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			BigDecimal averageCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCostQuery)).setScale(0,
					RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getAverageCost(), averageCostDb, "Average Cost do not match against Db for "
					+ row.getPracticeName() + " and query is --> " + averageCostQuery);

			// Verify Risk Average
			String riskAverageQuery = "select RiskAdjustedAverageCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			BigDecimal riskAverageCost = new BigDecimal(dbConnector.executeQuerySingleValue(riskAverageQuery)).setScale(0,
					RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getRiskAverage(), riskAverageCost, "Risk Average do not match against Db for "
					+ row.getPracticeName() + " and query is --> " + riskAverageQuery);

			// Verify Total Risk
			String riskTotalQuery = "select RiskAdjustedTotalCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			BigDecimal riskTotalCost = new BigDecimal(dbConnector.executeQuerySingleValue(riskTotalQuery)).setScale(0,
					RoundingMode.HALF_UP);
			Verify.verifyEquals(riskTotalCost, row.getRiskTotal(), "Risk Total do not match against Db for "
					+ row.getPracticeName() + " and query is --> " + riskTotalQuery);

		}

	}

	@Test(priority = 5, description = "Turn Off Risk and apply filters", groups = {
			"Network Analysis MainPage- Acute" })
	public void verifyAvgCostCalculationRiskOffAcute() {
		Synchronize wait = new Synchronize();
		NetworkAnalysisFilter filter = new NetworkAnalysisFilter(driver);
		filter.getRiskAdjustOff().click();
		wait.waitUntilElementPresent(3000);

		filter.getEpisodeVolumeMinFilter().click();
		filter.getEpisodeVolumeMinFilter().sendKeys("10");
		filter.getEpisodeVolumeFilterTextHeader().click();
		wait.waitUntilElementPresent(6000);
		
		  NetworkAnalysisTableValuesRiskOff value = new
		  NetworkAnalysisTableValuesRiskOff(driver);
		  
		  List<NetworkAnalysisTableHeaders> rowList = value.rowValues();
		  
		  for(NetworkAnalysisTableHeaders row: rowList ) { 
			// verify AverageTypicalCost = TotalTypicalCost/EpisodeCount
			  assertThat("Risk Off Average Typical Cost does not match for " + episodeCategory + " " + episodeName + " " + row.getPracticeName(),row.getTotalTypicalCost().divide(row.getEpisodeCounts(), 0, RoundingMode.HALF_UP), closeTo(row.getAverageTypical(),error));

			  // Verify AverageCompCost = TotalCompCost/EpisodeCount
			  assertThat("Risk Off Average Comp Cost does not match for "  + episodeCategory + " " + episodeName + " " +  row.getPracticeName(), row.getTotalCompCost().divide(row.getEpisodeCounts(), 0, RoundingMode.HALF_UP), closeTo(row.getAverageCompCost(),error));

			  // Verify Average Pac = TotalPac/EpisodeCount
			  assertThat("Risk Off Average Pac Cost does not match for "  + episodeCategory + " " + episodeName + " " +  row.getPracticeName(), row.getTotalPacCost().divide(row.getEpisodeCounts(), 0, RoundingMode.HALF_UP), closeTo(row.getAveragePacCost(),error));

			  // Verify Average Cost = TotalCost/EpisodeCount

			  assertThat("Risk Off Average Cost does not match for "  + episodeCategory + " " + episodeName + " " +  row.getPracticeName(), row.getTotalCost().divide(row.getEpisodeCounts(), 0, RoundingMode.HALF_UP), closeTo(row.getAverageCost(), error));
		  
		  }
		 

	}

	@Test(priority = 6, description = "validate each rows of data against DB When Risk Off", groups = {
			"Network Analysis MainPage- Acute" })
	public void verifyCostsAgainstDbRiskOffAcute() {
		NetworkAnalysisTableValuesRiskOn value = new NetworkAnalysisTableValuesRiskOn(driver);

		List<NetworkAnalysisTableHeaders> rowList = value.rowValues();

		for (NetworkAnalysisTableHeaders row : rowList) {
			// Verify Episode Counts
			String episodeCountQuery = "select episodeCount from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			BigDecimal episodeCountDb = new BigDecimal(dbConnector.executeQuerySingleValue(episodeCountQuery));
			Verify.verifyEquals(row.getEpisodeCounts(), episodeCountDb,
					"Risk Off EpisodeCounts do not match with DB for " + row.getPracticeName() + " and query is --> "
							+ episodeCountQuery);

			// verify AverageTypicalCost = TotalTypicalCost/EpisodeCount
			String averageTypicalQuery = "select AverageTypicalCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			BigDecimal averageTypicalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageTypicalQuery))
					.setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageTypical(), averageTypicalCostDb,
					"Risk Off Average Typical Cost do not match against Db for " + row.getPracticeName()
							+ " and query is --> " + averageTypicalQuery);

			// Verify AverageCompCost = TotalCompCost/EpisodeCount
			String averageCompCostQuery = "select AverageTypicalComplicationCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			;
			BigDecimal averageCompCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCompCostQuery)).setScale(0,
					RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getAverageCompCost(), averageCompCostDb,
					"Risk Off Average Comp Cost do not match against Db for " + row.getPracticeName()
							+ " and query is --> " + averageCompCostQuery);

			// Verify Average Pac = TotalPac/EpisodeCount
			String averagePacCostQuery = "select AveragePacCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			BigDecimal averagePacCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averagePacCostQuery)).setScale(0,
					RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getAveragePacCost(), averagePacCostDb,
					"Risk Off Average Pac Cost do not match against Db for " + row.getPracticeName()
							+ " and query is --> " + averagePacCostQuery);

			// Verify Average Cost = TotalCost/EpisodeCount
			String averageCostQuery = "select AverageCost from rpt.NetworkAnalysisUnSplit where EcrRunId = "
					+ env.EcrRunId() + " and  LevelId = " + altarumLevel + "  and OutlierTypeId=1 and PracticeDisplayName = '"
					+ row.getPracticeName() + "'" + " and PartnerTin= '" + row.getTin() + "'"
					+ " and episodeCategoryName=" + "'" + episodeCategory + "'" + " and EpisodeName=" + "'"
					+ episodeName + "' ;";
			BigDecimal averageCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCostQuery)).setScale(0,
					RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getAverageCost(), averageCostDb,
					"Risk Off Average Cost do not match against Db for " + row.getPracticeName() + " and query is --> "
							+ averageCostQuery);

		}
	}

	@Test(priority = 7, description = "Navigate to Network Analysis Subpage", groups = {
			"Network Analysis SubPage- Acute" })
	public void navigateToSubpageAcute() {
		NetworkAnalysisSubPage subPage = new NetworkAnalysisSubPage(driver);
		Synchronize wait = new Synchronize();
		NavigationHeader nav = new NavigationHeader(driver);
		nav.getTableQuickLink().click();
		wait.waitUntilElementPresent(1000);
		NetworkAnalysisMainPage main = new NetworkAnalysisMainPage(driver);
		practiceName = main.getFirstPracticeNameNtwkAnlysMainPageTable().getText();
		partnerTin = main.getFirstPracticeTinNtwkAnlysMainPageTable().getText();

		main.getFirstPracticeNameNtwkAnlysMainPageTable().click();
		wait.waitUntilElementPresent(4000);

		nav.getBarChartQuickLink().click();
		wait.waitUntilElementPresent(1000);
		new Actions(driver).dragAndDrop(subPage.getDragFrom(), subPage.getDragTo()).perform();
		wait.waitUntilElementPresent(3000);
		// partnerTin =
		// subPage.getSubPageTableHeaderPracticeNameAndTin().getAttribute("innerText").replaceAll("",
		// "");

	}

	@Test(priority = 8, description = "compare Cost Breakdown data against Db", groups = {
			"Network Analysis SubPage- Acute" })
	public void costBreakdownDataValidationAcute() {
		NavigationHeader nav = new NavigationHeader(driver);
		episodeAcronym = nav.getSubPageEpisodeNameBreadCrumb().getAttribute("innerText").replaceAll("-.*", "").trim();

		NetworkAnalysisCostBreakdownTableValues costBreakdownTableValues = new NetworkAnalysisCostBreakdownTableValues(
				driver);
		List<NetworkAnalysisCostBreakDownTableHeaders> rowList = costBreakdownTableValues.rowValues();

		for (NetworkAnalysisCostBreakDownTableHeaders row : rowList) {
			// Verify Average Typical Cost against Db
			String preTriggerCostQuery = "SELECT PreTriggerCost FROM rpt.PracticeCostBreakdown where ecrrunid = "
					+ env.EcrRunId() + " and PracticeName = " + "'" + practiceName + "'" + " and PartnerTin = " + "'"
					+ partnerTin + "'" + " and OutlierTypeId= 1 and  LevelId = " + altarumLevel + "  and EpisodeAcronym= " + "'"
					+ episodeAcronym + "'" + " and ccsGroupName= " + "'" + row.getCcsGroup() + "'" + "and CcsName = "
					+ "'" + row.getCcsName() + "'" + " and ClaimTypeName = " + "'" + row.getClaimType() + "'"
					+ " and PlaceOfServiceName = " + "'" + row.getPlaceOfService() + "' ;";
			BigDecimal preTriggerCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(preTriggerCostQuery)).setScale(0,
					RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAveragePreTrigger(), preTriggerCostDb,
					"Average PreTrigger cost does not match and Query is " + preTriggerCostQuery);

			// Verify pre-Trigger PAC % against Db
			String preTriggerPacPercentQuery = "SELECT PreTriggerPacCostPercentage*100 FROM rpt.PracticeCostBreakdown where ecrrunid = "
					+ env.EcrRunId() + " and PracticeName = " + "'" + practiceName + "'" + " and PartnerTin = " + "'"
					+ partnerTin + "'" + " and OutlierTypeId= 1 and  LevelId = " + altarumLevel + "  and EpisodeAcronym= " + "'"
					+ episodeAcronym + "'" + " and ccsGroupName= " + "'" + row.getCcsGroup() + "'" + "and CcsName = "
					+ "'" + row.getCcsName() + "'" + " and ClaimTypeName = " + "'" + row.getClaimType() + "'"
					+ " and PlaceOfServiceName = " + "'" + row.getPlaceOfService() + "' ;";
			BigDecimal preTriggerPercentDb = new BigDecimal(dbConnector.executeQuerySingleValue(preTriggerPacPercentQuery))
					.setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getPreTriggerPacPercent(), preTriggerPercentDb,
					"Pre-Trigger Pac Cost Percentage does not match and Query is " + preTriggerPacPercentQuery);

			// Verify Average Trigger Cost against Db
			String averageTriggerQuery = "SELECT TriggerCost FROM rpt.PracticeCostBreakdown where ecrrunid = "
					+ env.EcrRunId() + " and PracticeName = " + "'" + practiceName + "'" + " and PartnerTin = " + "'"
					+ partnerTin + "'" + " and OutlierTypeId= 1 and  LevelId = " + altarumLevel + "  and EpisodeAcronym= " + "'"
					+ episodeAcronym + "'" + " and ccsGroupName= " + "'" + row.getCcsGroup() + "'" + "and CcsName = "
					+ "'" + row.getCcsName() + "'" + " and ClaimTypeName = " + "'" + row.getClaimType() + "'"
					+ " and PlaceOfServiceName = " + "'" + row.getPlaceOfService() + "' ;";
			BigDecimal averageTriggerDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageTriggerQuery)).setScale(0,
					RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageTrigger(), averageTriggerDb,
					"Trigger Cost does not match and Query is " + averageTriggerQuery);

			// Verify Trigger PAC % against Db
			String triggerPacPercentQuery = "SELECT TriggerPacCostPercentage*100 FROM rpt.PracticeCostBreakdown where ecrrunid = "
					+ env.EcrRunId() + " and PracticeName = " + "'" + practiceName + "'" + " and PartnerTin = " + "'"
					+ partnerTin + "'" + " and OutlierTypeId= 1 and  LevelId = " + altarumLevel + "  and EpisodeAcronym= " + "'"
					+ episodeAcronym + "'" + " and ccsGroupName= " + "'" + row.getCcsGroup() + "'" + "and CcsName = "
					+ "'" + row.getCcsName() + "'" + " and ClaimTypeName = " + "'" + row.getClaimType() + "'"
					+ " and PlaceOfServiceName = " + "'" + row.getPlaceOfService() + "' ;";
			BigDecimal triggerPacPercentDb = new BigDecimal(dbConnector.executeQuerySingleValue(triggerPacPercentQuery))
					.setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getTriggerPacPercent(), triggerPacPercentDb,
					"Trigger Pac Cost Percentage does not match and Query is " + triggerPacPercentQuery);

			// Verify Average Post Trigger against Db
			String averagePostTriggerQuery = "SELECT PostTriggerCost FROM rpt.PracticeCostBreakdown where ecrrunid = "
					+ env.EcrRunId() + " and PracticeName = " + "'" + practiceName + "'" + " and PartnerTin = " + "'"
					+ partnerTin + "'" + " and OutlierTypeId= 1 and  LevelId = " + altarumLevel + "  and EpisodeAcronym= " + "'"
					+ episodeAcronym + "'" + " and ccsGroupName= " + "'" + row.getCcsGroup() + "'" + "and CcsName = "
					+ "'" + row.getCcsName() + "'" + " and ClaimTypeName = " + "'" + row.getClaimType() + "'"
					+ " and PlaceOfServiceName = " + "'" + row.getPlaceOfService() + "' ;";
			BigDecimal averagePostTriggerDb = new BigDecimal(dbConnector.executeQuerySingleValue(averagePostTriggerQuery))
					.setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAveragePostTrigger(), averagePostTriggerDb,
					"Post Trigger Cost does not match and Query is " + averagePostTriggerQuery);

			// Verify Post-Trigger PAC % against Db
			String postTriggerPacPercentQuery = "SELECT PostTriggerPacCostPercentage*100 FROM rpt.PracticeCostBreakdown where ecrrunid = "
					+ env.EcrRunId() + " and PracticeName = " + "'" + practiceName + "'" + " and PartnerTin = " + "'"
					+ partnerTin + "'" + " and OutlierTypeId= 1 and  LevelId = " + altarumLevel + "  and EpisodeAcronym= " + "'"
					+ episodeAcronym + "'" + " and ccsGroupName= " + "'" + row.getCcsGroup() + "'" + "and CcsName = "
					+ "'" + row.getCcsName() + "'" + " and ClaimTypeName = " + "'" + row.getClaimType() + "'"
					+ " and PlaceOfServiceName = " + "'" + row.getPlaceOfService() + "' ;";
			BigDecimal postTriggerPacPercentDb = new BigDecimal(dbConnector.executeQuerySingleValue(postTriggerPacPercentQuery))
					.setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getPostTriggerPacPercent(), postTriggerPacPercentDb,
					"Post Trigger Pac Cost Percentage  does not match and Query is " + postTriggerPacPercentQuery);

			// Verify Average Cost against Db
			String averageCostQuery = "SELECT TotalCost FROM rpt.PracticeCostBreakdown where ecrrunid = "
					+ env.EcrRunId() + " and PracticeName = " + "'" + practiceName + "'" + " and PartnerTin = " + "'"
					+ partnerTin + "'" + " and OutlierTypeId= 1 and  LevelId = " + altarumLevel + "  and EpisodeAcronym= " + "'"
					+ episodeAcronym + "'" + " and ccsGroupName= " + "'" + row.getCcsGroup() + "'" + "and CcsName = "
					+ "'" + row.getCcsName() + "'" + " and ClaimTypeName = " + "'" + row.getClaimType() + "'"
					+ " and PlaceOfServiceName = " + "'" + row.getPlaceOfService() + "' ;";
			BigDecimal averageCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCostQuery)).setScale(0,
					RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageCost(), averageCostDb,
					"Total Cost  does not match and Query is " + averageCostQuery);

			// Verify PAC % against Db
			String pacPercentQuery = "SELECT TotalPacCostPercentage*100 FROM rpt.PracticeCostBreakdown where ecrrunid = "
					+ env.EcrRunId() + " and PracticeName = " + "'" + practiceName + "'" + " and PartnerTin = " + "'"
					+ partnerTin + "'" + " and OutlierTypeId= 1 and  LevelId = " + altarumLevel + "  and EpisodeAcronym= " + "'"
					+ episodeAcronym + "'" + " and ccsGroupName= " + "'" + row.getCcsGroup() + "'" + "and CcsName = "
					+ "'" + row.getCcsName() + "'" + " and ClaimTypeName = " + "'" + row.getClaimType() + "'"
					+ " and PlaceOfServiceName = " + "'" + row.getPlaceOfService() + "' ;";
			BigDecimal pacPercentDb = new BigDecimal(dbConnector.executeQuerySingleValue(pacPercentQuery)).setScale(0,
					RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getPacPercent(), pacPercentDb,
					"Total Pac Cost Percentage  does not match and Query is " + postTriggerPacPercentQuery);

		}
	}

	@Test(priority = 9, description = "compare Cost Per Member data against Db", groups = {
			"Network Analysis SubPage- Acute" })
	public void costPerMemberDataValidationAcute() {
		Synchronize wait = new Synchronize();
		NetworkAnalysisSubPage sub = new NetworkAnalysisSubPage(driver);
		NavigationHeader nav = new NavigationHeader(driver);
		nav.getExpandQuickLink().click();
		sub.getCostPerMemberTab().click();
		wait.waitUntilElementPresent(5000);

		while (!sub.getDragFrom().getText().contains("Drag a column header here to group by that column")) {
			new Actions(driver).dragAndDrop(sub.getDragFrom(), sub.getDragTo()).perform();
		}
		nav.getBarChartQuickLink().click();
		wait.waitUntilElementPresent(2000);

		NetworkAnalysisCostPerMemberTableValues costBreakdownTableValues = new NetworkAnalysisCostPerMemberTableValues(
				driver);
		List<NetworkAnalysisCostPerMemberTableHeaders> rowList = costBreakdownTableValues.rowValues();

		for (NetworkAnalysisCostPerMemberTableHeaders row : rowList) {
			// Verify Average Typical Cost against Db
			// String groupCode = row.get
			String averagePreTriggerQuery = "SELECT PreTriggerCost FROM rpt.PracticeCostBreakdownPerMember where ecrrunid = "
					+ env.EcrRunId() + " and PracticeName = " + "'" + practiceName + "'" + " and MemberKey= " + "'"
					+ row.getMember() + "'" + " and ProviderName = " + "'" + row.getProvider() + "'"
					+ " and PartnerTin = " + "'" + partnerTin + "'"
					+ " and OutlierTypeId= 1 and  LevelId = " + altarumLevel + "  and EpisodeAcronym= " + "'" + episodeAcronym + "'"
					+ " and ccsGroupName= " + "'" + row.getCcsGroup() + "'" + "and CcsName = " + "'" + row.getCcsName()
					+ "'" + " and ClaimTypeName = " + "'" + row.getClaimType() + "'" + " and PlaceOfServiceName = "
					+ "'" + row.getPlaceOfService() + "' ;";
			BigDecimal preTriggerCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averagePreTriggerQuery))
					.setScale(0, RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getAveragePreTrigger(), preTriggerCostDb,
					"Average PreTrigger cost does not match and Query is " + averagePreTriggerQuery);

		}
	}

	
	 
	 

	// Issue lies when group has -1 in beginning
	@Test(priority = 10, description = "compare Pac Report data against Db", groups = {
			"Network Analysis SubPage- Acute" })
	public void pacReportDataValidation() {
		Synchronize wait = new Synchronize();
		NetworkAnalysisSubPage sub = new NetworkAnalysisSubPage(driver);
		NavigationHeader nav = new NavigationHeader(driver);
		nav.getExpandQuickLink().click();
		sub.getPacReportTab().click();
		wait.waitUntilElementPresent(5000);

		nav.getTableQuickLink().click();
		wait.waitUntilElementPresent(2000);

		NetworkAnalysisPacReportTableValues pacReportTableValues = new NetworkAnalysisPacReportTableValues(driver);
		List<NetworkAnalysisPacReportTableHeaders> rowList = pacReportTableValues.rowValues();

		for (NetworkAnalysisPacReportTableHeaders row : rowList) {
			String memberCountQuery = "select  MemberCount from rpt.PracticeCostBreakdownPac where EcrRunId= "
					+ env.EcrRunId() + " and groupcode= " + "'" + row.getGroupCode() + "'" + " and dxcode = " + "'"
					+ row.getDxCode() + "'" + " and partnertin = " + "'" + partnerTin + "'"
					+ " and practicedisplayname = " + "'" + practiceName + "'"
					+ " and OutlierTypeId = 1 and episodeAcronym = " + "'" + episodeAcronym + "';";
			
			BigDecimal memberCountDb = new BigDecimal((dbConnector.executeQuerySingleValue(memberCountQuery))).setScale(0,
					RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getMemberCount(), memberCountDb,
					"member count does not match and query is " + memberCountQuery);

		}
	}

	/*@Test(priority = 11, description = "compare Benchmark Report data against Db", groups = {
			"Network Analysis SubPage- Acute" })
	public void benchmarkReportDataValidation() {
		Synchronize wait = new Synchronize();
		WebDriverWait pause = new WebDriverWait(driver, 20);
		NetworkAnalysisSubPage sub = new NetworkAnalysisSubPage(driver);
		NavigationHeader nav = new NavigationHeader(driver);
		EpisodeOpportunityFilter eFilter = new EpisodeOpportunityFilter(driver);
		NetworkAnalysisFilter filter = new NetworkAnalysisFilter(driver);
		nav.getExpandQuickLink().click();
		sub.getBenchmarkTab().click();
		wait.waitUntilElementPresent(5000);

		

		try {
			WebElement changeLevel = pause
					.until(ExpectedConditions.elementToBeClickable(eFilter.getChangeLevelToAltarumDefault()));// filter.getChangeLevelToAltarumDefault();
			
			changeLevel.isDisplayed();
			altarumLevel = changeLevel.getAttribute("innerText").replaceAll("Change to level ", "");

			changeLevel.click(); 
		}
		catch(Exception e) {
			System.out.println("Altarum level not displayed getting level from level bar ");
			
			
			for(int i =1; i<=5; i++) {
				
				if(driver.findElement(By.xpath("//*[@id=\"level-filter\"]/div[1]/div["+i +"]")).getAttribute("aria-selected").equals("true")) {
					System.out.println("LevelId = " + driver.findElement(By.xpath("//*[@id=\"level-filter\"]/div[1]/div["+i +"]")).getAttribute("innerText"));
					altarumLevel = driver.findElement(By.xpath("//*[@id=\"level-filter\"]/div[1]/div["+i +"]")).getAttribute("innerText") ;
				}
				
			}
			
		}

		wait.waitUntilElementPresent(3000);
		nav.getTableQuickLink().click();
		wait.waitUntilElementPresent(9000);

		NetworkAnalysisBenchmarkReportTableValues benchmarkReportTableValues = new NetworkAnalysisBenchmarkReportTableValues(
				driver);
		List<NetworkAnalysisBenchmarkReportTableHeaders> rowList = benchmarkReportTableValues.rowValues();

		for (NetworkAnalysisBenchmarkReportTableHeaders row : rowList) {
			// System.out.println("Practice Name On Benchmark Report Is
			// -->>>"+row.getPractice());

			String query = "SELECT sum(ape.SplitCostTot)/sum(ape.EpisodeCount) FROM agg.AttributedPracticeEpisodeLevelAggFact ape join dbo.PracticeDim pd on pd.PracticeId= ape.practiceid join dbo.EpisodeTypeDim ed on ed.EpisodeTypeId = ape.EpisodeTypeId where ape.EcrRunId= "
					+ env.EcrRunId() + " and pd.PracticeName= " + "'" + row.getPractice() + "'"
					+ " and ape.OutlierTypeId = 1 and ape.levelid = " + altarumLevel + "  and ed.episodeAcronym = "
					+ "'" + episodeAcronym + "' group by pd.PracticeName, pd.PracticeId ;";

			BigDecimal averageCostDb = new BigDecimal(dbConnector.executeSqlQuery(query)).setScale(0,
					RoundingMode.HALF_UP);

			Verify.verifyEquals(row.getAverageCost(), averageCostDb,
					" Average Cost does not match and the query is " + query);

		}

	}*/

	@AfterSuite()
	// @AfterClass
	public void tearDown() {

		LogOut logout = new LogOut(driver);
		logout.logOff();
		dbConnector.closeConnection();

	}
}
