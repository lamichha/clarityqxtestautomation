/**
 * 
 */
package com.healthqx.AutomationFrame;



import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;

/**
 * 
 * @author Ashish L.
 *
 *
 *This class contains test method to start up a browser and test a valid login
 */
public class VerifyValidLogin2FAEnabled {
	
	WebDriver driver;
	@Test(priority=1)
	public void verifyLogin()   {
		
		//This will launch browser and open application
		WebDriver driver = StartUpBrowser.StartBrowser("ie", "https://cqx-horizon-q.azurewebsites.net/");
	
		//Call method to Sign on
		
		Login login = new Login(driver);
		
		login.loginToApp("ashish.lamichhane@mckesson.com", "Mom01@momq");
		
		login.twoFactorAuth();
	}
	
	@Test(priority=2)
	public void verifyLogOut() {
	
		LogOut logoff = new LogOut(driver);
		logoff.logOff();
	}

}
