package com.healthqx.AutomationFrame;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;

public class TestQueries {
	WebDriver driver;
	DataBaseConnector dbConnector;
	Environment env;
	
	@Test()
	public void verifyEpisodeCounts() {
	/*	String query = "select count(EpisodeName) as EpisodeCounts FROM rpt.EpisodeOpportunitySplit where EcrRunId=26 and LevelId=1 and EpisodeCategoryName='Acute';";
		 String EpisodeCountFromDB= DataBaseConnector.executeSqlQuery("HorizonQA", query);
		System.out.println(EpisodeCountFromDB);*/
		//creating DB Conection
		dbConnector= new DataBaseConnector(env.dbEnv(),env.dbUrlDqx());
				dbConnector.openConnection();
		
		String SqlQuery = "select count(EpisodeName) as EpisodeCounts FROM rpt.EpisodeOpportunitySplit where EcrRunId=18 and LevelId=1 and EpisodeCategoryName='Acute';";
		String verifCode = dbConnector.executeQuerySingleValue(SqlQuery);
		
		System.out.println(verifCode);
	}
}

