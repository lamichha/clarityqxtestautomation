/**
 * 
 */
package com.healthqx.AutomationFrame;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.annotations.Test;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;

import com.healthqx.AutomationFrame.HealthQX.Login;

import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;



/**
 * @author Ashish L.
 *
 */
public class EpisodeOpportunityTestVersion {
	WebDriver driver;
	
	NavigationTiles eoNav;  
	//NavigationHeader navigation = new NavigationHeader(driver);
	
	
	//TakeScreenShot getScreenShot = new TakeScreenShot();
	
	//By TotalCostsPieChart = By.xpath("//*[@id=\"episode-types-piechart\"]");
	
	
	
	@Test(priority=1)
	// StartUp browser by calling utility method
	public void openBrowser() {
		eoNav = new NavigationTiles(driver);
		driver = StartUpBrowser.StartBrowser("chrome",
				"https://horizon-qa.clarityqx.com/");
		// login by calling login method
		Login login = new Login(driver);
		login.loginToApp("ashish.lamichhane@mckesson.com", "Mom01@momq");
		//getScreenShot.captureScreenshot(driver);
		//Navigate to Episode Opportunity
		
		eoNav.getEpisodeOpportunityTile().click();
		System.out.println("Clicked on Episode Opportunity");
	}
	
	/*@Test(priority=2)
	public void verifyTotalCosts() {
		
		
		
		navigation.getTableQuickLink().click();
		
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		
		String text1 = driver.findElement(TotalCostsPieChart).getText();
		System.out.println("Text is ++++++++----------->>>>>>>>>>" + text1);
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//TakeScreenShot getScreenShot = new TakeScreenShot();
		getScreenShot.captureScreenshot(driver);
		
		
	}*/	
	
	@Test(priority=2, description="verify pagination")
	public void verifyPagination() {
		List <WebElement> pagination = driver.findElements(By.xpath("//*[@id=\"costs-datagrid\"]/div/div[10]/div[2]/div"));
		
		System.out.println("Total size of the element is --->" + pagination.size());
		int size = pagination.size();
		// checkif pagination link exists 
		if(size>0)
		{ 
		System.out.println("pagination exists");
		// click on pagination link
		for(int i=3; i<size; i++){ 
		try{ 
		driver.findElement(By.xpath("//*[@id=\"costs-datagrid\"]/div/div[10]/div[2]/div["+i+"]")).click() ;
		Thread.sleep(5000); 
		System.out.println("Loop "+i);
		}
		catch(Exception e){ 
		e.printStackTrace(); } 
		} 
		} 
		else { 
		System.out.println("pagination not exists"); 
		} 
	}
	
	
	@Test(priority=2,description="Verify counts of record on page 10.20.40 etc")
	public void verifyPageCounts() {
		Synchronize pause = new Synchronize();
		
		List <WebElement> pageCounts = driver.findElements(By.xpath("//*[@id=\"costs-datagrid\"]/div/div[10]/div[1]/div"));
		
		System.out.println("Total Size of the page is" + pageCounts.size());
		
		int size = pageCounts.size();
		
		//Check if page has multiple counts 10 20 30 etc
		if(size>0) {
			System.out.println("Multile pages exists");
			
			for(int i =1; i<=size; i++) {
				driver.findElement(By.xpath("//*[@id=\"costs-datagrid\"]/div/div[10]/div[1]/div["+i+"]")).click();
				pause.waitUntilElementPresent(1000);
				
			}
		}
	}
	
	
	@Test(priority=2,description="")
	public void verifyNumberOfRecord() {
		
		List <WebElement> episodesOnGrid = driver.findElements(By.xpath("//*[@id=\"costs-datagrid\"]/div/div[6]/div/div[1]/div/table/tbody/tr/td[1]/a"));
		
		int size = episodesOnGrid.size();
		
		System.out.println("Number of Episodes displayed = " + size);
		
		
	}
	
		
	
	
	/*@Test(priority=9)
	public void logout() {
			
		LogOut loggingOff = new LogOut();
		loggingOff.logOff(driver);
	}*/
	
	

}
