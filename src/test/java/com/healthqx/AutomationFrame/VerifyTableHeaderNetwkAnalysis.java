/**
 * 
 */
package com.healthqx.AutomationFrame;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;

import org.testng.*;

/**
 * @author Ashish L.
 *
 */
public class VerifyTableHeaderNetwkAnalysis {
WebDriver driver;

public String PracticeName; 
public String PracticeTin;


By selectedPracticeNameFrmTable = By.xpath("//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/table/tbody/tr[1]/td[1]/a");

By selectedpracticeTin = By.xpath("//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/table/tbody/tr[1]/td[2]");

By costBrekdownTab = By.xpath("//A[@href='/Strategic-Insights/Reports/Cost-Breakdown'][text()='Cost Breakdown']/self::A");
By costPerMemberTab = By.xpath("//A[@href='/Strategic-Insights/Reports/Cost-Per-Member'][text()='Cost Per Member']/self::A");
By pACReportTab = By.xpath("//A[@href='/Strategic-Insights/Reports/PAC'][text()='PAC Report']/self::A");
By benchmarkTab = By.xpath("//A[@href='/Strategic-Insights/Reports/BenchmarkReport'][text()='Benchmark']/self::A");


By TableHeader = By.xpath("//H4/self::H4");



@BeforeSuite
//Start up the browser and launch application
public void launchApp() {
	driver = StartUpBrowser.StartBrowser("IE", "https://cqx-horizon-q.azurewebsites.net/Account/Login");
	//driver = StartUpBrowser.StartBrowser("IE", "https://ci-d-cqx-web002.azurewebsites.net");
	Login login = new Login(driver);
	login.loginToApp("ashish.lamichhane@mckesson.com", "Mom01@momq");
	
	//Navigate to Network Analysis Page
	NavigationTiles navigation = new NavigationTiles(driver);
	navigation.getNetworkAnalysisTile().click();
	
	//select a practice to navigate to the drill down pages
		PracticeName = driver.findElement(selectedPracticeNameFrmTable).getText();
		PracticeTin = driver.findElement(selectedpracticeTin).getText();
		driver.findElement(selectedPracticeNameFrmTable).click();
}

@Test(priority=1)
public void VerifyHeaderCostBreakdown() {
	
	//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	//Explicit wait
	
	WebDriverWait waiter = new WebDriverWait(driver, 5000);
	waiter.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"cost-breakdown-provider-chart\"]")));
	
	
	//Verify table header
	String ActText = driver.findElement(TableHeader).getText();
	String ExpText = "Cost Breakdown for " + PracticeName + " " + "(" + PracticeTin + ")";
	Assert.assertEquals(ExpText, ActText);
	}

@Test(priority=2)
public void VerifyHeaderCostPerMember() {
	
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	//navigate to cost per member page
	driver.findElement(costPerMemberTab).click();
	
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	//Verify table header
	
	String ActText = driver.findElement(TableHeader).getText();
	String ExpText = "Cost Per Member for " + PracticeName + " " + "(" + PracticeTin + ")";
	Assert.assertEquals(ExpText, ActText);
	}

@Test(priority=3)
public void VerifyHeaderPACReport() {
	
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	//navigate to cost per member page
	driver.findElement(pACReportTab).click();
	
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	//Verify table header
	
	String ActText = driver.findElement(TableHeader).getText();
	System.out.println("PAC Report Header Text is ---->" + ActText);
	//String ExpText = "Cost Per Member for " + PracticeName + " " + "(" + PracticeTin + ")";
	//Assert.assertEquals(ExpText, ActText);
	}

@Test(priority=4)
public void VerifyHeaderBenchMark() {
	
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	//navigate to cost per member page
	driver.findElement(benchmarkTab).click();
	
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	//Verify table header
	
	String ActText = driver.findElement(TableHeader).getText();
	System.out.println("Benchmark Header Text is ---->" + ActText);
	//String ExpText = "Cost Per Member for " + PracticeName + " " + "(" + PracticeTin + ")";
	//Assert.assertEquals(ExpText, ActText);
	}
}
