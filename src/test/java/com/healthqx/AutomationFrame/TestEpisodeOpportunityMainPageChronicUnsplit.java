/**
 * 
 */
package com.healthqx.AutomationFrame;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityFilter;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPage;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPageTableValuesRiskOff;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPageTableValuesRiskOn;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunitySubPage;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityTableHeaders;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.Verify;

/**
 * @author enebo4f
 *
 */
public class TestEpisodeOpportunityMainPageChronicUnsplit {

	WebDriver driver;

	Environment env;
	BrowserTypes  browser;
	
	DataBaseConnector dbConnector;
	private String episodeName;
	private String totalTypicalWithCompCostList;
	private String totalTypicalCostList;
	private String totalCostList;
	private String totalPacCostList;
	private String uITotalEpisodeCountList ;
	private String uIAverageTypicalCostList;
	private String uIAverageTypicalWithCompCostList;
	private String uIAveragePacCostList;
	private String uIAverageTotalCostList;

	//@BeforeSuite
	@BeforeClass
		@Parameters({"browser","environment"})
		public void beforeTest(String browserType,String environment) {
			ConfigFactory.setProperty("browser", browserType);
			browser = ConfigFactory.create(BrowserTypes.class);
			
			ConfigFactory.setProperty("env", environment);
			env = ConfigFactory.create(Environment.class);
			driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
			
			Login signIn = new Login(driver);
			signIn.loginToApp(env.username(), env.password());
			
			//creating DB Conection
			dbConnector= new DataBaseConnector(env.dbEnv(),env.dbUrlDqx());
			dbConnector.openConnection();
			
		}

	@Test(description = "Navigate to Episode Opportunity-Chronic UnSplit", priority = 1,groups={"Category:Chronic, Cost Type:UnSplit"})
	public void navigateToEpisodeOpportunityChronicUnsplit() {

		//NavigationTiles navigate = new NavigationTiles(driver);
		NavigationHeader navigation = new NavigationHeader(driver);
		//navigate.navigateToEpisodeOpportunity();
		navigation.navigateToEpisodeOpportunityFromDropDown();
		Assert.assertTrue(navigation.getSubModuleBreadCrumb().getText().contains("Episode Opportunity"));
		System.out.println("bread crumb text is --->>> "+navigation.getSubModuleBreadCrumb().getText());
	}

	@Test(description = "Total Costs pie chart should be 100%- Chronic UnSplit", priority = 2,groups={"Category:Chronic, Cost Type:UnSplit"})
	public void verifyChronicOnlyCostChronicUnsplit() {

		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		Synchronize pause = new Synchronize();
		//clicking un upsplit to get unsplit cost data
		filter.getCostTypeUnsplit().click();
		pause.waitUntilElementPresent(1000);
		// Unchecking other categories, SRF is unchecked by default
		filter.getAcuteCheckBox().click();
		pause.waitUntilElementPresent(1000);
		filter.getOtherCheckBox().click();
		pause.waitUntilElementPresent(1000);
		filter.getProceduralCheckBox().click();
		pause.waitUntilElementPresent(1000);
		
		
		

		//setting a pause
			
				pause.waitUntilElementPresent(6000);
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		String expTotalCostPercent = "100.00%";
		String actTocalCostPercent = mainPage.getTotalCostOneCategory().getText();
		Assert.assertEquals(actTocalCostPercent, expTotalCostPercent);
	}

	@Test(description = "Member Counts pie chart should be 100%- Chronic UnSplit", priority = 3,groups={"Category:Chronic, Cost Type:UnSplit"})
	public void verifyChronicOnlyMemberCountChronicUnsplit() {
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);

		String expMemberCountPercent = "100.00%";
		String actMemberCountPercent = mainPage.getTotalMemberOneCategory().getText();
		Assert.assertEquals(actMemberCountPercent, expMemberCountPercent);
	}

	@Test(description = "Typical Cost on list should match with Total Typical Cost on table - Chronic UnSplit", priority =4,groups={"Category:Chronic, Cost Type:UnSplit"})
	public void verifyTypicalCostChronicUnsplit() {
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		String typicalCostFromList = mainPage.getTypicalCostFromList().getText().replace("Typical Cost:", "").replace("\n", "").trim();
		String typicalCostFromTable = mainPage.getTableTotalTypical().getText();
		Assert.assertEquals(typicalCostFromList, typicalCostFromTable);
	}

	@Test(description = "Typical WIth Comp Cost on list should match with Total Typical With Comp Cost in table - Chronic UnSplit", priority = 5,groups={"Category:Chronic, Cost Type:UnSplit"})
	public void verifyTypicalWithCompCostChronicUnsplit() {
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		String typicalWithCompFromList = mainPage.getTypicalWithCompCostFromList().getText().replace("Typical with Comp Cost:", "").replace("\n", "").trim();
		String typicalWithCompFromTable = mainPage.getTableTotalComp().getText();
		Assert.assertEquals(typicalWithCompFromList, typicalWithCompFromTable);
	}

	@Test(description = "PAC Cost on list should match with Total PAC Cost in table - Chronic UnSplit", priority = 6,groups={"Category:Chronic, Cost Type:UnSplit"})
	public void verifyPACCostChronicUnsplit() {
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		String pacCostFromList = mainPage.getPacCostFromList().getText().replace("PAC Cost:", "").replace("\n", "").trim();
		String pacCostFromTable = mainPage.getTableTotalPac().getAttribute("innerText");
		Assert.assertEquals(pacCostFromList, pacCostFromTable);
	}

	@Test(description = "Total Cost on list should matches with Total Cost in table - Chronic UnSplit", priority = 7,groups={"Category:Chronic, Cost Type:UnSplit"})
	public void verifyTotalCostChronicUnsplit() {
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		String actTotalCostFromList = mainPage.getTotalCostFromList().getText().replace("Cost:", "").replace("\n", "").trim();  
		String expTotalCostFromTable = mainPage.getTableTotalCost().getAttribute("innerText").replaceAll(".*: ", "");
		Assert.assertEquals(actTotalCostFromList, expTotalCostFromTable);
	}

	@Test(description = "data On table should match data in DB - Chronic UnSplit Risk On", priority = 8, groups={"Category:Chronic, Cost Type:UnSplit"})
	public void verifydataOnTableGridAgainstDbRiskOn() {
		EpisodeOpportunityMainPageTableValuesRiskOn episodeOpportunityMainPageTableValuesRiskOn = new EpisodeOpportunityMainPageTableValuesRiskOn(driver);
		List<EpisodeOpportunityTableHeaders> rowList = episodeOpportunityMainPageTableValuesRiskOn.rowValues();
		for(EpisodeOpportunityTableHeaders row: rowList) {
			//Verify Member Counts for an episode
			String memberCountsQuery = "select MemberCount FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			
			BigDecimal memberCountDb = new BigDecimal(dbConnector.executeQuerySingleValue(memberCountsQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getMemberCount(), memberCountDb, "member count does not match for " + row.getEpisodeName() + " episode and query is = " + memberCountsQuery );
		
			//Verify Providers count for an episode against Db
			String providerCountQuery = "select providerCount FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			
			BigDecimal providerCountDb = new BigDecimal(dbConnector.executeQuerySingleValue(providerCountQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getProviderCount(), providerCountDb, "provider count does not match for " + row.getEpisodeName() + " episode and query is = " + providerCountQuery);
		
			//Verify Average Typical cost for an episode against Db
			String averageTypicalQuery = "select AverageTypicalCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			
			BigDecimal averageTypicalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageTypicalQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageTypicalCost(), averageTypicalCostDb,"Average Typical Cost does not match for " + row.getEpisodeName() + " episode and query is = " + averageTypicalQuery );
			
			//Verify Average Typical Complication Cost against Db
			String averageCompQuery = "select AverageTypicalComplicationCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal averageTypicalCompCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCompQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageTypicalComplicationCost(), averageTypicalCompCostDb, "Average Typical Complication Cost does not match for " + row.getEpisodeName() + " episode and query is = " + averageCompQuery);
			
			//Verify Average PAC Cost against Db
			String averagePacQuery = "select AveragePacCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;" ;
			BigDecimal averagePacCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averagePacQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAveragePacCost(), averagePacCostDb,"Average Pac Cost does not match for " + row.getEpisodeName() + " episode and query is = " + averagePacQuery );
			
			//Verify Average Cost against Db
			String averageCostQuery = "select AverageCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal averageCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCostQuery)).setScale(0,RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageCost(), averageCostDb, "Average Cost does not match for " + row.getEpisodeName() + " episode and query is = " + averageCostQuery);
			
			//Verify Risk Average against Db
			String riskAverageQuery = "select RiskAdjustedAverageCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal riskAverageCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(riskAverageQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getRiskAdjustedAverageCost(), riskAverageCostDb, "Risk Adjusted Average Cost does not match for " + row.getEpisodeName() + " episode and query is = " + riskAverageQuery);
			
			//Verify Total Typical Cost against Db
			String totalTypicalQuery = "select TotalTypicalCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal totalTypicalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(totalTypicalQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getTotalTypicalCost(), totalTypicalCostDb,"Total Typical Cost does not match for " + row.getEpisodeName() + " episode and query is = " + totalTypicalQuery );
			
			
			//Verify Total Typical with Complication Cost against Db
			String totalTypicalWCompQuery = "select TotalTypicalComplicationCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal totalTypicalWithCompCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(totalTypicalWCompQuery)).setScale(0,RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getTotalTypicalComplicationCost(), totalTypicalWithCompCostDb,"Total Typical Complication Cost does not match for " + row.getEpisodeName() + " episode and query is = " + totalTypicalWCompQuery);
			
			//Verify Total PAC Cost against Db
			String totalPacCostQuery = "select TotalPacCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal totalPacCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(totalPacCostQuery)).setScale(0,RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getTotalPacCost(), totalPacCostDb,"Total Pac Cost does not match for " + row.getEpisodeName() + " episode and query is = " + totalPacCostQuery );
			
			//Verify Total Cost against Db
			String totalCostQuery = "select TotalCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal totalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(totalCostQuery)).setScale(0,RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getTotalCost(), totalCostDb,"Total Cost does not match for " + row.getEpisodeName() + " episode and query is = " + totalCostQuery );
			
			//Verify Risk Total Cost against Db
			String riskTotalCostQuery = "select RiskAdjustedTotalCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal riskTotalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(riskTotalCostQuery)).setScale(0,RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getRiskAdjustedTotalCost(), riskTotalCostDb,"Risk Adjusted Total Cost does not match for " + row.getEpisodeName() + " episode and query is = " + riskTotalCostQuery );	
		}
	}
	
	@Test(description = "data On table should match data in DB - Chronic UnSplit Risk On", priority = 9, groups={"Category:Chronic, Cost Type:UnSplit"})
	public void verifydataOnTableGridAgainstDbRiskOff() {
		Synchronize wait = new Synchronize();
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		filter.getRiskAdjustToggleSwitch().click();
		wait.waitUntilElementPresent(3000);
		
		
		EpisodeOpportunityMainPageTableValuesRiskOff episodeOpportunityMainPageTableValuesRiskOff = new EpisodeOpportunityMainPageTableValuesRiskOff(driver);
		List<EpisodeOpportunityTableHeaders> rowList = episodeOpportunityMainPageTableValuesRiskOff.rowValues();
		
		for(EpisodeOpportunityTableHeaders row: rowList) {
			//Verify Member Counts for an episode
			String memberCountsQuery = "select MemberCount FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal memberCountDb = new BigDecimal(dbConnector.executeQuerySingleValue(memberCountsQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getMemberCount(), memberCountDb, "member count does not match for " + row.getEpisodeName() + " episode and query is = " + memberCountsQuery );
		
			//Verify Providers count for an episode against Db
			String providerCountQuery = "select providerCount FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal providerCountDb = new BigDecimal(dbConnector.executeQuerySingleValue(providerCountQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getProviderCount(), providerCountDb, "provider count does not match for " + row.getEpisodeName() + " episode and query is = " + providerCountQuery);
		
			//Verify Average Typical cost for an episode against Db
			String averageTypicalQuery = "select AverageTypicalCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal averageTypicalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageTypicalQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageTypicalCost(), averageTypicalCostDb,"Average Typical Cost does not match for " + row.getEpisodeName() + " episode and query is = " + averageTypicalQuery );
			
			//Verify Average Typical Complication Cost against Db
			String averageCompQuery = "select AverageTypicalComplicationCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal averageTypicalCompCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCompQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageTypicalComplicationCost(), averageTypicalCompCostDb, "Average Typical Complication Cost does not match for " + row.getEpisodeName() + " episode and query is = " + averageCompQuery);
			
			//Verify Average PAC Cost against Db
			String averagePacQuery = "select AveragePacCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;" ;
			BigDecimal averagePacCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averagePacQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAveragePacCost(), averagePacCostDb,"Average Pac Cost does not match for " + row.getEpisodeName() + " episode and query is = " + averagePacQuery );
			
			//Verify Average Cost against Db
			String averageCostQuery = "select AverageCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal averageCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(averageCostQuery)).setScale(0,RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getAverageCost(), averageCostDb, "Average Cost does not match for " + row.getEpisodeName() + " episode and query is = " + averageCostQuery);
			
			
			//Verify Total Typical Cost against Db
			String totalTypicalQuery = "select TotalTypicalCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal totalTypicalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(totalTypicalQuery)).setScale(0, RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getTotalTypicalCost(), totalTypicalCostDb,"Total Typical Cost does not match for " + row.getEpisodeName() + " episode and query is = " + totalTypicalQuery );
			
			
			//Verify Total Typical with Complication Cost against Db
			String totalTypicalWCompQuery = "select TotalTypicalComplicationCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal totalTypicalWithCompCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(totalTypicalWCompQuery)).setScale(0,RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getTotalTypicalComplicationCost(), totalTypicalWithCompCostDb,"Total Typical Complication Cost does not match for " + row.getEpisodeName() + " episode and query is = " + totalTypicalWCompQuery);
			
			//Verify Total PAC Cost against Db
			String totalPacCostQuery = "select TotalPacCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal totalPacCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(totalPacCostQuery)).setScale(0,RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getTotalPacCost(), totalPacCostDb,"Total Pac Cost does not match for " + row.getEpisodeName() + " episode and query is = " + totalPacCostQuery );
			
			//Verify Total Cost against Db
			String totalCostQuery = "select TotalCost FROM rpt.EpisodeOpportunityUnSplit where EcrRunId= " + env.EcrRunId() + " and levelId=1 and episodeName= " + "'"+ row.getEpisodeName() +"'"+ " and episodeCategoryName= " +"'"+ row.getEpisodeCategory() + "'" + " ;";
			BigDecimal totalCostDb = new BigDecimal(dbConnector.executeQuerySingleValue(totalCostQuery)).setScale(0,RoundingMode.HALF_UP);
			Verify.verifyEquals(row.getTotalCost(), totalCostDb,"Total Cost does not match for " + row.getEpisodeName() + " episode and query is = " + totalCostQuery );
		}
	}
	
	@Test(description = "Navigate To SubPage - Chronic UnSplit", priority = 10, groups={"Category:Acute, Cost Type:Split"})
	public void navigateToClinicalClassification() {
			
			EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
			NavigationHeader header = new NavigationHeader(driver);
			
			Synchronize pause = new Synchronize();
			pause.waitUntilElementPresent(1000);
			
			header.getTableQuickLink().click();
			
			
			
		if(mainPage.getFirstEpisodeFromTable().getText().contains("'")) {
			episodeName = mainPage.getFirstEpisodeFromTable().getText().replaceAll("'", "''");
		}
		else {
			episodeName = mainPage.getFirstEpisodeFromTable().getText();
		}
		
		
		String query = "SELECT distinct EpisodeAcronym FROM [rpt].EpisodeOpportunityUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName =" + "'" + episodeName + "'" ;
		String episodeAcronym = dbConnector.executeQuerySingleValue(query);
			
			pause.waitUntilElementPresent(1000);
			mainPage.getFirstEpisodeFromTable().click();
			pause.waitUntilElementPresent(2000);
			
			Assert.assertTrue(header.getSubPageBreadCrumb().getText().contains(episodeAcronym));
			
			
			
		}
	
	@Test (priority=11,description="User should be on Chronic UnSplit subpage by default- Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyUserOnClinicalClassification() {
		
EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);

boolean isEnabled = subPage.getClinicalClassificationTab().isEnabled();

Assert.assertEquals(isEnabled, true);	
	}
	@Test(priority=12,description="Average Typical Cost on List  should match with DB - Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyAverageTypicalCost() {
		
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(AverageTypicalCost),0)  ) as AverageTypicalCost FROM [rpt].EpisodeOpportunityUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName =" + "'" + episodeName + "'" ;
				
		uIAverageTypicalCostList = subPage.getAverageTypicalCost().getText().replaceAll("Average Typical Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim(); 	
		System.out.println("uIAverageTypicalCostList is ----> "+uIAverageTypicalCostList);
				String expAverageTypicalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");	
				Assert.assertEquals(uIAverageTypicalCostList, expAverageTypicalCostDB);	
			}
	
	@Test(priority=13,description="Average Typical Cost on List should match with DB - Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyAverageTypicalWithCompCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query="SELECT CONCAT ( '$', round(sum(AverageTypicalComplicationCost),0)  ) as AverageTypicalWithCompCost FROM [rpt].EpisodeOpportunityUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		
		
		 uIAverageTypicalWithCompCostList = subPage.getAverageTypicalWithCompCost().getText().replaceAll("Average Typical with Comp Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		
		String expAverageTypicalWithCompCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(uIAverageTypicalWithCompCostList, expAverageTypicalWithCompCostDB);
	}
	
	@Test(priority=14,description="Average Pac Cost on List should match with DB - Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyAveragePacCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query="SELECT CONCAT ( '$', round(sum(AveragePacCost),0)  ) as AveragePACCost FROM [rpt].EpisodeOpportunityUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		 uIAveragePacCostList = subPage.getAveragePacCost().getText().replaceAll("Average PAC Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expAveragePacCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		Assert.assertEquals(uIAveragePacCostList, expAveragePacCostDB);
	}
	
	@Test(priority=15,description="Average Total Cost on List should match with DB - Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyAverageTotalCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(AverageCost),0)  ) as AverageTotalCost FROM [rpt].EpisodeOpportunityUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		 uIAverageTotalCostList = subPage.getAverageTotalCost().getText().replaceAll("Average Total Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expAverageTotalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(uIAverageTotalCostList, expAverageTotalCostDB);
	}
	
	@Test(priority=16,description="Total Typical Cost on List should match with DB - Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyTotalTypicalCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String query = "SELECT CONCAT ( '$', round(sum(TotalTypicalCost),0)  ) as TotalTypicalCost FROM [rpt].EpisodeOpportunityCostBreakdownUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalTypicalCostList = subPage.getTotalTypicalCost().getText().replaceAll("Total Typical Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalTypicalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(totalTypicalCostList, expTotalTypicalCostDB);
	}
	
	@Test(priority=17,description="Total Typical With Comp Cost on List should match with DB - Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyTotalTypicalWithCompCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String query="SELECT CONCAT ( '$', round(sum(TotalTypicalComplicationCost),0)  ) as TotalTypicalWithCompCost FROM [rpt].EpisodeOpportunityCostBreakdownUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalTypicalWithCompCostList = subPage.getTotalTypicalWithCompCost().getText().replaceAll("Total Typical with Comp Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalTypicalWithCompCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		Assert.assertEquals(totalTypicalWithCompCostList, expTotalTypicalWithCompCostDB);
	}
	
	@Test(priority=18,description="Total PAC Cost on List shoudl match with DB - Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyTotalPacCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(TotalPacCost),0)  ) as TotalPACCost FROM [rpt].EpisodeOpportunityCostBreakdownUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalPacCostList = subPage.getTotalPacCost().getText().replaceAll("Total PAC Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalPacCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(totalPacCostList, expTotalPacCostDB);
	}
	
	@Test(priority=19,description="Total Cost on List should match with DB - Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyTotalCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(TotalCost),0)  ) as TotalCost FROM [rpt].EpisodeOpportunityCostBreakdownUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalCostList = subPage.getTotalCost().getText().replaceAll("Total Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(totalCostList, expTotalCostDB);
	}
	
	@Test(priority=20,description="Total Episode Count on List should match with DB - Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyTotalEpisodeCount() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String query = "SELECT   round(sum(EpisodeCount),0)  as TotalEpisodeCount FROM [rpt].EpisodeOpportunityUnSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'";
		uITotalEpisodeCountList = subPage.getTotalEpisodeCount().getText().replaceAll("Total Episode Count:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalEpisodeCountDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(uITotalEpisodeCountList, expTotalEpisodeCountDB);
	}
	
	@Test(priority=21,description="Total Typical Cost on List should match with Grid", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyTotalTypicalListWGrid() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String totalTypicalGrid = subPage.getTotalTypicalGrid().getAttribute("innerText").replaceAll(",", "");
		
		Assert.assertEquals(totalTypicalCostList, totalTypicalGrid);
		
		
	}
	
	@Test(priority=22,description="Total Typical With Comp Cost on List should match with Grid", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyTotalCompListWGrid() {
		
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String totalTypicalWithCompGrid = subPage.getTotalTypicalWithCompGrid().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalTypicalWithCompCostList, totalTypicalWithCompGrid);
		
		
		
	}
	
	@Test(priority=23,description="Total Pac Cost on List should match with Grid", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyTotalPacListWGrid() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String totalPacCostGrid = subPage.getTotalPacGrid().getAttribute("innerText").replaceAll(",", "");
		
		Assert.assertEquals(totalPacCostList, totalPacCostGrid);
		
		
	}
	
	@Test(priority=24,description="Total Cost on List should match with Grid", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyTotalListWGrid() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String totalCostGrid = subPage.getTotalCostGrid().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalCostList, totalCostGrid);
	}
	
	@Test(priority=25, description = "Average Typical on List should equal Total Typical Cost divided by the Episode Count- Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyAverageTypCostCalculation() {
		 BigDecimal averageTypicalCost = new BigDecimal(uIAverageTypicalCostList.replace("$", ""));
		 BigDecimal totalTypicalCostListToCompare = new BigDecimal(totalTypicalCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCountToCompare = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 BigDecimal calculatedAverageTypCost =  totalTypicalCostListToCompare.divide(uITotalEpisodeCountToCompare,0,RoundingMode.HALF_UP); 
		 
		 Verify.verifyEquals(calculatedAverageTypCost,averageTypicalCost);
		 
		 
	}
	
	@Test(priority=26, description = "Average Typical With Comp on List should equal Total Typical With Comp Cost divided by the Episode Count- Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyAverageTypWithCompCostCalculation() {
		 BigDecimal averageTypWCompCost = new BigDecimal(uIAverageTypicalWithCompCostList.replace("$", ""));
		 BigDecimal totalTypWCompCostList = new BigDecimal(totalTypicalWithCompCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCount = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 BigDecimal calculatedAverageTotTypWCompCost =  totalTypWCompCostList.divide(uITotalEpisodeCount,0,RoundingMode.HALF_UP);
		 Verify.verifyEquals(calculatedAverageTotTypWCompCost,averageTypWCompCost); 
	}
	
	@Test(priority=27, description = "Average PAC Cost on List should equal Total PAC Cost divided by the Episode Count- Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyAveragePacCostCalculation() {
		 BigDecimal averagePacCost = new BigDecimal(uIAveragePacCostList.replace("$", ""));
		 BigDecimal totalPACCostList = new BigDecimal(totalPacCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCount = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 
		 
		 BigDecimal calculatedAveragePacCost =  totalPACCostList.divide(uITotalEpisodeCount,0,RoundingMode.HALF_UP);
		 
		 
		 Verify.verifyEquals(calculatedAveragePacCost,averagePacCost);
		 
		 
	}
	
	@Test(priority=28, description = "Average Total Cost on List should equal Total Cost divided by the Episode Count- Chronic UnSplit", groups= {"Episode Opportunity SubPage - Chronic UnSplit"})
	public void verifyAverageTotalCostCalculation() {
		 BigDecimal averageTotalCost = new BigDecimal(uIAverageTotalCostList.replace("$", ""));
		
		 BigDecimal totCostList = new BigDecimal(totalCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCount = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 BigDecimal calculatedAveragePacCost =  totCostList.divide(uITotalEpisodeCount,0,RoundingMode.HALF_UP);  
		 Verify.verifyEquals(calculatedAveragePacCost,averageTotalCost);
	}

	@AfterSuite()
	//@AfterClass
	public void closeBrowser() {
		
		dbConnector.closeConnection();
		
		LogOut logout = new LogOut(driver);
		logout.logOff();
	}

}
