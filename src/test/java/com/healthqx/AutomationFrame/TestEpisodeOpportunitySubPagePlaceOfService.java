package com.healthqx.AutomationFrame;


import java.math.BigDecimal;
import java.math.RoundingMode;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPage;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunitySubPage;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.Verify;

public class TestEpisodeOpportunitySubPagePlaceOfService {
	WebDriver driver;
	DataBaseConnector dbConnector;
private String episodeName;  
Environment env;
BrowserTypes  browser;

/*private String totalTypicalWithCompCostList;
private String totalTypicalCostList;
private String totalCostList;
private String totalPacCostList;*/

private String totalTypicalWithCompCostList;
private String totalTypicalCostList;
private String totalCostList;
private String totalPacCostList;
private String uITotalEpisodeCountList ;
private String uIAverageTypicalCostList;
private String uIAverageTypicalWithCompCostList;
private String uIAveragePacCostList;
private String uIAverageTotalCostList;




//@BeforeSuite
@BeforeClass
@Parameters({"browser","environment"})
public void beforeTest(String browserType,String environment) {
	
	ConfigFactory.setProperty("browser", browserType);
	browser = ConfigFactory.create(BrowserTypes.class);
	ConfigFactory.setProperty("env", environment);
	env = ConfigFactory.create(Environment.class);
	driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
	
	Login signIn = new Login(driver);
	signIn.loginToApp(env.username(), env.password());
	
	//creating DB Connection
	dbConnector= new DataBaseConnector(env.dbEnv(),env.dbUrlDqx());
	dbConnector.openConnection();
}
	
	@Test(priority=1,description="Navigate to the Episode Opportunity Sub Page - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void preCondition() {
		//NavigationTiles navigate = new NavigationTiles(driver);
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		NavigationHeader header = new NavigationHeader(driver);
		//navigate.navigateToEpisodeOpportunity();
		header.navigateToEpisodeOpportunityFromDropDown();
		Synchronize pause = new Synchronize();
		pause.waitUntilElementPresent(1000);
		header.getTableQuickLink().click();
		
		episodeName = mainPage.getFirstEpisodeFromTable().getText();
		pause.waitUntilElementPresent(1000);
		mainPage.getFirstEpisodeFromTable().click();
		pause.waitUntilElementPresent(1000);
		Assert.assertTrue(header.getSubPageBreadCrumb().getText().contains(episodeName));
		
	}
		
	
	@Test (priority=2,description="Navigate To Place Of Service tab- Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyUserOnClinicalClassification() {
		
EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
subPage.getPlaceOfServiceTab().click();
boolean isEnabled = subPage.getPlaceOfServiceTab().isEnabled();
Assert.assertEquals(isEnabled, true);	
	}
	
	@Test(priority=3,description="Average Typical Cost on List  should match with DB - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyAverageTypicalCostCC() {
		
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(AverageTypicalCost),0)  ) as AverageTypicalCost FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName =" + "'" + episodeName + "'" ;
				
		 uIAverageTypicalCostList = subPage.getAverageTypicalCost().getText().replaceAll("Average Typical Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim(); 	
				String expAverageTypicalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");	
				Assert.assertEquals(uIAverageTypicalCostList, expAverageTypicalCostDB);	
			}
	
	@Test(priority=4,description="Average Typical Cost on List should match with DB - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyAverageTypicalWithCompCostCC() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query="SELECT CONCAT ( '$', round(sum(AverageTypicalComplicationCost),0)  ) as AverageTypicalWithCompCost FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		
		
		 uIAverageTypicalWithCompCostList = subPage.getAverageTypicalWithCompCost().getText().replaceAll("Average Typical with Comp Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		
		String expAverageTypicalWithCompCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(uIAverageTypicalWithCompCostList, expAverageTypicalWithCompCostDB);
	}
	
	@Test(priority=5,description="Average Pac Cost on List should match with DB - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyAveragePacCostCC() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query="SELECT CONCAT ( '$', round(sum(AveragePacCost),0)  ) as AveragePACCost FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		 uIAveragePacCostList = subPage.getAveragePacCost().getText().replaceAll("Average PAC Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expAveragePacCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		Assert.assertEquals(uIAveragePacCostList, expAveragePacCostDB);
	}
	
	@Test(priority=6,description="Average Total Cost on List should match with DB - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyAverageTotalCostCC() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(AverageCost),0)  ) as AverageTotalCost FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		 uIAverageTotalCostList = subPage.getAverageTotalCost().getText().replaceAll("Average Total Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expAverageTotalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(uIAverageTotalCostList, expAverageTotalCostDB);
	}
	
	@Test(priority=7,description="Total Typical Cost on List should match with DB - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyTotalTypicalCostCC() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String query = "SELECT CONCAT ( '$', round(sum(TotalTypicalCost),0)  ) as TotalTypicalCost FROM [rpt].EpisodeOpportunityCostBreakdownSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalTypicalCostList = subPage.getTotalTypicalCost().getText().replaceAll("Total Typical Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalTypicalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(totalTypicalCostList, expTotalTypicalCostDB);
	}
	
	@Test(priority=8,description="Total Typical With Comp Cost on List should match with DB - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyTotalTypicalWithCompCostCC() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String query="SELECT CONCAT ( '$', round(sum(TotalTypicalComplicationCost),0)  ) as TotalTypicalWithCompCost FROM [rpt].EpisodeOpportunityCostBreakdownSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalTypicalWithCompCostList = subPage.getTotalTypicalWithCompCost().getAttribute("innerText").replaceAll("Total Typical with Comp Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalTypicalWithCompCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		Assert.assertEquals(totalTypicalWithCompCostList, expTotalTypicalWithCompCostDB);
	}
	
	@Test(priority=9,description="Total PAC Cost on List shoudl match with DB - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyTotalPacCostCC() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(TotalPacCost),0)  ) as TotalPACCost FROM [rpt].EpisodeOpportunityCostBreakdownSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalPacCostList = subPage.getTotalPacCost().getText().replaceAll("Total PAC Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalPacCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(totalPacCostList, expTotalPacCostDB);
	}
	
	@Test(priority=10,description="Total Cost on List should match with DB - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyTotalCostCC() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(TotalCost),0)  ) as TotalCost FROM [rpt].EpisodeOpportunityCostBreakdownSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalCostList = subPage.getTotalCost().getText().replaceAll("Total Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(totalCostList, expTotalCostDB);
	}
	
	@Test(priority=11,description="Total Episode Count on List should match with DB - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyTotalEpisodeCountCC() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String query = "SELECT   round(sum(EpisodeCount),0)  as TotalEpisodeCount FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'";
		 uITotalEpisodeCountList = subPage.getTotalEpisodeCount().getText().replaceAll("Total Episode Count:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalEpisodeCountDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(uITotalEpisodeCountList, expTotalEpisodeCountDB);
	}
	
	@Test(priority=12,description="Total Typical Cost on List should match with Grid - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyTotalTypicalListWGrid() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String totalTypicalGrid = subPage.getTotalTypicalGrid().getAttribute("innerText").replaceAll(",", "");
		
		Assert.assertEquals(totalTypicalCostList, totalTypicalGrid);
		
		
	}
	
	@Test(priority=13,description="Total Typical With Comp Cost on List should match with Grid - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyTotalCompListWGrid() {
		
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String totalTypicalWithCompGrid = subPage.getTotalTypicalWithCompGrid().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalTypicalWithCompCostList, totalTypicalWithCompGrid);
		
	}
	
	@Test(priority=14,description="Total Pac Cost on List should match with Grid - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyTotalPacListWGrid() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String totalPacCostGrid = subPage.getTotalPacGrid().getAttribute("innerText").replaceAll(",", "");
		
		Assert.assertEquals(totalPacCostList, totalPacCostGrid);
		
		
	}
	
	@Test(priority=15,description="Total Cost on List should match with Grid - Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyTotalListWGrid() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String totalCostGrid = subPage.getTotalCostGrid().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalCostList, totalCostGrid);
	}
	
	@Test(priority=16, description = "Avergae Typical on List should equal Total Typical Cost divided by the Episode Count- Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyAverageTypCostCalculation() {
		 BigDecimal averageTypicalCost = new BigDecimal(uIAverageTypicalCostList.replace("$", ""));
		 BigDecimal totalTypicalCostListToCompare = new BigDecimal(totalTypicalCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCountToCompare = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 BigDecimal calculatedAverageTypCost =  totalTypicalCostListToCompare.divide(uITotalEpisodeCountToCompare,0,RoundingMode.HALF_UP); 
		 
		 Verify.verifyEquals(calculatedAverageTypCost,averageTypicalCost);
	}
	
	@Test(priority=17, description = "Avergae Typical With Comp on List should equal Total Typical With Comp Cost divided by the Episode Count- Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyAverageTypWithCompCostCalculation() {
		 BigDecimal averageTypWCompCost = new BigDecimal(uIAverageTypicalWithCompCostList.replace("$", ""));
		 BigDecimal totalTypWCompCostList = new BigDecimal(totalTypicalWithCompCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCount = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 BigDecimal calculatedAverageTotTypWCompCost =  totalTypWCompCostList.divide(uITotalEpisodeCount,0,RoundingMode.HALF_UP);
		 Verify.verifyEquals(calculatedAverageTotTypWCompCost,averageTypWCompCost); 
	}
	
	@Test(priority=18, description = "Avergae PAC Cost on List should equal Total PAC Cost divided by the Episode Count- Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyAveragePacCostCalculation() {
		 BigDecimal averagePacCost = new BigDecimal(uIAveragePacCostList.replace("$", ""));
		 BigDecimal totalPACCostList = new BigDecimal(totalPacCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCount = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 
		 
		 BigDecimal calculatedAveragePacCost =  totalPACCostList.divide(uITotalEpisodeCount,0);
		 
		 
		 Verify.verifyEquals(calculatedAveragePacCost,averagePacCost);
	}
	
	@Test(priority=19, description = "Avergae Total Cost on List should equal Total Cost divided by the Episode Count- Place Of Service", groups= {"Episode Opportunity SubPage - Place Of Service"})
	public void verifyAverageTotalCostCalculation() {
		 BigDecimal averageTotalCost = new BigDecimal(uIAverageTotalCostList.replace("$", ""));
		 BigDecimal totCostList = new BigDecimal(totalCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCount = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 BigDecimal calculatedAveragePacCost =  totCostList.divide(uITotalEpisodeCount,0);  
		 Verify.verifyEquals(calculatedAveragePacCost,averageTotalCost);
	}
	
	@AfterSuite()
	//@AfterClass
	public void tearDown() {
		LogOut logout = new LogOut(driver);
		logout.logOff();
		
		dbConnector.closeConnection();
	}
	
	
}
