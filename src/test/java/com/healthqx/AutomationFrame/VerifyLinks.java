package com.healthqx.AutomationFrame;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPage;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.LinkUtil;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.Verify;

public class VerifyLinks {
	
	WebDriver driver;
	Environment env;
	BrowserTypes  browser;

	
	
	//@BeforeSuite
	@BeforeClass
	//@Parameters({"browser","environment"})
	public void beforeTest() {
		Synchronize waits = new Synchronize();
		driver = StartUpBrowser.StartBrowser("chrome", "https://anthem-sit.clarityqx.com"); 
		
		driver.findElement(By.xpath("//*[@id=\"bySelection\"]/div[4]/div/span")).click();
		
		driver.findElement(By.xpath("//*[@id=\"userNameInput\"]")).sendKeys(".\\Alamichhane");
		driver.findElement(By.xpath("//*[@id=\"passwordInput\"]")).sendKeys("Password01!");
		driver.findElement(By.xpath("//*[@id=\"submitButton\"]")).click();
		waits.waitUntilElementPresent(3000);
		
		
		/*ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
		
		Login signIn = new Login(driver);
		signIn.loginToApp(env.username(), env.password());*/
		
	}
	 
	
	
	@Test(priority=1,description="Test Links Main Page")
	public void verifyMainPageLinks()
	{
		Map<Boolean, List<String>> map= driver.findElements(By.xpath("//*[@href]"))  // find all elements which has href attribute
                .stream()
                .map(ele -> ele.getAttribute("href"))   // get the value of href
                .map(String::trim)                      // trim the text
                .distinct()                             // there could be duplicate links , so find unique
                .collect(Collectors.partitioningBy(link -> LinkUtil.getResponseCode(link) == 200)); // partition based on response code
	
		

map.get(false)
 .stream()
 .forEach(System.out::println);
    } 
	
	
	@Test(priority=2,description="Test Links EO Main Page")
	public void verifyEpisodeOpportunityMainPageLinks()
	{
		NavigationHeader nav = new NavigationHeader(driver);
		
		Synchronize waits = new Synchronize();
		
		nav.navigateToEpisodeOpportunityFromDropDown();
		
		waits.waitUntilElementPresent(3000);
		
		Map<Boolean, List<String>> map= driver.findElements(By.xpath("//*[@href]"))  // find all elements which has href attribute
                .stream()
                .map(ele -> ele.getAttribute("href"))   // get the value of href
                .map(String::trim)                      // trim the text
                .distinct()                             // there could be duplicate links , so find unique
                .collect(Collectors.partitioningBy(link -> LinkUtil.getResponseCode(link) == 200)); // partition based on response code
	

		

map.get(false)
 .stream()
 .forEach(System.out::println);
    }
	
	@Test(priority=3,description="Test Links EO Sub Page")
	public void verifyEpisodeOpportunitySubPageLinks()
	{
		NavigationHeader nav = new NavigationHeader(driver);
		
		Synchronize waits = new Synchronize();
		
		//nav.navigateToEpisodeOpportunityFromDropDown();
		
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		
		Synchronize pause = new Synchronize();
		pause.waitUntilElementPresent(1000);
		
		nav.getTableQuickLink().click();
		
		pause.waitUntilElementPresent(1000);
		mainPage.getFirstEpisodeFromTable().click();
		
		waits.waitUntilElementPresent(3000);
		
		Map<Boolean, List<String>> map= driver.findElements(By.xpath("//*[@href]"))  // find all elements which has href attribute
                .stream()
                .map(ele -> ele.getAttribute("href"))   // get the value of href
                .map(String::trim)                      // trim the text
                .distinct()                             // there could be duplicate links , so find unique
                .collect(Collectors.partitioningBy(link -> LinkUtil.getResponseCode(link) == 200)); // partition based on response code
		

map.get(false)
 .stream()
 .forEach(System.out::println);;
    }
	
	
	@Test(priority=4,description="Test Links Network Analysis Main Page")
	public void verifyNetworkAnalysisMainPageLinks()
	{
		NavigationHeader nav = new NavigationHeader(driver);
		
		Synchronize waits = new Synchronize();
		
		nav.navigateToNetworkAnalysisFromDropDown();
		
		waits.waitUntilElementPresent(3000);
		
		Map<Boolean, List<String>> map= driver.findElements(By.xpath("//*[@href]"))  // find all elements which has href attribute
                .stream()
                .map(ele -> ele.getAttribute("href"))   // get the value of href
                .map(String::trim)                      // trim the text
                .distinct()                             // there could be duplicate links , so find unique
                .collect(Collectors.partitioningBy(link -> LinkUtil.getResponseCode(link) == 200)); // partition based on response code
	

		

map.get(false)
 .stream()
 .forEach(System.out::println);
    }
	

}