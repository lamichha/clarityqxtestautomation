/**
 * 
 */
package com.healthqx.AutomationFrame;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;

/**
 * @author Ashish L.
 *
 */
public class VerifyNavigationBar {
	WebDriver driver;
	NavigationHeader nav = new NavigationHeader(driver);	
	
	@BeforeSuite
	// StartUp browser by calling utility method
	public void openBrowser() {
		driver = StartUpBrowser.StartBrowser("IE",
				"https://horizon-qa.clarityqx.com");
		//Call login method to sign in
		Login login = new Login(driver);
		login.loginToApp("ashish.lamichhane@mckesson.com", "Mom01@momq");
	}

	@Test (priority=1, description="Verify \"HealthQX\" logo is present on Navigation Bar")
		public void verifyLogoPresent() {
		NavigationHeader nav = new NavigationHeader(driver);	
		WebElement logo = nav.getHealthQXLogo();
		Assert.assertEquals(true, logo.isDisplayed());
		}
	
	@Test(priority=2)
	public void verifyStrategicInsightsPresent() {
		NavigationHeader nav = new NavigationHeader(driver);
		WebElement strategicInsights = nav.getStrategicInsights();
		Assert.assertEquals(true, strategicInsights.isDisplayed());
	}
	
	
		
	
	
	@Test(priority=9)
	public void logout() {
			
		LogOut loggingOff = new LogOut(driver);
		loggingOff.logOff();
	}
	
	

}
