/**
 * 
 */
package com.healthqx.AutomationFrame.NoRisk;



import java.math.BigDecimal;
import java.math.RoundingMode;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityFilter;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPage;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPageRiskOff;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunitySubPage;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.Verify;

/**
 * @author enebo4f
 *
 */
public class TestEpisodeOpportunityNoRiskOtherSplit {

	WebDriver driver;
	
	Environment env;
	BrowserTypes  browser;
	
	private String totalTypicalWithCompCostList;
	private String totalTypicalCostList;
	private String totalCostList;
	private String totalPacCostList;
	private String uITotalEpisodeCountList ;
	private String uIAverageTypicalCostList;
	private String uIAverageTypicalWithCompCostList;
	private String uIAveragePacCostList;
	private String uIAverageTotalCostList;
	private String episodeName; 
	
	DataBaseConnector dbConnector;
	
	//@BeforeSuite
	@BeforeClass
	@Parameters({"browser","environment"})
	public void beforeTest(String browserType,String environment) {
		
		ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
		
		
		Login signIn = new Login(driver);
		signIn.loginToApp(env.username(), env.password());
		
		//creating DB Connection
		dbConnector= new DataBaseConnector(env.dbEnv(),env.dbUrlDqx());
		dbConnector.openConnection();
		
		
	}
	

	@Test(description = "Navigate to Episode Opportunity-Other Split No Risk", priority = 1, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void navigateToEpisodeOpportunityChronicSplitRiskOff() {
		
		//NavigationTiles navigate = new NavigationTiles(driver);
		NavigationHeader navigation = new NavigationHeader(driver);
		
		//navigate.navigateToEpisodeOpportunity();
		navigation.navigateToEpisodeOpportunityFromDropDown();
		//Assert.assertTrue(navigation.getSubModuleBreadCrumb().getText().contains("Episode Opportunity"));
		 Verify.verifyTrue(navigation.getSubModuleBreadCrumb().getText().contains("Episode Opportunity"));
		
	}
	
	/*@Test(description="Turn Off Risk",priority=2)
	public void turnOffRisk() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		WebElement riskSwitch = filter.getRiskAdjustToggleSwitch();
		riskSwitch.click();
		System.out.println("Switch Text is " + riskSwitch.getText());
		
		
		WebElement riskOnSwitch = filter.getRiskAdjustOn();
		System.out.println("Risk On Switch is "+ riskOnSwitch.getText());
		
		WebElement riskOffSwitch = filter.getRiskAdjustOff();
		System.out.println("No Risk Switch is " + riskOffSwitch.getText());
		
		//riskOnSwitch.click();
		
		System.out.println(riskOnSwitch.getText() + " is No Risk ???");
	}*/
	
	

	@Test(description = "Total Costs pie chart should be 100%- Other Split No Risk", priority = 3, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyChronicOnlyCostChronicSplitRiskOff() {

		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		Synchronize pause = new Synchronize();
		// Unchecking other categories, SRF is unchecked by default
		filter.getAcuteCheckBox().click();
		pause.waitUntilElementPresent(1000);
		filter.getChronicCheckBox().click();
		//filter.getOtherCheckBox().click();
		pause.waitUntilElementPresent(1000);
		filter.getProceduralCheckBox().click();
		pause.waitUntilElementPresent(1000);

		//setting a pause
		
		pause.waitUntilElementPresent(6000);
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String expTotalCostPercent = "100.00%";
		
		
		String actTocalCostPercent = mainPage.getTotalCostOneCategory().getText();
		//Assert.assertEquals(actTocalCostPercent, expTotalCostPercent);
		Verify.verifyEquals(actTocalCostPercent, expTotalCostPercent);
		
	}

	@Test(description = "Member Counts pie chart should be 100%- Other Split No Risk", priority = 4, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyChronicOnlyMemberCountChronicSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);

		String expMemberCountPercent = "100.00%";
		String actMemberCountPercent = mainPage.getTotalMemberOneCategory().getText();
		//Assert.assertEquals(actMemberCountPercent, expMemberCountPercent);
		Verify.verifyEquals(actMemberCountPercent, expMemberCountPercent);
	}
	
	

	@Test(description = "Typical Cost on list should match with Total Typical Cost on table - Other Split No Risk", priority = 5, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTypicalCostChronicSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		//String typicalCostFromList = mainPage.getTypicalCostFromList().getText().replaceAll(".*: ", "");
		String typicalCostFromList = mainPage.getTypicalCostFromList().getText().replace("Typical Cost:", "").replace("\n", "").trim();
		String typicalCostFromTable = mainPage.getTableTotalTypical().getText();
		//Assert.assertEquals(typicalCostFromList, typicalCostFromTable);
		Verify.verifyEquals(typicalCostFromList, typicalCostFromTable);
	}

	@Test(description = "Typical WIth Comp Cost on list should match with Total Typical With Comp Cost in table - Other Split No Risk", priority = 6, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTypicalWithCompCostChronicSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String typicalWithCompFromList = mainPage.getTypicalWithCompCostFromList().getText().replace("Typical with Comp Cost:", "").replace("\n", "").trim();
		String typicalWithCompFromTable = mainPage.getTableTotalComp().getText();
		//Assert.assertEquals(typicalWithCompFromList, typicalWithCompFromTable);
		Verify.verifyEquals(typicalWithCompFromList, typicalWithCompFromTable);
	}

	@Test(description = "PAC Cost on list should match with Total PAC Cost in table - Other Split No Risk", priority = 7, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyPACCostChronicSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String pacCostFromList = mainPage.getPacCostFromList().getText().replace("PAC Cost:", "").replace("\n", "").trim();
		String pacCostFromTable = mainPage.getTableTotalPac().getText();
		//Assert.assertEquals(pacCostFromList, pacCostFromTable);
		Verify.verifyEquals(pacCostFromList, pacCostFromTable);
	}

	@Test(description = "Total Cost on list should matches with Total Cost in table - Other Split No Risk", priority = 8, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTotalCostChronicSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String actTotalCostFromList = mainPage.getTotalCostFromList().getText().replace("Cost:", "").replace("\n", "").trim();
		String expTotalCostFromTable = mainPage.getTableTotalCost().getAttribute("innerText").replaceAll(".*: ", "");
		//Assert.assertEquals(actTotalCostFromList, expTotalCostFromTable);
		Verify.verifyEquals(actTotalCostFromList, expTotalCostFromTable);
	}

	@Test(description = "Episode Counts on UI table should match with counts in DB - Other Split No Risk", priority = 9, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyEpisodeCountsChronicSplitRiskOff() {
		String query = "select count(EpisodeName) as EpisodeCounts FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + " and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE' and episodeName not like '%SUBTYPE';";
		
		String EpisodeCountFromDB = dbConnector.executeQuerySingleValue(query);
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String episodesCountsOnUI = mainPage.getTableTotalNumberOfEpisodes().getText().replaceAll(" Episodes", "");
		//Assert.assertEquals(EpisodeCountFromDB, episodesCountsOnUI);
		Verify.verifyEquals(EpisodeCountFromDB, episodesCountsOnUI);
	}

	@Test(description = "Member Counts on UI table should match with counts in DB - Other Split No Risk", priority = 10, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyMemberCountsChronicSplitRiskOff() {
		String query = "select sum(MemberCount) as TotalMemberCounts FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String membersCountFromDB = dbConnector.executeQuerySingleValue(query);
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String membersCountsOnUI = mainPage.getTableTotalNumberOfMembers().getText().replaceAll(",", "");
		//Assert.assertEquals(membersCountFromDB, membersCountsOnUI);
		Verify.verifyEquals(membersCountFromDB, membersCountsOnUI);
	}

	@Test(description = "Providers Counts on UI table should match with counts in DB - Other Split No Risk", priority = 11, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyProvidersCountsChronicSplitRiskOff() {
		String query = "select sum(ProviderCount) as TotalProviderCount FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String providersCountFromDB = dbConnector.executeQuerySingleValue(query);
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String providersCountsOnUI = mainPage.getTableTotalNumberOfProviders().getText().replaceAll(",", "");
		//Assert.assertEquals(providersCountFromDB, providersCountsOnUI);
		Verify.verifyEquals(providersCountFromDB, providersCountsOnUI);
	}

	@Test(description = "Total Average Typical Cost on UI table should match with DB - Other Split No Risk", priority = 12, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTotalAverageTypicalChronicSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(AverageTypicalCost)/count(EpisodeName),0)  ) as TotalAverageTypicalCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String totalAverageTypicalFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalAverageTypicalOnUI = mainPage.getTableTotalAverageTypical().getText().replaceAll(",", "");
		//Assert.assertEquals(totalAverageTypicalFromDB, totalAverageTypicalOnUI);
		Verify.verifyEquals(totalAverageTypicalFromDB, totalAverageTypicalOnUI);
	}

	@Test(description = "Total Average Typical Complication Cost on UI table should match with DB - Other Split No Risk", priority = 13, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTotalAverageCompChronicSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(AverageTypicalComplicationCost)/count(EpisodeName),0)  ) as TotalAverageTypicalComplicationCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String totalAverageCompFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalAverageCompOnUI = mainPage.getTableTotalAverageComp().getAttribute("innerText").replaceAll(",", "");
		//Assert.assertEquals(totalAverageCompFromDB, totalAverageCompOnUI);
		Verify.verifyEquals(totalAverageCompFromDB, totalAverageCompOnUI);
	}

	@Test(description = "Total Average PAC Cost on UI table should match with DB - Other Split No Risk", priority = 14, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTotalAveragePacChronicSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(AveragePacCost)/count(EpisodeName),0)  ) as TotalAveragePacCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String totalAveragePacFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalAveragePacOnUI = mainPage.getTableTotalAveragePac().getAttribute("innerText").replaceAll(",", "");
		//Assert.assertEquals(totalAveragePacFromDB, totalAveragePacOnUI);
		Verify.verifyEquals(totalAveragePacFromDB, totalAveragePacOnUI);
	}

	@Test(description = "Total Average  Cost on UI table should match with DB - Other Split No Risk", priority = 15, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTotalAverageChronicSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(AverageCost)/count(EpisodeName),0)  ) as TotalAverageCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String totalAverageFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalAverageOnUI = mainPage.getTableTotalAverage().getAttribute("innerText").replaceAll(",", "");
		//Assert.assertEquals(totalAverageFromDB, totalAverageOnUI);
		Verify.verifyEquals(totalAverageFromDB, totalAverageOnUI);
	}

	@Test(description = "Total Typical Cost on UI table should match with DB - Other Split No Risk", priority = 16, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTotalTypicalChronicSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(TotalTypicalCost),0)  )  as TotalTotalTypicalCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String totalTypicalFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalTypicalOnUI = mainPage.getTableTotalTypical().getAttribute("innerText").replaceAll(",", "");
		//Assert.assertEquals(totalTypicalFromDB, totalTypicalOnUI);
		Verify.verifyEquals(totalTypicalFromDB, totalTypicalOnUI);
	}

	@Test(description = "Total Typical With Complication Cost on UI table should match with DB - Other Split No Risk", priority = 17, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTotalCompChronicSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(TotalTypicalComplicationCost),0)  )  as TotalTotalTypicalComplicationCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String totalCompFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalCompOnUI = mainPage.getTableTotalComp().getAttribute("innerText").replaceAll(",", "");
		//Assert.assertEquals(totalCompFromDB, totalCompOnUI);
		Verify.verifyEquals(totalCompFromDB, totalCompOnUI);
	}

	@Test(description = "Total PAC Cost on UI table should match with DB - Other Split No Risk", priority = 18, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTotalPacChronicSplit() {
		String query = "select CONCAT ( '$', round(sum(TotalPacCost),0)  )  as TotalTotalPacCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String totalPacFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalPacOnUI = mainPage.getTableTotalPac().getAttribute("innerText").replaceAll(",", "");
		//Assert.assertEquals(totalPacFromDB, totalPacOnUI);
		Verify.verifyEquals(totalPacOnUI, totalPacFromDB);
	}

	@Test(description = "Total Cost on UI table should match with DB - Other Split No Risk", priority = 19, groups={"Category:Other, Cost Type:Split, No Risk"})
	public void verifyTotalCostAgainstDbChronicSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(TotalCost),0)  )  as TotalTotalCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Other' and episodeName not like '%SUBTYPE';";
		String totalCostFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalCostOnUI = mainPage.getTableTotalCost().getAttribute("innerText").replaceAll(",", "");
		//Assert.assertEquals(totalCostFromDB, totalCostOnUI);
		Verify.verifyEquals(totalCostFromDB, totalCostOnUI);
	}
	
	@Test(description = "Navigate To SubPage - Other Split No Risk", priority = 21, groups={"Category:Acute, Cost Type:Split"})
	public void navigateToClinicalClassification() {
			
			EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
			NavigationHeader header = new NavigationHeader(driver);
			
			Synchronize pause = new Synchronize();
			pause.waitUntilElementPresent(1000);
			
			header.getTableQuickLink().click();
			
			
			
		if(mainPage.getFirstEpisodeFromTable().getText().contains("'")) {
			episodeName = mainPage.getFirstEpisodeFromTable().getText().replaceAll("'", "''");
		}
		else {
			episodeName = mainPage.getFirstEpisodeFromTable().getText();
		}
		
		
		String query = "SELECT distinct EpisodeAcronym FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName =" + "'" + episodeName + "'" ;
		String episodeAcronym = dbConnector.executeQuerySingleValue(query);
			
			pause.waitUntilElementPresent(1000);
			mainPage.getFirstEpisodeFromTable().click();
			pause.waitUntilElementPresent(1000);
			
			Assert.assertTrue(header.getSubPageBreadCrumb().getText().contains(episodeAcronym));
			
			
			
		}
	
	@Test (priority=22,description="User should be on Other Split No Risk subpage by default- Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyUserOnClinicalClassification() {
		
EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
boolean isEnabled = subPage.getClinicalClassificationTab().isEnabled();
Assert.assertEquals(isEnabled, true);	
	}
	@Test(priority=23,description="Average Typical Cost on List  should match with DB - Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyAverageTypicalCost() {
		
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(AverageTypicalCost),0)  ) as AverageTypicalCost FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName =" + "'" + episodeName + "'" ;
				
		uIAverageTypicalCostList = subPage.getAverageTypicalCost().getText().replaceAll("Average Typical Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim(); 	
		System.out.println("uIAverageTypicalCostList is ----> "+uIAverageTypicalCostList);
				String expAverageTypicalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");	
				Assert.assertEquals(uIAverageTypicalCostList, expAverageTypicalCostDB);	
			}
	
	@Test(priority=24,description="Average Typical Cost on List should match with DB - Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyAverageTypicalWithCompCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query="SELECT CONCAT ( '$', round(sum(AverageTypicalComplicationCost),0)  ) as AverageTypicalWithCompCost FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		
		
		 uIAverageTypicalWithCompCostList = subPage.getAverageTypicalWithCompCost().getText().replaceAll("Average Typical with Comp Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		
		String expAverageTypicalWithCompCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(uIAverageTypicalWithCompCostList, expAverageTypicalWithCompCostDB);
	}
	
	@Test(priority=25,description="Average Pac Cost on List should match with DB - Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyAveragePacCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query="SELECT CONCAT ( '$', round(sum(AveragePacCost),0)  ) as AveragePACCost FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		 uIAveragePacCostList = subPage.getAveragePacCost().getText().replaceAll("Average PAC Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expAveragePacCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		Assert.assertEquals(uIAveragePacCostList, expAveragePacCostDB);
	}
	
	@Test(priority=26,description="Average Total Cost on List should match with DB - Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyAverageTotalCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(AverageCost),0)  ) as AverageTotalCost FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		 uIAverageTotalCostList = subPage.getAverageTotalCost().getText().replaceAll("Average Total Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expAverageTotalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(uIAverageTotalCostList, expAverageTotalCostDB);
	}
	
	@Test(priority=27,description="Total Typical Cost on List should match with DB - Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyTotalTypicalCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String query = "SELECT CONCAT ( '$', round(sum(TotalTypicalCost),0)  ) as TotalTypicalCost FROM [rpt].EpisodeOpportunityCostBreakdownSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalTypicalCostList = subPage.getTotalTypicalCost().getText().replaceAll("Total Typical Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalTypicalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(totalTypicalCostList, expTotalTypicalCostDB);
	}
	
	@Test(priority=28,description="Total Typical With Comp Cost on List should match with DB - Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyTotalTypicalWithCompCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String query="SELECT CONCAT ( '$', round(sum(TotalTypicalComplicationCost),0)  ) as TotalTypicalWithCompCost FROM [rpt].EpisodeOpportunityCostBreakdownSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalTypicalWithCompCostList = subPage.getTotalTypicalWithCompCost().getText().replaceAll("Total Typical with Comp Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalTypicalWithCompCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		Assert.assertEquals(totalTypicalWithCompCostList, expTotalTypicalWithCompCostDB);
	}
	
	@Test(priority=29,description="Total PAC Cost on List shoudl match with DB - Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyTotalPacCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(TotalPacCost),0)  ) as TotalPACCost FROM [rpt].EpisodeOpportunityCostBreakdownSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalPacCostList = subPage.getTotalPacCost().getText().replaceAll("Total PAC Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalPacCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(totalPacCostList, expTotalPacCostDB);
	}
	
	@Test(priority=30,description="Total Cost on List should match with DB - Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyTotalCost() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String query = "SELECT CONCAT ( '$', round(sum(TotalCost),0)  ) as TotalCost FROM [rpt].EpisodeOpportunityCostBreakdownSplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'" ;
		totalCostList = subPage.getTotalCost().getText().replaceAll("Total Cost:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalCostDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(totalCostList, expTotalCostDB);
	}
	
	@Test(priority=31,description="Total Episode Count on List should match with DB - Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyTotalEpisodeCount() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String query = "SELECT   round(sum(EpisodeCount),0)  as TotalEpisodeCount FROM [rpt].EpisodeOpportunitySplit where  "
				+ "EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and  EpisodeName = " + "'" + episodeName + "'";
		uITotalEpisodeCountList = subPage.getTotalEpisodeCount().getText().replaceAll("Total Episode Count:", "").replaceAll("\n", "").replaceAll(",", "").trim();
		String expTotalEpisodeCountDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		Assert.assertEquals(uITotalEpisodeCountList, expTotalEpisodeCountDB);
	}
	
	@Test(priority=32,description="Total Typical Cost on List should match with Grid", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyTotalTypicalListWGrid() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String totalTypicalGrid = subPage.getTotalTypicalGrid().getAttribute("innerText").replaceAll(",", "");
		
		Assert.assertEquals(totalTypicalCostList, totalTypicalGrid);
		
		
	}
	
	@Test(priority=33,description="Total Typical With Comp Cost on List should match with Grid", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyTotalCompListWGrid() {
		
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		
		String totalTypicalWithCompGrid = subPage.getTotalTypicalWithCompGrid().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalTypicalWithCompCostList, totalTypicalWithCompGrid);
		
		
		
	}
	
	@Test(priority=34,description="Total Pac Cost on List should match with Grid", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyTotalPacListWGrid() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String totalPacCostGrid = subPage.getTotalPacGrid().getAttribute("innerText").replaceAll(",", "");
		
		Assert.assertEquals(totalPacCostList, totalPacCostGrid);
		
		
	}
	
	@Test(priority=35,description="Total Cost on List should match with Grid", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyTotalListWGrid() {
		EpisodeOpportunitySubPage subPage = new EpisodeOpportunitySubPage(driver);
		String totalCostGrid = subPage.getTotalCostGrid().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalCostList, totalCostGrid);
	}
	
	@Test(priority=36, description = "Average Typical on List should equal Total Typical Cost divided by the Episode Count- Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyAverageTypCostCalculation() {
		 BigDecimal averageTypicalCost = new BigDecimal(uIAverageTypicalCostList.replace("$", ""));
		 BigDecimal totalTypicalCostListToCompare = new BigDecimal(totalTypicalCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCountToCompare = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 BigDecimal calculatedAverageTypCost =  totalTypicalCostListToCompare.divide(uITotalEpisodeCountToCompare,0,RoundingMode.HALF_UP); 
		 
		 Verify.verifyEquals(calculatedAverageTypCost,averageTypicalCost);
	}
	
	@Test(priority=37, description = "Average Typical With Comp on List should equal Total Typical With Comp Cost divided by the Episode Count- Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyAverageTypWithCompCostCalculation() {
		 BigDecimal averageTypWCompCost = new BigDecimal(uIAverageTypicalWithCompCostList.replace("$", ""));
		 BigDecimal totalTypWCompCostList = new BigDecimal(totalTypicalWithCompCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCount = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 BigDecimal calculatedAverageTotTypWCompCost =  totalTypWCompCostList.divide(uITotalEpisodeCount,0,RoundingMode.HALF_UP);
		 Verify.verifyEquals(calculatedAverageTotTypWCompCost,averageTypWCompCost); 
	}
	
	@Test(priority=38, description = "Average PAC Cost on List should equal Total PAC Cost divided by the Episode Count- Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyAveragePacCostCalculation() {
		 BigDecimal averagePacCost = new BigDecimal(uIAveragePacCostList.replace("$", ""));
		 BigDecimal totalPACCostList = new BigDecimal(totalPacCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCount = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 
		 
		 BigDecimal calculatedAveragePacCost =  totalPACCostList.divide(uITotalEpisodeCount,0,RoundingMode.HALF_UP);
		 
		 
		 Verify.verifyEquals(calculatedAveragePacCost,averagePacCost);
	}
	
	@Test(priority=39, description = "Average Total Cost on List should equal Total Cost divided by the Episode Count- Other Split No Risk", groups= {"Episode Opportunity SubPage - Other Split No Risk"})
	public void verifyAverageTotalCostCalculation() {
		 BigDecimal averageTotalCost = new BigDecimal(uIAverageTotalCostList.replace("$", ""));
		
		 BigDecimal totCostList = new BigDecimal(totalCostList.replace("$", ""));
		 BigDecimal uITotalEpisodeCount = new BigDecimal(uITotalEpisodeCountList.replace("$", "")); 
		 BigDecimal calculatedAveragePacCost =  totCostList.divide(uITotalEpisodeCount,0,RoundingMode.HALF_UP);  
		 Verify.verifyEquals(calculatedAveragePacCost,averageTotalCost);
	}


	@AfterSuite()
	//@AfterClass
	public void tearDown() {
			
		LogOut logout = new LogOut(driver);
		logout.logOff();
		dbConnector.closeConnection();
	}

}
