/**
 * 
 */
package com.healthqx.AutomationFrame;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.LoginPage;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;

/**
 * @author Ashish L.
 *
 *
 *This class contains test method to start up a browser and test a valid login
 */
public class VerifyValidLogin {
	
	WebDriver driver;
	Environment env;
	BrowserTypes  browser;
	//LoginPage loginPage = new LoginPage(driver);
	
	@BeforeSuite
	@Parameters({"browser","environment"})
	public void beforeTest(String browserType,String environment) {
		
		ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
		
		Login signIn = new Login(driver);
		signIn.loginToApp(env.username(), env.password());
		
	}
	
	@Test(priority=1)
	public void verifyWelcomeTextPresent() {
		LoginPage loginPage = new LoginPage(driver);
		WebElement welcomeText = loginPage.getWelcomeText();
		Assert.assertEquals(true, welcomeText.isDisplayed());
	}
	
	@Test(priority=2)
	public void verifyLegalTermsPresent() {
		LoginPage loginPage = new LoginPage(driver);
		WebElement disclaimerTexts = loginPage.getDisclaimerTexts();
		Assert.assertEquals(true, disclaimerTexts.isDisplayed());
	}
	
@Test(priority=3)
public void verifytermsOfUseDisplayed() {
	LoginPage loginPage = new LoginPage(driver);
	WebElement termsOfUse = loginPage.getTermsOfUse();
	Assert.assertEquals(true, termsOfUse.isDisplayed());
	
}

@Test (priority=4)
public void verifyPrivacyPolicyDisplayed() {
	LoginPage loginPage = new LoginPage(driver);
	WebElement privacyPolicy = loginPage.getPrivacyNotice();
	Assert.assertEquals(true, privacyPolicy.isDisplayed());
}

@Test(priority=5)
public void verifycontactUsDisplayed() {
	LoginPage loginPage = new LoginPage(driver);
	WebElement contactUs = loginPage.getContactUs();
	Assert.assertEquals(true, contactUs.isDisplayed());
	String title = driver.getTitle();
	System.out.println("Title is ---->>>" + title);
}


@Test(priority=6)
public void signOn() {
	Login signIn = new Login(driver);
	signIn.loginToApp("ashish.lamichhane@mckesson.com", "Mom01@momq");
}

@Test(priority=7, description="Check if user has signed on successfully; by verifying navigation bar is present.")
public void verifySignOn() {
	NavigationHeader navigationBar = new NavigationHeader(driver);
	WebElement navigationBarPresent = navigationBar.getNavigationBar();
	Assert.assertEquals(true, navigationBarPresent.isDisplayed() , "User is not signed in!!");
	//Assert.assertEquals(true, navigationBarPresent.isDisplayed());
}


}
