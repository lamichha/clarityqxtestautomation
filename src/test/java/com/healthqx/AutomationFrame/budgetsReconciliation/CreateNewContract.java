/**
 * 
 */
package com.healthqx.AutomationFrame.budgetsReconciliation;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.BudgetsReconciliationMainPage;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.LoginPage;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisTableHeaders;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisTableValuesRiskOff;
import com.healthqx.AutomationFrame.HealthQX.SsoLogin;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.HealthQX.NetworkAnalysisFilter;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.VerificationMethodListener;
import com.healthqx.AutomationFrame.Utility.Verify;
//import com.sun.jna.platform.unix.X11.Window;

/**
 * @author enebo4f
 *Test case is to perform data validation in Network Analysis main page
 */
@Listeners(VerificationMethodListener.class)
public class CreateNewContract {

	
	WebDriver driver;
	Environment env;
	BrowserTypes  browser;
	DataBaseConnector dbConnector;
	
	private String contractName;
	
	private String contractStartDate;
	
	private String practiceName;
	
	private String episodeName;
	private String contractEndDate;
	private String earliestEpisodeStartDate;
	private String dateEligibleForFirstPayment;
	private String configurationSetName;
	
	
	
	//@BeforeSuite
	@BeforeClass
	@Parameters({"browser","environment"})
	public void beforeTest(String browserType,String environment) {
		
		ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
		
		//Login signIn = new Login(driver);
		SsoLogin signIn = new SsoLogin(driver);
		signIn.loginToApp(env.username(), env.password());
		
		//creating DB Connection
				dbConnector= new DataBaseConnector(env.dbEnv(),env.dbUrlDqx());
				dbConnector.openConnection();
		
		
	}
	
	
	
	@Test(priority=1,description ="Navigate to Contract Overview page",groups= {"Budgets & Reconciliation"})
	public void navigateToContractOverview() {
		NavigationHeader nav = new NavigationHeader(driver);
		BudgetsReconciliationMainPage main = new BudgetsReconciliationMainPage(driver);
		nav.navigateToBudgetsAndReconciliationFromDropDown();
		//main.getCreateNewContractLink().click();
		Synchronize wait = new Synchronize();
		wait.waitUntilElementPresent(3000);
		
	}

	/*@Test(priority=2,description ="Rank Contracts",groups= {"Budgets & Reconciliation"})
	public void rankContracts() {
		BudgetsReconciliationMainPage main = new BudgetsReconciliationMainPage(driver);
		Synchronize wait = new Synchronize();
		wait.waitUntilElementPresent(3000);
		main.getRankContractsLink().click();
		wait.waitUntilElementPresent(3000);
		main.getEpisodeToRankDropDown().click();
		wait.waitUntilElementPresent(3000);
		
		List<WebElement> episodes = main.getSelectEpisodeToRank().findElements(By.xpath("//*[@class=\"dx-scrollview-content\"]/div"));
		
		episodes.get(episodes.size()-2).click();
		
		
		main.getStartRanking().click();
	}*/
	
	
	
	@Test(priority=3,description ="Create a new Contract",groups= {"Budgets & Reconciliation"})
	public void createNewContract() {
		BudgetsReconciliationMainPage main = new BudgetsReconciliationMainPage(driver);
		contractStartDate = main.contractStartDate();
		contractEndDate = main.contractEndDate();
		earliestEpisodeStartDate = main.earliestEpisodeStartDate();
		dateEligibleForFirstPayment = main.dateEligibleForFirstPayment();
		String practiceDataQuery = "SELECT TOP 1 PracticeName FROM rpt.NetworkAnalysisSplit where EcrRunId = " +env.EcrRunId() + " and EpisodeCategoryName not in ('SRF','Other') and TotalCost>1000 order by newid();" ;
		practiceName = dbConnector.executeQuerySingleValue(practiceDataQuery);
		contractName = "AutomationTestContract" + new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
		String configurationSetNameQuery="select OrganizationConfigurationSetName from dbo.OrganizationConfigurationSetDim where OrganizationConfigurationSetId in (select OrganizationConfigurationSetId from dbo.EcrRunDim where EcrRunId = " + env.EcrRunId() + ");";
		configurationSetName = dbConnector.executeQuerySingleValue(configurationSetNameQuery); 
		String episodeNameQuery = "SELECT TOP 1 EpisodeName FROM rpt.NetworkAnalysisSplit where EcrRunId = " +env.EcrRunId() + " and EpisodeCategoryName not in ('SRF','Other') and practiceName = " +"'" + practiceName +"'"+" and TotalCost>1000 order by newid();";
		episodeName = dbConnector.executeQuerySingleValue(episodeNameQuery);
		
		Synchronize wait = new Synchronize();
		wait.waitUntilElementPresent(1000);
		Actions action = new Actions(driver);
		action.moveToElement(main.getCreateNewContractLink()).click().build().perform() ;
		wait.waitUntilElementPresent(1000);
		action.moveToElement(main.getPracticeNameTextBox()).click().build().perform();
		main.getPracticeNameTextBox().sendKeys(practiceName);
		wait.waitUntilElementPresent(5000);
		WebElement practiceNameElement = driver.findElement(By.xpath("//DIV[@class='dx-item-content dx-list-item-content'][contains(text(),'" + practiceName +"')]/self::DIV")) ;
		practiceNameElement.click();
		wait.waitUntilElementPresent(3000);
		//Enter contract Name
		action.moveToElement(main.getContractNameTextBox()).click().build().perform();
		main.getContractNameTextBox().sendKeys(contractName);
		//select configuration set
		main.getConfigurationSetDropDown().click();
		WebElement configurationSetNameElement = driver.findElement(By.xpath("//DIV[@class='dx-item-content dx-list-item-content'][contains(text(),'" + configurationSetName +"')]/self::DIV"));
		configurationSetNameElement.click();
		//Enter Episode Name;
		action.sendKeys(main.getEpisodeNameDropdown(), episodeName).build().perform();
		wait.waitUntilElementPresent(2000);
		WebElement episodeNameElement = driver.findElement(By.xpath("//DIV[@class='dx-item-content dx-list-item-content'][contains(text(),'" + episodeName + "')]/self::DIV"));
		episodeNameElement.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", main.getContractStartDateField()) ;
		main.getContractStartDateField().click();
		main.getContractStartDateField().sendKeys(contractStartDate);
		
		main.getContractEndDateField().click();
		main.getContractEndDateField().sendKeys(contractEndDate);
		main.getEarliestEpisodeStartDateField().click();
		
		main.getEarliestEpisodeStartDateField().sendKeys(earliestEpisodeStartDate);
		
		main.getDateEligibleForFirstPaymentField().click();
		main.getDateEligibleForFirstPaymentField().sendKeys(dateEligibleForFirstPayment);
		action.moveToElement(main.getContractAmount()).doubleClick().sendKeys("1000").build().perform();
		main.getClickOutSideWithinPpmScreen().click();
		action.moveToElement(main.getSaveContractButton()).click().build().perform();
		wait.waitUntilElementPresent(1000);
		}
	
	@Test(priority=4,description ="Approve Contract",groups= {"Budgets & Reconciliation"})
	public void testContractAgainstDb() throws ClassNotFoundException, SQLException {
		
		Synchronize wait = new Synchronize();
		wait.waitUntilElementPresent(5000);
		//String query = "select c.ContractStartDate from bud.Contract c where c.ContractName = '" + contractName + "' ;";
		String query = "select c.ContractStartDate, c.contractEndDate, c.earliestEpisodeStartDate, c.DateEligibleForFirstPayment from bud.Contract c where c.ContractName = '" + contractName + "' ;";
		
		
		LocalDate startDate = LocalDate.parse(dbConnector.executeQueryMultipleValues(query).get(0)) ;
		String contractStartDateFromDb =  DateTimeFormatter.ofPattern("M/d/yyyy").format(startDate) ;
		
		LocalDate endDateDb = LocalDate.parse(dbConnector.executeQueryMultipleValues(query).get(1)); 
		String contractEndDateFromDb = DateTimeFormatter.ofPattern("M/d/yyyy").format(endDateDb) ;
		
		LocalDate earliestStartDate = LocalDate.parse(dbConnector.executeQueryMultipleValues(query).get(2)) ;
		String earliestEpisodeStartDateFromDb = DateTimeFormatter.ofPattern("M/d/yyyy").format(earliestStartDate);
		
		LocalDate dateEligibleForPayment = LocalDate.parse(dbConnector.executeQueryMultipleValues(query).get(3)) ;
		String dateEligibleForFirstPaymentFromDb = DateTimeFormatter.ofPattern("M/d/yyyy").format(dateEligibleForPayment);
		
		Verify.verifyEquals(contractStartDate, contractStartDateFromDb, "Contract Start Date does not match and query is: " + query);
		
		Verify.verifyEquals(dateEligibleForFirstPayment, dateEligibleForFirstPaymentFromDb, "DateEligibleForFirstPayment does not match and query is: " + query);
		
		Verify.verifyEquals(contractEndDate, contractEndDateFromDb, "contractEndDate does not match and query is: " + query);
		
		Verify.verifyEquals(earliestEpisodeStartDate, earliestEpisodeStartDateFromDb, "earliestEpisodeStartDate does not match and query is: " + query);
		
	}
	
	
	@AfterSuite()
	//@AfterClass
	public void tearDown() {
			
		LogOut logout = new LogOut(driver);
		logout.logOff();
		dbConnector.closeConnection();
		
	}
}
