/**
 * 
 */
package com.healthqx.AutomationFrame;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityFilter;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPage;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.MyHealthQX;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.VerificationMethodListener;
import com.healthqx.AutomationFrame.Utility.Verify;

/**
 * @author enebo4f
 *
 */
@Listeners(VerificationMethodListener.class)
public class EpisodeOpportunitySaveBookmark {
	WebDriver driver;
	Environment env;
	BrowserTypes  browser;
	DataBaseConnector dbConnector;
	
	EpisodeOpportunityFilter filter;
	NavigationHeader navigation;
	MyHealthQX myHealthQx;
	
	private String bookmarkName;
	
	@BeforeSuite
	@Parameters({"browser","environment"})
	public void beforeTest(String browserType,String environment) {
		ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
		
		Login signIn = new Login(driver);
		signIn.loginToApp(env.username(), env.password());
		
		//creating DB Connection
				dbConnector= new DataBaseConnector(env.dbEnv(),env.dbUrlCqx());
				dbConnector.openConnection();
		
	}
	@Test(priority=1,description="Navigate to Episode Opportunity to Access Save Bookmark Test")
	public void preConditions() {
		navigation = new NavigationHeader(driver);
		Synchronize pause = new Synchronize();
		
		navigation.navigateToEpisodeOpportunityFromDropDown();
		pause.waitUntilElementPresent(1000);
		
		String expBreadCrumb = "Episode Opportunity";
		
		Verify.verifyTrue(navigation.getSubModuleBreadCrumb().getText().contains(expBreadCrumb));
		//Assert.assertTrue(navigation.getSubModuleBreadCrumb().getText().contains(expBreadCrumb));
		
		//selecting Acute only to save a bookmark
		filter = new EpisodeOpportunityFilter(driver);
		pause.waitUntilElementPresent(1000);

		filter.getChronicCheckBox().click();
		pause.waitUntilElementPresent(1000);

		filter.getOtherCheckBox().click();
		pause.waitUntilElementPresent(1000);

		filter.getProceduralCheckBox().click();
		pause.waitUntilElementPresent(1000);
		
		pause.waitUntilElementPresent(6000);
		
		EpisodeOpportunityMainPage mainPage = new EpisodeOpportunityMainPage(driver);
		String expTotalCostPercent = "100.00%";
		String actTocalCostPercent = mainPage.getTotalCostOneCategory().getText();
		Verify.verifyEquals(actTocalCostPercent, expTotalCostPercent);
		//Assert.assertEquals(actTocalCostPercent, expTotalCostPercent);
	}
	
	
	
	
	
	@Test(priority=2,description="Save Bookmark")
	public void saveBookmark() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		filter = new EpisodeOpportunityFilter(driver);
		navigation = new NavigationHeader(driver);
		myHealthQx = new MyHealthQX(driver);
		
		
		WebElement saveBookmarkButton = wait.until(ExpectedConditions.visibilityOf(filter.getSaveBookmarkButton()));
		saveBookmarkButton.click();
		
		bookmarkName = "AutomationTest" + new SimpleDateFormat("yyyyMMddHHmm").format(new Date()); 
		System.out.println(bookmarkName);
		
		WebElement saveBookmarkName = wait.until(ExpectedConditions.visibilityOf(filter.getSaveBookmarkName()));
		Actions action = new Actions(driver);
		action.moveToElement(saveBookmarkName).click().build().perform();
		saveBookmarkName.sendKeys(bookmarkName);
		filter.getToSaveBookmark().click();
		
		WebElement saveConfirmation = filter.getBookmarkSavedConfirmation();
		
		
		
		Verify.verifyTrue(saveConfirmation.isDisplayed());
		
	}
	
	@Test(priority=3,description="Newly created bookmark should be available in UI")
	public void verifyBookmarkInUi() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		filter = new EpisodeOpportunityFilter(driver);
		Synchronize pause = new Synchronize();
		WebElement bookmarkDropDown = wait.until(ExpectedConditions.visibilityOf(filter.getBookmarkDropdown()));
		Actions action = new Actions(driver);
		
		action.moveToElement(bookmarkDropDown).click().build().perform();
		  
			
			pause.waitUntilElementPresent(1000);
		
		bookmarkDropDown.sendKeys(bookmarkName);
		pause.waitUntilElementPresent(1000);
			
			WebElement newBookmarkName = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//DIV[@class='dx-item-content dx-list-item-content'][text()=" + "'" + bookmarkName + "'" +"]/self::DIV"))));
		String bookMarkNameFromDropdown = newBookmarkName.getText();
			System.out.println("bookMark NameFrom Dropdown is" + bookMarkNameFromDropdown);
			Verify.verifyEquals(bookMarkNameFromDropdown, bookmarkName);
			
	}

	@Test(priority= 4,description="Newly created bookmark should be available in database")
	public void checkBookmarkInDB() {
String query = "SELECT name FROM dbo.AnalysisToolQuery where name ="  +  "'"  + bookmarkName  + "';" ;
		
		String expBookmarkName = dbConnector.executeQuerySingleValue(query);
		
		Verify.verifyEquals(expBookmarkName, bookmarkName);
		
	}
	
	@AfterSuite()
	public void tearDown() {
			dbConnector.closeConnection();
		
		LogOut logout = new LogOut(driver);
		logout.logOff();
	}
	
}
