/**
 * 
 */
package com.healthqx.AutomationFrame;



import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityFilter;
import com.healthqx.AutomationFrame.HealthQX.EpisodeOpportunityMainPageRiskOff;
import com.healthqx.AutomationFrame.HealthQX.LogOut;
import com.healthqx.AutomationFrame.HealthQX.Login;
import com.healthqx.AutomationFrame.HealthQX.NavigationHeader;
import com.healthqx.AutomationFrame.HealthQX.NavigationTiles;
import com.healthqx.AutomationFrame.Utility.BrowserTypes;
import com.healthqx.AutomationFrame.Utility.DataBaseConnector;
import com.healthqx.AutomationFrame.Utility.Environment;
import com.healthqx.AutomationFrame.Utility.StartUpBrowser;
import com.healthqx.AutomationFrame.Utility.Synchronize;
import com.healthqx.AutomationFrame.Utility.Verify;

/**
 * @author enebo4f
 *
 */
public class TestEpisodeOpportunityMainPageRiskOffAcuteSplit {

	WebDriver driver;
	
	Environment env;
	BrowserTypes  browser;
	
	
	DataBaseConnector dbConnector;
	
	//@BeforeSuite
	@BeforeClass
	@Parameters({"browser","environment"})
	public void beforeTest(String browserType,String environment) {
		
		ConfigFactory.setProperty("browser", browserType);
		browser = ConfigFactory.create(BrowserTypes.class);
		ConfigFactory.setProperty("env", environment);
		env = ConfigFactory.create(Environment.class);
		driver = StartUpBrowser.StartBrowser(browser.browserType(), env.url()); 
		
		
		Login signIn = new Login(driver);
		signIn.loginToApp(env.username(), env.password());
		
		//creating DB Connection
		dbConnector= new DataBaseConnector(env.dbEnv(),env.dbUrlDqx());
		dbConnector.openConnection();
		
		
	}
	

	@Test(description = "Navigate to Episode Opportunity-Acute Split Risk Off", priority = 1, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void navigateToEpisodeOpportunityAcuteSplitRiskOff() {
		
		//NavigationTiles navigate = new NavigationTiles(driver);
		NavigationHeader navigation = new NavigationHeader(driver);
		
		//navigate.navigateToEpisodeOpportunity();
		navigation.navigateToEpisodeOpportunityFromDropDown();
		Assert.assertTrue(navigation.getSubModuleBreadCrumb().getText().contains("Episode Opportunity"));
		
		
	}
	
	@Test(description="Turn Off Risk",priority=2)
	public void turnOffRisk() {
		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		WebElement riskSwitch = filter.getRiskAdjustToggleSwitch();
		riskSwitch.click();
		System.out.println("Switch Text is " + riskSwitch.getText());
		
		
		WebElement riskOnSwitch = filter.getRiskAdjustOn();
		System.out.println("Risk On Switch is "+ riskOnSwitch.getText());
		
		WebElement riskOffSwitch = filter.getRiskAdjustOff();
		System.out.println("Risk Off Switch is " + riskOffSwitch.getText());
		
		//riskOnSwitch.click();
		
		System.out.println(riskOnSwitch.getText() + " is Risk Off ???");
	}
	
	

	@Test(description = "Total Costs pie chart should be 100%- Acute Split Risk Off", priority = 3, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyAcuteOnlyCostAcuteSplitRiskOff() {

		EpisodeOpportunityFilter filter = new EpisodeOpportunityFilter(driver);
		Synchronize pause = new Synchronize();
		// Unchecking other categories, SRF is unchecked by default
		filter.getChronicCheckBox().click();
		pause.waitUntilElementPresent(1000);
		filter.getOtherCheckBox().click();
		pause.waitUntilElementPresent(1000);
		filter.getProceduralCheckBox().click();
		pause.waitUntilElementPresent(1000);

		//setting a pause
		
		pause.waitUntilElementPresent(6000);
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String expTotalCostPercent = "100.00%";
		
		
		String actTocalCostPercent = mainPage.getTotalCostOneCategory().getText();
		Assert.assertEquals(actTocalCostPercent, expTotalCostPercent);
	}

	@Test(description = "Member Counts pie chart should be 100%- Acute Split Risk Off", priority = 4, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyAcuteOnlyMemberCountAcuteSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);

		String expMemberCountPercent = "100.00%";
		String actMemberCountPercent = mainPage.getTotalMemberOneCategory().getText();
		Assert.assertEquals(actMemberCountPercent, expMemberCountPercent);
	}

	@Test(description = "Typical Cost on list should match with Total Typical Cost on table - Acute Split Risk Off", priority = 5, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTypicalCostAcuteSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		//String typicalCostFromList = mainPage.getTypicalCostFromList().getText().replaceAll(".*: ", "");
		String typicalCostFromList = mainPage.getTypicalCostFromList().getText().replace("Typical Cost:", "").replace("\n", "").trim();
		String typicalCostFromTable = mainPage.getTableTotalTypical().getText();
		Assert.assertEquals(typicalCostFromList, typicalCostFromTable);
	}

	@Test(description = "Typical WIth Comp Cost on list should match with Total Typical With Comp Cost in table - Acute Split Risk Off", priority = 6, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTypicalWithCompCostAcuteSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String typicalWithCompFromList = mainPage.getTypicalWithCompCostFromList().getText().replace("Typical with Comp Cost:", "").replace("\n", "").trim();
		String typicalWithCompFromTable = mainPage.getTableTotalComp().getText();
		Assert.assertEquals(typicalWithCompFromList, typicalWithCompFromTable);
	}

	@Test(description = "PAC Cost on list should match with Total PAC Cost in table - Acute Split Risk Off", priority = 7, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyPACCostAcuteSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String pacCostFromList = mainPage.getPacCostFromList().getText().replace("PAC Cost:", "").replace("\n", "").trim();
		String pacCostFromTable = mainPage.getTableTotalPac().getText();
		Assert.assertEquals(pacCostFromList, pacCostFromTable);
	}

	@Test(description = "Total Cost on list should matches with Total Cost in table - Acute Split Risk Off", priority = 8, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTotalCostAcuteSplitRiskOff() {
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String actTotalCostFromList = mainPage.getTotalCostFromList().getText().replace("Cost:", "").replace("\n", "").trim();
		String expTotalCostFromTable = mainPage.getTableTotalCost().getAttribute("innerText").replaceAll(".*: ", "");
		Assert.assertEquals(actTotalCostFromList, expTotalCostFromTable);
	}

	@Test(description = "Episode Counts on UI table should match with counts in DB - Acute Split Risk Off", priority = 9, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyEpisodeCountsAcuteSplitRiskOff() {
		String query = "select count(EpisodeName) as EpisodeCounts FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + " and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE' and episodeName not like '%SUBTYPE';";
		
		String EpisodeCountFromDB = dbConnector.executeQuerySingleValue(query);
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String episodesCountsOnUI = mainPage.getTableTotalNumberOfEpisodes().getText().replaceAll(" Episodes", "");
		Assert.assertEquals(EpisodeCountFromDB, episodesCountsOnUI);
	}

	@Test(description = "Member Counts on UI table should match with counts in DB - Acute Split Risk Off", priority = 10, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyMemberCountsAcuteSplitRiskOff() {
		String query = "select sum(MemberCount) as TotalMemberCounts FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String membersCountFromDB = dbConnector.executeQuerySingleValue(query);
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String membersCountsOnUI = mainPage.getTableTotalNumberOfMembers().getText().replaceAll(",", "");
		Assert.assertEquals(membersCountFromDB, membersCountsOnUI);
	}

	@Test(description = "Providers Counts on UI table should match with counts in DB - Acute Split Risk Off", priority = 11, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyProvidersCountsAcuteSplitRiskOff() {
		String query = "select sum(ProviderCount) as TotalProviderCount FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String providersCountFromDB = dbConnector.executeQuerySingleValue(query);
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String providersCountsOnUI = mainPage.getTableTotalNumberOfProviders().getText().replaceAll(",", "");
		Assert.assertEquals(providersCountFromDB, providersCountsOnUI);
	}

	@Test(description = "Total Average Typical Cost on UI table should match with DB - Acute Split Risk Off", priority = 12, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTotalAverageTypicalAcuteSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(AverageTypicalCost)/count(EpisodeName),0)  ) as TotalAverageTypicalCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String totalAverageTypicalFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalAverageTypicalOnUI = mainPage.getTableTotalAverageTypical().getText().replaceAll(",", "");
		Assert.assertEquals(totalAverageTypicalFromDB, totalAverageTypicalOnUI);
	}

	@Test(description = "Total Average Typical Complication Cost on UI table should match with DB - Acute Split Risk Off", priority = 13, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTotalAverageCompAcuteSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(AverageTypicalComplicationCost)/count(EpisodeName),0)  ) as TotalAverageTypicalComplicationCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String totalAverageCompFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalAverageCompOnUI = mainPage.getTableTotalAverageComp().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalAverageCompFromDB, totalAverageCompOnUI);
	}

	@Test(description = "Total Average PAC Cost on UI table should match with DB - Acute Split Risk Off", priority = 14, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTotalAveragePacAcuteSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(AveragePacCost)/count(EpisodeName),0)  ) as TotalAveragePacCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String totalAveragePacFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalAveragePacOnUI = mainPage.getTableTotalAveragePac().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalAveragePacFromDB, totalAveragePacOnUI);
	}

	@Test(description = "Total Average  Cost on UI table should match with DB - Acute Split Risk Off", priority = 15, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTotalAverageAcuteSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(AverageCost)/count(EpisodeName),0)  ) as TotalAverageCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String totalAverageFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalAverageOnUI = mainPage.getTableTotalAverage().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalAverageFromDB, totalAverageOnUI);
	}

	@Test(description = "Total Typical Cost on UI table should match with DB - Acute Split Risk Off", priority = 16, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTotalTypicalAcuteSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(TotalTypicalCost),0)  )  as TotalTotalTypicalCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String totalTypicalFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalTypicalOnUI = mainPage.getTableTotalTypical().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalTypicalFromDB, totalTypicalOnUI);
	}

	@Test(description = "Total Typical With Complication Cost on UI table should match with DB - Acute Split Risk Off", priority = 17, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTotalCompAcuteSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(TotalTypicalComplicationCost),0)  )  as TotalTotalTypicalComplicationCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String totalCompFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalCompOnUI = mainPage.getTableTotalComp().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalCompFromDB, totalCompOnUI);
	}

	@Test(description = "Total PAC Cost on UI table should match with DB - Acute Split Risk Off", priority = 18, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTotalPacAcuteSplit() {
		String query = "select CONCAT ( '$', round(sum(TotalPacCost),0)  )  as TotalTotalPacCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String totalPacFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalPacOnUI = mainPage.getTableTotalPac().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalPacFromDB, totalPacOnUI);
		Verify.verifyEquals(totalPacOnUI, totalPacFromDB);
	}

	@Test(description = "Total Cost on UI table should match with DB - Acute Split Risk Off", priority = 19, groups={"Category:Acute, Cost Type:Split, Risk Off"})
	public void verifyTotalCostAgainstDbAcuteSplitRiskOff() {
		String query = "select CONCAT ( '$', round(sum(TotalCost),0)  )  as TotalTotalCost  FROM rpt.EpisodeOpportunitySplit where EcrRunId=" + env.EcrRunId() + "  and LevelId=1 and EpisodeCategoryName='Acute' and episodeName not like '%SUBTYPE';";
		String totalCostFromDB = dbConnector.executeQuerySingleValue(query).replaceAll("\\.00$", "");
		EpisodeOpportunityMainPageRiskOff mainPage = new EpisodeOpportunityMainPageRiskOff(driver);
		String totalCostOnUI = mainPage.getTableTotalCost().getAttribute("innerText").replaceAll(",", "");
		Assert.assertEquals(totalCostFromDB, totalCostOnUI);
	}


	@AfterSuite()
	//@AfterClass
	public void tearDown() {
			
		LogOut logout = new LogOut(driver);
		logout.logOff();
		dbConnector.closeConnection();
	}

}
