/**
 * 
 */
package com.healthqx.AutomationFrame.Utility;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

/**
 * @author enebo4f
 *
 */
public class TakeScreenShot {

	 WebDriver driver;
	public  void captureScreenshot(WebDriver driver) {
		
		
		 File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
         
            try {
				FileUtils.copyFile(scrFile, new File("C:/seleniumScreenShots/" + System.currentTimeMillis()+ ".png"));
				
			} catch (IOException e) {
				
				System.out.println(e.getMessage());
			}
	}
}
