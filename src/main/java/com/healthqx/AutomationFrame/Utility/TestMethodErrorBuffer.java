/**
 * 
 */
package com.healthqx.AutomationFrame.Utility;

import java.util.List;

/**
 * Buffer to hold test method verification errors till the completion of test method execution.
 * @author enebo4f
 *
 */
class TestMethodErrorBuffer {
	
	//Thread safe while running tests in parallel
	private static ThreadLocal<List<Throwable>> testErrorBuffer = new ThreadLocal<>();
	
	static List<Throwable> get(){
		return testErrorBuffer.get();
	}
	
	static void set(List<Throwable> errorBuffer) {
		testErrorBuffer.set(errorBuffer);
	}
	
	static void remove() {
		testErrorBuffer.remove();
	}

}
