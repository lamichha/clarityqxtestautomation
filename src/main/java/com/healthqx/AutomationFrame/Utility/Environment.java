/**
 * 
 */
package com.healthqx.AutomationFrame.Utility;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;
import org.aeonbits.owner.ConfigFactory;

/**
 * @author enebo4f
 *
 */
@Sources({
	"classpath:${env}.properties"
})
public interface Environment extends Config {
	//Urls and Credentials
	String url();
	String username();
	String password();
	String dbEnv();
	String dbUrlDqx();
	String dbUrlCqx();
	String dbUrlAut();
	String dbUrlEds();
	String EcrRunId();
	
	
	
	@Key("db.hostname")
	String getDBHostname();
	
	 @Key("db.port")
	    int getDBPort();

	    @Key("db.username")
	    String getDBUsername();

	    @Key("db.password")
	    String getDBPassword();
	    
	    Environment env = ConfigFactory.create(Environment.class);
	    
	

}
