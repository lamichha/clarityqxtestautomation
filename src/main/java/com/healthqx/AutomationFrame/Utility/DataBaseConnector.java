/**
 * 
 */
package com.healthqx.AutomationFrame.Utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



/**
 * @author Ashish L.
 *
 */
public class DataBaseConnector {
	Environment env;
	private String dbEnv;
	private String dbUrl;
	
	Connection connection;

	
	public DataBaseConnector(String dbEnv,String dbUrl) {
		this.dbEnv=dbEnv;
		this.dbUrl=dbUrl;
	}
	
	
	public boolean openConnection() {
		// open connection
				try {
					connection = DriverManager.getConnection(this.dbUrl );
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (connection != null) {
					System.out.println("Connected to the database...");
					return true;
				} else {
					System.out.println("Database connection failed to " + dbEnv + " Environment");
					return false;
				}
				
				
	}
	
	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	
	public  String executeQuerySingleValue(String SqlQuery) {

		
		
		String resultValue = "";
		
		ResultSet rs;

		

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}

		try {
			Statement stmt = connection.createStatement();
			rs = stmt.executeQuery(SqlQuery);

			try {
				while (rs.next()) {
					 resultValue = rs.getString(1).toString();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (NullPointerException err) {
				System.out.println("No Records obtained for this specific query");
				err.printStackTrace();
			}

		} catch (SQLException sqlEx) {
			System.out.println("SQL Exception:" + sqlEx.getMessage());
		}

		
		return resultValue;
	}
	

	
	public ArrayList <String> executeQueryMultipleValues(String sqlQuery) throws SQLException, ClassNotFoundException{
		ResultSet rs;
		
		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Statement	st = connection.createStatement();
		rs = st.executeQuery(sqlQuery);
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCounts = rsmd.getColumnCount();
		
		ArrayList <String> resultValue = new ArrayList<>();
		while(rs.next()) {
			for(int i=1; i<=columnCounts; i++) {
				resultValue.add(rs.getString(i));
				
			}
			
		}
		return resultValue;
	}
		

}
