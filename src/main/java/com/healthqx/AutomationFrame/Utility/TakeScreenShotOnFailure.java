/**
 * 
 */
package com.healthqx.AutomationFrame.Utility;



import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;




/**
 * @author enebo4f
 */
public class TakeScreenShotOnFailure implements ITestListener  {
	private WebDriver driver;
	String filePath = "C:/seleniumScreenShots/";
	//private static final String OUTPUT_FOLDER = "test-output/";
	
	
	
    @Override
    public void onTestFailure(ITestResult result) {
    	System.out.println("***** Error "+result.getName()+" test has failed *****");
    	String methodName=result.getName().toString().trim();
    	takeScreenShot(methodName);
    	//takeFullScreenShot(methodName);
    	
    	
    	
    	
    	
    }
  
    public void takeScreenShot(String methodName) {
    	
    	//get the driver
    driver = StartUpBrowser.driver; 
    File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	 
         //The below method will save the screen shot in above path labeled with corresponding method name 
            try {
				
				FileUtils.copyFile(scrFile, new File(filePath+methodName+".png"));
            	
            	//ImageIO.write(image, "png", outputFile)
            	
				System.out.println("***Placed screen shot in "+filePath+" ***");	
				
			} catch (IOException e) {
				e.printStackTrace();
			}
    }
    
    
/*
 * This class is to take full screen shot.....Need to switch the method name on onTestFailure method. 
 */
public void takeFullScreenShot(String methodName) {
    	
    	//get the driver
    driver = StartUpBrowser.driver;  
   
    Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
    try {
		ImageIO.write(fpScreenshot.getImage(),"PNG",new File(filePath+methodName+".png"));
		
		
		System.out.println("***Placed screen shot in "+filePath+" ***");
	} catch (IOException e1) {
		
		e1.printStackTrace();
	}
   
    }
    
    
	public void onFinish(ITestContext context) {}
  
    public void onTestStart(ITestResult result) {   }
  
    public void onTestSuccess(ITestResult result) {   }

    public void onTestSkipped(ITestResult result) {   }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {   }

    public void onStart(ITestContext context) {   }
}  
