/**
 * 
 */
package com.healthqx.AutomationFrame.Utility;

import java.util.concurrent.TimeUnit;

/**
 * @author enebo4f
 *
 */
//This class contains methods for explicit/implicit waits to synchronize page
public class Synchronize {
	
	public void waitUntilElementPresent(Integer milliseconds) {
		
		try {
			TimeUnit.MILLISECONDS.sleep(milliseconds);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}

}
