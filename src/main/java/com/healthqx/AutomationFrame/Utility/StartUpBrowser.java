/**
 * 
 */
package com.healthqx.AutomationFrame.Utility;

import java.io.File;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
/**
 * @author Ashish L.
 * 
 * This class will store methods to start up any browsers, currently: FireFox, Chrome & IE
 *
 */
public class StartUpBrowser {

	static WebDriver driver;
	
	
	public static WebDriver StartBrowser(String browserName,String url) {
		
		if(browserName.equalsIgnoreCase("firefox")) {
			
			driver = new FirefoxDriver();
		}
		
		else if (browserName.equalsIgnoreCase("Chrome")) {
			//File file = new File("C:/Apps/JarFilesDrivers/chromedriver_win32/chromedriver.exe");
//			File file = new File("C:/Users/enebo4f/eclipse-workspace/com.healthqx.AutomationFrame/browser-Drivers/chromedriver.exe");
			File file = new File("browser-Drivers/chromedriver.exe");		
			
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-infobars");
			driver = new ChromeDriver(options);
			//WebElement html = driver.findElement(By.tagName("html")) ;
			//html.sendKeys(Keys.chord(Keys.CONTROL,Keys.SUBTRACT));
			
		}
		
		else if (browserName.equalsIgnoreCase("IE")) {
			//File file = new File("C:/Apps/JarFilesDrivers/IEDriverServer_Win32_3.6.0/IEDriverServer.exe");
			//File file = new File("C:/Users/enebo4f/eclipse-workspace/com.healthqx.AutomationFrame/browser-Drivers/IEDriverServer.exe");
			File file = new File("browser-Drivers/IEDriverServer.exe");

			//File file = new File("C:/Apps/JarFilesDrivers/IEDriverServer_Win32_3.8.0/IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			driver = new InternetExplorerDriver();		
		}
		
		driver.manage().window().maximize();
		driver.get(url);
		return driver;
		
	}
	
	
}
