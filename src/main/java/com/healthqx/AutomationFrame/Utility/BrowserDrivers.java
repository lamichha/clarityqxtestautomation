/**
 * 
 */
package com.healthqx.AutomationFrame.Utility;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;
import org.aeonbits.owner.ConfigFactory;

/**
 * @author enebo4f
 *
 */
@Sources({
	"classpath:${browser}Browser.properties"
})
public interface BrowserDrivers extends Config {
	
	
	String browserType();
	    
	BrowserDrivers browser = ConfigFactory.create(BrowserDrivers.class);
}
