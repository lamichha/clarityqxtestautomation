/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

/**
 * @author Ashish Lamichhane
 *
 */
public class NetworkAnalysisMainPage {
	WebDriver driver;

	//Network Analysis Main Page Export To Excel Button
	@FindBy(xpath="//*[@id=\"grid\"]/div[1]/div[4]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]")
	private WebElement exportToExcel;
	

	
	// Network Analysis Tabular View Name column header (Practice Display Name)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[2]")
	private WebElement nameColumnHeader;

	// Network Analysis Tabular View TIN Column header (partner/practice Tin)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/div[2]")
	private WebElement tinColumnHeader;

	// Network Analysis Tabular View Episodes Column header (EpisodeName)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[3]/div[2]")
	private WebElement episodesColumnHeader;

	// Network Analysis Tabular View Average Typical Cost Column Header (AverageTypicalCost)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[4]/div[2]")
	private WebElement averageTypicalColumnHeader;

	// Network Analysis Tabular View Average Comp Cost Column Header (AverageTypicalComplicationCost)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[5]/div[2]")
	private WebElement averageCompColumnHeader;

	// Network Analysis Tabular View Average PAC Cost Column Header (AveragePacCost)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[6]/div[2]")
	private WebElement averagePacColumnHeader;

	// Network Analysis Tabular View Average Cost Column Header (AverageCost)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[7]/div[2]")
	private WebElement averageCostColumnHeader;

	// Network Analysis Tabular View Total Typical Cost Column Header When Risk Off (TotalTypicalCost)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[8]/div[2]")
	private WebElement totalTypicalCost_RiskOff;
	
	// Network Analysis Tabular View Total Typical Cost Column Header When Risk On (TotalTypicalCost)
		@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[9]/div[2]")
		private WebElement totalTypicalCost_RiskOn;

		// Network Analysis Tabular View Risk Average Cost Column Header When Risk On (RiskAdjustedAverageCost)
				@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[8]/div[2]")
				private WebElement riskAverage;
				
		//Network Analysis Tabular View Total Typical With Comp Cost when Risk On (TotalTypicalComplicationCost)
				@FindBy(xpath="//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[10]/div[2]")
				private WebElement totalComp_RiskOn;
				
		//Network Analysis 	Tabular View Total PAC Cost Column Header when Risk On ()
				@FindBy(xpath="//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[11]/div[2]")
				private WebElement totalPac_RiskOn;
				
				
		//Network Analysis Tabular View Total Cost Column Header when Risk On ()
				@FindBy(xpath="//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[12]/div[2]")
				private WebElement totalCost_RiskOn;
				
		//Network Analysis Tabular View Risk Total Cost Column Header when Risk On ()
				@FindBy(xpath="//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[13]/div[2]")
				private WebElement riskTotal_RiskOn;	
				
	// Network Analysis Tabular View Total Typical With Comp Cost Column Header When Risk Off (TotalTypicalComplicationCost)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[9]/div[2]")
	private WebElement totalComp_RiskOff;

	// Network Analysis Tabular View Total PAC Cost Column Header When Risk Off (TotalPacCost)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[10]/div[2]")
	private WebElement totalPac_RiskOff;

	// Network Analysis Tabular View Total Cost Column Header When Risk Off (TotalCost)
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[11]/div[2]")
	private WebElement totalCost_RiskOff;

	// Network Analysis Tabular View Potential Target Cost Column Header When Risk Off ()
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[12]/div[2]")
	private WebElement potentialTarget_RiskOff;

	// Network Analysis Tabular View Potential Savings Cost Column Header When Risk Off ()
	@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[13]/div[2]")
	private WebElement potentialSavings_RiskOff;
	
	// Network Analysis Tabular View Contract Column Header ()
		@FindBy(xpath = "//*[@id=\"grid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[14]/div[2]")
		private WebElement contract;
		
	
		
	
	
		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[1]/td[1]/a")
		private WebElement firstPracticeNameNtwkAnlysMainPageTable;
		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[2]/td[1]/a")
		private WebElement secondPracticeNameNtwkAnlysMainPageTable;

		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[3]/td[1]/a")
		private WebElement thirdPracticeNameNtwkAnlysMainPageTable;

		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[4]/td[1]/a")
		private WebElement fourthPracticeNameNtwkAnlysMainPageTable;

		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[5]/td[1]/a")
		private WebElement fifthPracticeNameNtwkAnlysMainPageTable;

		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[6]/td[1]/a")
		private WebElement sixthPracticeNameNtwkAnlysMainPageTable;	
		
		
		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[1]/td[2]")
		private WebElement firstPracticeTinNtwkAnlysMainPageTable;
		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[2]/td[2]")
		private WebElement secondPracticeTinNtwkAnlysMainPageTable;

		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[3]/td[2]")
		private WebElement thirdPracticeTinNtwkAnlysMainPageTable;

		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[4]/td[2]")
		private WebElement fourthPracticeTinNtwkAnlysMainPageTable;

		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[5]/td[2]")
		private WebElement fifthPracticeTinNtwkAnlysMainPageTable;

		@FindBy(xpath="//*[@id=\"grid\"]/div/div[6]/div/div[1]/div/div/table/tbody/tr[6]/td[2]")
		private WebElement sixthPracticeTinNtwkAnlysMainPageTable;
		
		
		
		
		
	// Creating constructor

	public int timeoutValue = 30;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getExportToExcel() {
		return exportToExcel;
	}

	public void setExportToExcel(WebElement exportToExcel) {
		this.exportToExcel = exportToExcel;
	}

	public WebElement getNameColumnHeader() {
		return nameColumnHeader;
	}

	public void setNameColumnHeader(WebElement nameColumnHeader) {
		this.nameColumnHeader = nameColumnHeader;
	}

	public WebElement getTinColumnHeader() {
		return tinColumnHeader;
	}

	public void setTinColumnHeader(WebElement tinColumnHeader) {
		this.tinColumnHeader = tinColumnHeader;
	}

	public WebElement getEpisodesColumnHeader() {
		return episodesColumnHeader;
	}

	public void setEpisodesColumnHeader(WebElement episodesColumnHeader) {
		this.episodesColumnHeader = episodesColumnHeader;
	}

	public WebElement getAverageTypicalColumnHeader() {
		return averageTypicalColumnHeader;
	}

	public void setAverageTypicalColumnHeader(WebElement averageTypicalColumnHeader) {
		this.averageTypicalColumnHeader = averageTypicalColumnHeader;
	}

	public WebElement getAverageCompColumnHeader() {
		return averageCompColumnHeader;
	}

	public void setAverageCompColumnHeader(WebElement averageCompColumnHeader) {
		this.averageCompColumnHeader = averageCompColumnHeader;
	}

	public WebElement getAveragePacColumnHeader() {
		return averagePacColumnHeader;
	}

	public void setAveragePacColumnHeader(WebElement averagePacColumnHeader) {
		this.averagePacColumnHeader = averagePacColumnHeader;
	}

	public WebElement getAverageCostColumnHeader() {
		return averageCostColumnHeader;
	}

	public void setAverageCostColumnHeader(WebElement averageCostColumnHeader) {
		this.averageCostColumnHeader = averageCostColumnHeader;
	}

	public WebElement getTotalTypicalCost_RiskOff() {
		return totalTypicalCost_RiskOff;
	}

	public void setTotalTypicalCost_RiskOff(WebElement totalTypicalCost_RiskOff) {
		this.totalTypicalCost_RiskOff = totalTypicalCost_RiskOff;
	}

	public WebElement getTotalTypicalCost_RiskOn() {
		return totalTypicalCost_RiskOn;
	}

	public void setTotalTypicalCost_RiskOn(WebElement totalTypicalCost_RiskOn) {
		this.totalTypicalCost_RiskOn = totalTypicalCost_RiskOn;
	}

	public WebElement getRiskAverage() {
		return riskAverage;
	}

	public void setRiskAverage(WebElement riskAverage) {
		this.riskAverage = riskAverage;
	}

	public WebElement getTotalComp_RiskOn() {
		return totalComp_RiskOn;
	}

	public void setTotalComp_RiskOn(WebElement totalComp_RiskOn) {
		this.totalComp_RiskOn = totalComp_RiskOn;
	}

	public WebElement getTotalPac_RiskOn() {
		return totalPac_RiskOn;
	}

	public void setTotalPac_RiskOn(WebElement totalPac_RiskOn) {
		this.totalPac_RiskOn = totalPac_RiskOn;
	}

	public WebElement getTotalCost_RiskOn() {
		return totalCost_RiskOn;
	}

	public void setTotalCost_RiskOn(WebElement totalCost_RiskOn) {
		this.totalCost_RiskOn = totalCost_RiskOn;
	}

	public WebElement getRiskTotal_RiskOn() {
		return riskTotal_RiskOn;
	}

	public void setRiskTotal_RiskOn(WebElement riskTotal_RiskOn) {
		this.riskTotal_RiskOn = riskTotal_RiskOn;
	}

	public WebElement getTotalComp_RiskOff() {
		return totalComp_RiskOff;
	}

	public void setTotalComp_RiskOff(WebElement totalComp_RiskOff) {
		this.totalComp_RiskOff = totalComp_RiskOff;
	}

	public WebElement getTotalPac_RiskOff() {
		return totalPac_RiskOff;
	}

	public void setTotalPac_RiskOff(WebElement totalPac_RiskOff) {
		this.totalPac_RiskOff = totalPac_RiskOff;
	}

	public WebElement getTotalCost_RiskOff() {
		return totalCost_RiskOff;
	}

	public void setTotalCost_RiskOff(WebElement totalCost_RiskOff) {
		this.totalCost_RiskOff = totalCost_RiskOff;
	}

	public WebElement getPotentialTarget_RiskOff() {
		return potentialTarget_RiskOff;
	}

	public void setPotentialTarget_RiskOff(WebElement potentialTarget_RiskOff) {
		this.potentialTarget_RiskOff = potentialTarget_RiskOff;
	}

	public WebElement getPotentialSavings_RiskOff() {
		return potentialSavings_RiskOff;
	}

	public void setPotentialSavings_RiskOff(WebElement potentialSavings_RiskOff) {
		this.potentialSavings_RiskOff = potentialSavings_RiskOff;
	}

	public WebElement getContract() {
		return contract;
	}

	public void setContract(WebElement contract) {
		this.contract = contract;
	}


	

	public int getTimeoutValue() {
		return timeoutValue;
	}

	public void setTimeoutValue(int timeoutValue) {
		this.timeoutValue = timeoutValue;
	}

	public NetworkAnalysisMainPage(WebDriver driver) {
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, timeoutValue), this);
	}
	public WebElement getFirstPracticeNameNtwkAnlysMainPageTable() {
		return firstPracticeNameNtwkAnlysMainPageTable;
	}

	public void setFirstPracticeNameNtwkAnlysMainPageTable(WebElement firstPracticeNameNtwkAnlysMainPageTable) {
		this.firstPracticeNameNtwkAnlysMainPageTable = firstPracticeNameNtwkAnlysMainPageTable;
	}

	public WebElement getSecondPracticeNameNtwkAnlysMainPageTable() {
		return secondPracticeNameNtwkAnlysMainPageTable;
	}

	public void setSecondPracticeNameNtwkAnlysMainPageTable(WebElement secondPracticeNameNtwkAnlysMainPageTable) {
		this.secondPracticeNameNtwkAnlysMainPageTable = secondPracticeNameNtwkAnlysMainPageTable;
	}

	public WebElement getThirdPracticeNameNtwkAnlysMainPageTable() {
		return thirdPracticeNameNtwkAnlysMainPageTable;
	}

	public void setThirdPracticeNameNtwkAnlysMainPageTable(WebElement thirdPracticeNameNtwkAnlysMainPageTable) {
		this.thirdPracticeNameNtwkAnlysMainPageTable = thirdPracticeNameNtwkAnlysMainPageTable;
	}

	public WebElement getFourthPracticeNameNtwkAnlysMainPageTable() {
		return fourthPracticeNameNtwkAnlysMainPageTable;
	}

	public void setFourthPracticeNameNtwkAnlysMainPageTable(WebElement fourthPracticeNameNtwkAnlysMainPageTable) {
		this.fourthPracticeNameNtwkAnlysMainPageTable = fourthPracticeNameNtwkAnlysMainPageTable;
	}

	public WebElement getFifthPracticeNameNtwkAnlysMainPageTable() {
		return fifthPracticeNameNtwkAnlysMainPageTable;
	}

	public void setFifthPracticeNameNtwkAnlysMainPageTable(WebElement fifthPracticeNameNtwkAnlysMainPageTable) {
		this.fifthPracticeNameNtwkAnlysMainPageTable = fifthPracticeNameNtwkAnlysMainPageTable;
	}

	public WebElement getSixthPracticeNameNtwkAnlysMainPageTable() {
		return sixthPracticeNameNtwkAnlysMainPageTable;
	}

	public void setSixthPracticeNameNtwkAnlysMainPageTable(WebElement sixthPracticeNameNtwkAnlysMainPageTable) {
		this.sixthPracticeNameNtwkAnlysMainPageTable = sixthPracticeNameNtwkAnlysMainPageTable;
	}

	public WebElement getFirstPracticeTinNtwkAnlysMainPageTable() {
		return firstPracticeTinNtwkAnlysMainPageTable;
	}

	public void setFirstPracticeTinNtwkAnlysMainPageTable(WebElement firstPracticeTinNtwkAnlysMainPageTable) {
		this.firstPracticeTinNtwkAnlysMainPageTable = firstPracticeTinNtwkAnlysMainPageTable;
	}

	public WebElement getSecondPracticeTinNtwkAnlysMainPageTable() {
		return secondPracticeTinNtwkAnlysMainPageTable;
	}

	public void setSecondPracticeTinNtwkAnlysMainPageTable(WebElement secondPracticeTinNtwkAnlysMainPageTable) {
		this.secondPracticeTinNtwkAnlysMainPageTable = secondPracticeTinNtwkAnlysMainPageTable;
	}

	public WebElement getThirdPracticeTinNtwkAnlysMainPageTable() {
		return thirdPracticeTinNtwkAnlysMainPageTable;
	}

	public void setThirdPracticeTinNtwkAnlysMainPageTable(WebElement thirdPracticeTinNtwkAnlysMainPageTable) {
		this.thirdPracticeTinNtwkAnlysMainPageTable = thirdPracticeTinNtwkAnlysMainPageTable;
	}

	public WebElement getFourthPracticeTinNtwkAnlysMainPageTable() {
		return fourthPracticeTinNtwkAnlysMainPageTable;
	}

	public void setFourthPracticeTinNtwkAnlysMainPageTable(WebElement fourthPracticeTinNtwkAnlysMainPageTable) {
		this.fourthPracticeTinNtwkAnlysMainPageTable = fourthPracticeTinNtwkAnlysMainPageTable;
	}

	public WebElement getFifthPracticeTinNtwkAnlysMainPageTable() {
		return fifthPracticeTinNtwkAnlysMainPageTable;
	}

	public void setFifthPracticeTinNtwkAnlysMainPageTable(WebElement fifthPracticeTinNtwkAnlysMainPageTable) {
		this.fifthPracticeTinNtwkAnlysMainPageTable = fifthPracticeTinNtwkAnlysMainPageTable;
	}

	public WebElement getSixthPracticeTinNtwkAnlysMainPageTable() {
		return sixthPracticeTinNtwkAnlysMainPageTable;
	}

	public void setSixthPracticeTinNtwkAnlysMainPageTable(WebElement sixthPracticeTinNtwkAnlysMainPageTable) {
		this.sixthPracticeTinNtwkAnlysMainPageTable = sixthPracticeTinNtwkAnlysMainPageTable;
	}
	

}