package com.healthqx.AutomationFrame.HealthQX;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NetworkAnalysisSubPage {
WebDriver driver;





@FindBy(xpath="//*[@id=\"details-sub-nav\"]/li[1]/a")
private WebElement costBreakDownTab;

@FindBy(xpath="//*[@id=\"details-sub-nav\"]/li[2]/a")
private WebElement costPerMemberTab;

@FindBy(xpath="//*[@id=\"details-sub-nav\"]/li[3]/a")
private WebElement pacReportTab;

@FindBy(xpath="//*[@id=\"details-sub-nav\"]/li[4]/a")
private WebElement benchmarkTab;

@FindBy(id="datagrid-header")
private WebElement subPageTableHeader;

@FindBy(id="data-grid-header")
private WebElement subPageTableHeaderPracticeNameAndTin;

@FindBy(xpath="//*[@id=\"cost-breakdown-datagrid\"]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")
private WebElement ccsGroupDragFromLocationCostBreakdown;

@FindBy(xpath="//*[@id=\"cost-breakdown-datagrid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/div[2]")
private WebElement ccsGroupDragToLocationCostBreakDown;

@FindBy(xpath="//*[@id=\"cost-breakdown-datagrid\"]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")
private WebElement memberDragFromLocation;

@FindBy(xpath="//*[@id=\"cost-breakdown-datagrid\"]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")
private WebElement dragFrom;

@FindBy(xpath="//*[@id=\"cost-breakdown-datagrid\"]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]")
private WebElement dragTo;



//constructor
public NetworkAnalysisSubPage(WebDriver driver) {
	
	PageFactory.initElements(driver, this);
}

public WebDriver getDriver() {
	return driver;
}

public void setDriver(WebDriver driver) {
	this.driver = driver;
}



public WebElement getCostBreakDownTab() {
	return costBreakDownTab;
}

public void setCostBreakDownTab(WebElement costBreakDownTab) {
	this.costBreakDownTab = costBreakDownTab;
}

public WebElement getCostPerMemberTab() {
	return costPerMemberTab;
}

public void setCostPerMemberTab(WebElement costPerMemberTab) {
	this.costPerMemberTab = costPerMemberTab;
}

public WebElement getPacReportTab() {
	return pacReportTab;
}

public void setPacReportTab(WebElement pacReportTab) {
	this.pacReportTab = pacReportTab;
}

public WebElement getBenchmarkTab() {
	return benchmarkTab;
}

public void setBenchmarkTab(WebElement benchmarkTab) {
	this.benchmarkTab = benchmarkTab;
}

public WebElement getSubPageTableHeader() {
	return subPageTableHeader;
}

public void setSubPageTableHeader(WebElement subPageTableHeader) {
	this.subPageTableHeader = subPageTableHeader;
}

public WebElement getCcsGroupDragFromLocationCostBreakdown() {
	return ccsGroupDragFromLocationCostBreakdown;
}

public void setCcsGroupDragFromLocationCostBreakdown(WebElement ccsGroupDragFromLocationCostBreakdown) {
	this.ccsGroupDragFromLocationCostBreakdown = ccsGroupDragFromLocationCostBreakdown;
}

public WebElement getCcsGroupDragToLocationCostBreakDown() {
	return ccsGroupDragToLocationCostBreakDown;
}

public void setCcsGroupDragToLocationCostBreakDown(WebElement ccsGroupDragToLocationCostBreakDown) {
	this.ccsGroupDragToLocationCostBreakDown = ccsGroupDragToLocationCostBreakDown;
}

public WebElement getMemberDragFromLocation() {
	return memberDragFromLocation;
}

public void setMemberDragFromLocation(WebElement memberDragFromLocation) {
	this.memberDragFromLocation = memberDragFromLocation;
}

public WebElement getDragFrom() {
	return dragFrom;
}

public void setDragFrom(WebElement dragFrom) {
	this.dragFrom = dragFrom;
}

public WebElement getDragTo() {
	return dragTo;
}

public void setDragTo(WebElement dragTo) {
	this.dragTo = dragTo;
}
@FindBy(xpath="//*[@id=\"cost-breakdown-datagrid\"]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div/div") 
private WebElement dragAndDropElements;


public WebElement getSubPageTableHeaderPracticeNameAndTin() {
	return subPageTableHeaderPracticeNameAndTin;
}

public void setSubPageTableHeaderPracticeNameAndTin(WebElement subPageTableHeaderPracticeNameAndTin) {
	this.subPageTableHeaderPracticeNameAndTin = subPageTableHeaderPracticeNameAndTin;
}

public WebElement getDragAndDropElements() {
	return dragAndDropElements;
}

public void setDragAndDropElements(WebElement dragAndDropElements) {
	this.dragAndDropElements = dragAndDropElements;
}



}
