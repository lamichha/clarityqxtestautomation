package com.healthqx.AutomationFrame.HealthQX;

import java.math.BigDecimal;

public class EpisodeOpportunityTableHeaders {

	private String episodeName;
	private String episodeCategory;
	private BigDecimal memberCount;
	private BigDecimal providerCount;
	private BigDecimal averageTypicalCost;
	private BigDecimal averageTypicalComplicationCost;
	private BigDecimal averagePacCost;
	private BigDecimal averageCost;
	private BigDecimal riskAdjustedAverageCost;
	private BigDecimal totalTypicalCost;
	private BigDecimal totalTypicalComplicationCost;
	private BigDecimal totalPacCost;
	private BigDecimal totalCost;
	private BigDecimal riskAdjustedTotalCost;
	
	
	public String getEpisodeName() {
		return episodeName;
	}
	public void setEpisodeName(String episodeName) {
		this.episodeName = episodeName;
	}
	public String getEpisodeCategory() {
		return episodeCategory;
	}
	public void setEpisodeCategory(String episodeCategory) {
		this.episodeCategory = episodeCategory;
	}
	public BigDecimal getMemberCount() {
		return memberCount;
	}
	public void setMemberCount(BigDecimal memberCount) {
		this.memberCount = memberCount;
	}
	public BigDecimal getProviderCount() {
		return providerCount;
	}
	public void setProviderCount(BigDecimal providerCount) {
		this.providerCount = providerCount;
	}
	public BigDecimal getAverageTypicalCost() {
		return averageTypicalCost;
	}
	public void setAverageTypicalCost(BigDecimal averageTypicalCost) {
		this.averageTypicalCost = averageTypicalCost;
	}
	public BigDecimal getAverageTypicalComplicationCost() {
		return averageTypicalComplicationCost;
	}
	public void setAverageTypicalComplicationCost(BigDecimal averageTypicalComplicationCost) {
		this.averageTypicalComplicationCost = averageTypicalComplicationCost;
	}
	public BigDecimal getAveragePacCost() {
		return averagePacCost;
	}
	public void setAveragePacCost(BigDecimal averagePacCost) {
		this.averagePacCost = averagePacCost;
	}
	public BigDecimal getAverageCost() {
		return averageCost;
	}
	public void setAverageCost(BigDecimal averageCost) {
		this.averageCost = averageCost;
	}
	public BigDecimal getRiskAdjustedAverageCost() {
		return riskAdjustedAverageCost;
	}
	public void setRiskAdjustedAverageCost(BigDecimal riskAdjustedAverageCost) {
		this.riskAdjustedAverageCost = riskAdjustedAverageCost;
	}
	public BigDecimal getTotalTypicalCost() {
		return totalTypicalCost;
	}
	public void setTotalTypicalCost(BigDecimal totalTypicalCost) {
		this.totalTypicalCost = totalTypicalCost;
	}
	public BigDecimal getTotalTypicalComplicationCost() {
		return totalTypicalComplicationCost;
	}
	public void setTotalTypicalComplicationCost(BigDecimal totalTypicalComplicationCost) {
		this.totalTypicalComplicationCost = totalTypicalComplicationCost;
	}
	public BigDecimal getTotalPacCost() {
		return totalPacCost;
	}
	public void setTotalPacCost(BigDecimal totalPacCost) {
		this.totalPacCost = totalPacCost;
	}
	public BigDecimal getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}
	public BigDecimal getRiskAdjustedTotalCost() {
		return riskAdjustedTotalCost;
	}
	public void setRiskAdjustedTotalCost(BigDecimal riskAdjustedTotalCost) {
		this.riskAdjustedTotalCost = riskAdjustedTotalCost;
	}
}
