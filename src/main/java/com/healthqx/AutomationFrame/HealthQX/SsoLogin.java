/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.healthqx.AutomationFrame.Utility.DataBaseConnector;

/**
 * @author Ashish L.
 *
 *
 *         This class will store all the locators and methods of SSO login page.
 */
public class SsoLogin {

	WebDriver driver;
	
	DataBaseConnector dbConnector;

	By signInAccount = By.xpath("//*[@id=\"bySelection\"]/div[4]");
	By username = By.id("userNameInput");
	By password = By.id("passwordInput");
	By loginbutton = By.id("submitButton");
	//By codeBox = By.xpath("//INPUT[@id='Code']/self::INPUT");
	//By verifyCodeButton = By.xpath("//BUTTON[@type='submit'][contains(text(),'Verify Code ')]/self::BUTTON");

	
	// Creating Constructor
	public SsoLogin(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// Creating method to login to application
	public void loginToApp(String uid, String pwd) {
		driver.findElement(signInAccount).click();
		driver.findElement(username).sendKeys(uid);
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(loginbutton).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	

	
}
