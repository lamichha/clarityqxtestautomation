/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Ashish L.
 *
 *
 *This class contains method to logout
 */
public class LogOut {
	
	WebDriver driver;
	
	public void logOff() {
		NavigationHeader nav = new NavigationHeader(driver);
		WebDriverWait wait = new WebDriverWait(driver,20);
		WebElement logingOut = wait.until(ExpectedConditions.visibilityOf(nav.getLogOut()));
		
		if(logingOut.isEnabled()&& logingOut.isDisplayed()) {
			logingOut.click();
		}
		else {
			System.out.println("log Out Button not found");
		}
		driver.quit();
	
	}
	
	// Creating Constructor
		public LogOut(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}

}
