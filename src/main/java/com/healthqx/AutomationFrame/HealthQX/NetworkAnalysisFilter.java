/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

/**
 * @author enebo4f
 *
 */
public class NetworkAnalysisFilter {

	WebDriver driver;

	// Saved Bookmarks header text for bookmark drop-down
	@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Saved Bookmarks']/self::SPAN")
	private WebElement bookmarkHeaderText;

	// Saved Bookmark dropdown
	//@FindBy(xpath = "//*[@id=\"analysis-tool-queries-filter\"]/div[1]/div/div[1]")
	@FindBy(xpath = "//*[@id=\"analysis-tool-queries-filter\"]/div[1]/div/input")
	private WebElement bookmarkDropdown;
	
	//@FindBy(xpath="//DIV[@class='dx-item-content dx-list-item-content']")
	@FindBy(xpath="//DIV[@class='dx-item-content dx-list-item-content']")
	private WebElement newBookmarkName;

	// study period header text *******Need to come back because different clients environment will have different study periods
	@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Study Periods']/self::SPAN")
	private WebElement studyPeriodHeaderText;
	
	//first corporate entity Study Period tree view toggle
	@FindBy(xpath="//*[@id=\"corporate-entities-filter\"]/div/div/div[1]/ul/li[1]/div[2]")
	private WebElement firstCorpEntityToggle;
	
	//First Corporate entity Name
	@FindBy(xpath="//*[@id=\"corporate-entities-filter\"]/div/div/div[1]/ul/li[1]/div[1]")
	private WebElement firstCorpEntityName;
	
	//Second corporate entity Study Period tree view toggle
	@FindBy(xpath="//*[@id=\"corporate-entities-filter\"]/div/div/div[1]/ul/li[2]/div[2]")
	private WebElement secondCorpEntityToggle;
	
	
	//Second Corporate entity Name
	@FindBy(xpath="//*[@id=\"corporate-entities-filter\"]/div/div/div[1]/ul/li[2]/div[1]")
	private WebElement secondCorpEntityName;
	
	
	//Risk Adjust Text Header
	@FindBy(xpath="//*[@id=\"chart-filters\"]/div[3]/span")
	private WebElement riskAdjustHeaderText;
	
	//Risk Adjust On/Off switch
	//@FindBy(xpath="//*[@id=\"risk-adjust-filter\"]")
	@FindBy(id="risk-adjust-filter")
	private WebElement riskAdjustToggleSwitch;
	
	//Risk On Switch
	//@FindBy(xpath="//*[@id=\"risk-adjust-filter\"]/div/div/div/div[1]")
	@FindBy(id="risk-adjust-filter")
	private WebElement riskAdjustOn;
	
	//Risk Off Switch
	//@FindBy(xpath="//*[@id=\"risk-adjust-filter\"]/div/div/div/div[3]")
	@FindBy(id="risk-adjust-filter")
	private WebElement riskAdjustOff;
	
	//Outlier Type Text
		@FindBy(xpath="//*[@id=\"chart-filters\"]/div[4]/span[1]")
		private WebElement outlierTypeTextHeader;
		
		//Outlier Type Everything
		@FindBy(xpath="//*[@id=\"outlier-types-filter\"]/div[1]/div[1]/div[2]/div[1]")
		private WebElement outlierTypeEverything;
		
		//Outlier Type Budget
		@FindBy(xpath="//*[@id=\"outlier-types-filter\"]/div[1]/div[1]/div[2]/div[2]")
		private WebElement outlierTypeBudget;
			
		//Outlier Type Performance
		@FindBy(xpath="//*[@id=\"outlier-types-filter\"]/div[1]/div[1]/div[2]/div[3]")
		private WebElement outlierTypePerformance;

	// Cost Type Text Header
	//@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Cost Type']/self::SPAN")
		@FindBy(xpath = "//*[@id=\"chart-filters\"]/div[5]/span[1]")
	private WebElement costTypeHeaderText;

	// Cost Type Split
	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='Split']/self::SPAN")
		@FindBy(xpath="//*[@id=\"cost-type-filter\"]/div[1]/div[1]")
	private WebElement costTypeSplit;

	// Cost Type Un-split
	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='Unsplit']/self::SPAN")
	@FindBy(xpath="//*[@id=\"cost-type-filter\"]/div[1]/div[2]")	
	private WebElement costTypeUnsplit;

	// Data Type Text Header
	//@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Data Type']/self::SPAN")
	@FindBy(xpath="//*[@id=\"chart-filters\"]/div[6]/span[1]")
	private WebElement dataTypeHeaderText;

	// Data Type Total
	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='Total']/self::SPAN")
	@FindBy(xpath="//*[@id=\"data-type-filter\"]/div[1]/div[1]")
	private WebElement dataTypeTotal;

	// Data Type Average
	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='Average']/self::SPAN")
	@FindBy(xpath="//*[@id=\"data-type-filter\"]/div[1]/div[2]")
	private WebElement dataTypeAverage;

	// Level Text Header
	//@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Level']/self::SPAN")
	@FindBy(xpath="//*[@id=\"chart-filters\"]/div[7]/span[1]")
	private WebElement levelHeaderText;

	// Various Levels One through Five
	@FindBy(xpath="//*[@id=\"level-filter\"]/div[1]/div")
	private WebElement level;
	
	public WebElement getLevel() {
		return level;
	}


	public void setLevel(WebElement level) {
		this.level = level;
	}

	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='1']/self::SPAN")
	@FindBy(xpath="//*[@id=\"level-filter\"]/div[1]/div[1]")
	private WebElement levelOne;

	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='2']/self::SPAN")
	@FindBy(xpath="//*[@id=\"level-filter\"]/div[1]/div[2]")
	private WebElement levelTwo;

	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='3']/self::SPAN")
	@FindBy(xpath="//*[@id=\"level-filter\"]/div[1]/div[3]")
	private WebElement levelThree;

	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='4']/self::SPAN")
	@FindBy(xpath="//*[@id=\"level-filter\"]/div[1]/div[4]")
	private WebElement levelFour;

	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='5']/self::SPAN")
	@FindBy(xpath="//*[@id=\"level-filter\"]/div[1]/div[5]")
	private WebElement levelFive;
	
	//Practice Count Header Text
	@FindBy(xpath="//*[@id=\"chart-filters\"]/div[8]/span[1]")
	private WebElement practiceCountHeaderText;
	
	//Practice Counts 10,50,100,500
	@FindBy(xpath="//*[@id=\"practice-count-filter\"]/div[1]/div[1]")
	private WebElement practiceCountTen;
	
	@FindBy(xpath="//*[@id=\"practice-count-filter\"]/div[1]/div[2]")
	private WebElement practiceCountFifty;
	
	@FindBy(xpath="//*[@id=\"practice-count-filter\"]/div[1]/div[3]")
	private WebElement practiceCountHundred;
	
	@FindBy(xpath="//*[@id=\"practice-count-filter\"]/div[1]/div[4]")
	private WebElement practiceCountFiveHundred;
	
	
	
	

	// Search Episodes Text Header
	//@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Episodes']/self::SPAN")
	@FindBy(xpath="//*[@id=\"chart-filters\"]/div[14]/span[1]")
	private WebElement searchEpisodesHeaderText;

	// Search Episodes Search Box
	@FindBy(xpath = "//*[@id=\"episodes-search-filter\"]/div/div/input")
	private WebElement searchEpisodes;

	// Episodes Categories
	// Acute Tree View Toggle
	//@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[2]")
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[2]")
	private WebElement acuteTreeToggle;

	
	// Acute Text
	//@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[1]")
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[1]")
	private WebElement acuteText;
	
	//select first episode from Acute Category 
	//@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/ul[1]/li[1]/ul[1]/li[1]/div[1]")
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/ul[1]/li[1]/div[1]")
	private WebElement firstEpisodeAcute;
	
	//select Second Episode Acute
	//@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/ul[1]/li[1]/ul[1]/li[2]/div[1]")
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/ul[1]/li[2]/div[1]")
	private WebElement secondEpisodeAcute;
	
	//select Third Episode Acute
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/ul[1]/li[3]/div[1]")
	private WebElement thirdEpisodeAcute;
		
	//select Fourth Episode Acute
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/ul[1]/li[4]/div[1]")
	private WebElement fourthEpisodeAcute;

	// Chronic Tree View Toggle
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[3]/div[2]")
	private WebElement chronicTreeToggle;

	
	// Chronic Text
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[3]/div[1]")
	private WebElement chronicText;
	
	//Select First Episode from Chronic Category
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[3]/ul[1]/li[1]/div[1]")
	private WebElement firstEpisodeChronic;
	
	//Select Second EPisode From Chronic Category
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[3]/ul[1]/li[2]/div[1]")
	private WebElement secondEpisodeChronic;
	
	//Select Third EPisode From Chronic Category
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[3]/ul[1]/li[3]/div[1]")
	private WebElement thirdEpisodeChronic;
	
	//Select Fourth Episode From Chronic Category
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[3]/ul[1]/li[4]/div[1]")
	private WebElement fourthEpisodeChronic;
	
	
	

	// Other Tree View Toggle
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[4]/div[2]")
	private WebElement otherTreeToggle;

	

	// Other Text
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[4]/div[1]")
	private WebElement otherText;
	
	//Select First Episode From Other Category
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[4]/ul[1]/li[1]/div[1]")
	private WebElement firstEpisodeOther;
	
	//Select Second Episode From Other Category
		@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[4]/ul[1]/li[2]/div[1]")
		private WebElement secondtEpisodeOther;
		
		//Select Third Episode From Other Category
		@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[4]/ul[1]/li[3]/div[1]")
		private WebElement thirdEpisodeOther;
		
		//Select Fourth Episode From Other Category
		@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[4]/ul[1]/li[4]/div[1]")
		private WebElement fourthEpisodeOther;
	
	

	// Procedural Tree View Toggle
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[5]/div[2]")
	private WebElement proceduralTreeToggle;

	

	// Procedural Text
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[5]/div[1]")
	private WebElement proceduralText;
	
	//Select First Episode From Procedural Category
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[5]/ul[1]/li[1]/div[1]")
	private WebElement firstEpisodeProcedural;
	
	//Select Second Episode From Procedural Category
		@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[5]/ul[1]/li[2]/div[1]")
		private WebElement secondEpisodeProcedural;
		
		//Select Third Episode From Procedural Category
		@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[5]/ul[1]/li[3]/div[1]")
		private WebElement thirdEpisodeProcedural;
		
		//Select Fourth Episode From Procedural Category
		@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[5]/ul[1]/li[4]/div[1]")
		private WebElement fourthEpisodeProcedural;

	// SRF Tree View Toggle
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[6]/div[2]")
	private WebElement srfTreeToggle;



	// SRF Text
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[6]/div[1]")
	private WebElement srfText;
	
	//Select First Episode From SRF Category
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[6]/ul[1]/li[1]/div[1]")
	private WebElement firstEpisodeSrf;
	
	//Select Second Episode From SRF Category
		@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[6]/ul[1]/li[2]/div[1]")
		private WebElement secondEpisodeSrf;
		
		//Select Third Episode From SRF Category
		@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[6]/ul[1]/li[3]/div[1]")
		private WebElement thirdEpisodeSrf;
		
		
		//Select Fourth Episode From SRF Category
		@FindBy(xpath="//*[@id=\"episodes-filter\"]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[6]/ul[1]/li[4]/div[1]")
		private WebElement fourthEpisodeSrf;
	
	//Episode Volume Filter Text Header
	@FindBy(xpath="//*[@id=\"chart-filters\"]/div[15]/span[1]")
	private WebElement episodeVolumeFilterTextHeader;
	
	//Episode Volume Minimum filter
	@FindBy(xpath="//*[@id=\"episode-volume-min-filter\"]/div[1]/input")
	private WebElement episodeVolumeMinFilter;
	
	//Episode Volume Maximum Filter
	@FindBy(xpath="//*[@id=\"episode-volume-max-filter\"]/div/input")
	private WebElement episodeVolumeMaxFilter;
	
	//Episode Average Cost Filter Text Header
	@FindBy(xpath="//*[@id=\"chart-filters\"]/div[17]/span[1]")
	private WebElement episodeAvgCostFilterTextHeader;
	
	//Episode Average Cost Minimum Filter
	@FindBy(xpath="//*[@id=\"total-amount-min-filter\"]/div[1]/input")
	private WebElement episodeAvgCostMinFilter;
	
	//Episode Average Cost Maximum Filter
	@FindBy(xpath="//*[@id=\"total-amount-max-filter\"]/div[1]/input")
	private WebElement episodeAvgCostMaxFilter;
	
	//Episode PAC Cost TFilter Text Header
	@FindBy(xpath="//*[@id=\"chart-filters\"]/div[18]/span[1]")
	private WebElement episodePacCostTextHeader;
	
	//Episode PAC Cost Minimum Filter
	@FindBy(xpath="//*[@id=\"pac-amount-min-filter\"]/div[1]/input")
	private WebElement episodePacCostMinFilter;
	
	//Episode PAC Cost Maximum Filter
	@FindBy(xpath="//*[@id=\"pac-amount-max-filter\"]/div[1]/input")
	private WebElement episodePacCostMaxFilter;
	
	
	
	

	// Save Bookmark Button
	@FindBy(xpath = "//SPAN[@class='dx-button-text'][text()='Save Bookmark']/self::SPAN")
	private WebElement saveBookmarkButton;

	// Save Bookmark Pop-Up -- Bookmark Name
	@FindBy(xpath = "//*[@id=\"analysis-tool-query-name-textbox\"]/div[1]/input")
	private WebElement saveBookmarkName;

	// Click Save Bookmark to save bookmark
	@FindBy(xpath = "//*[@id=\"view-model-base\"]/div[9]/div/div[2]/div/div[3]/div")
	private WebElement toSaveBookmark;

	// Save Bookmark popup cancel button
	@FindBy(xpath = "//*[@id=\"view-model-base\"]/div[9]/div/div[2]/div/div[4]")
	private WebElement toCancelBookmarkPopup;

	// Save Bookmark Pop up close "X"
	@FindBy(xpath = "//*[@id=\"view-model-base\"]/div[9]/div/div[1]/div/div[3]/div/div/div/div/i")
	private WebElement toCloseBookmarkPopup;

	// Save Bookmark Popup name required validation text
	@FindBy(xpath = "")
	private WebElement blankNameValidationText;
	
	//Bookmark Saved success sign
		@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div[1]/div[1]")
		private WebElement bookmarkSavedConfirmation;
		
		public WebElement getBookmarkSavedConfirmation() {
			return bookmarkSavedConfirmation;
		}


		public void setBookmarkSavedConfirmation(WebElement bookmarkSavedConfirmation) {
			this.bookmarkSavedConfirmation = bookmarkSavedConfirmation;
		}

	// Create constructor
	public int timeoutValue = 30;
	public NetworkAnalysisFilter(WebDriver driver) {
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, timeoutValue), this);
	}

	//setting returns for the elements
	public WebElement getBookmarkHeaderText() {
		return bookmarkHeaderText;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getBookmarkDropdown() {
		return bookmarkDropdown;
	}

	public void setBookmarkDropdown(WebElement bookmarkDropdown) {
		this.bookmarkDropdown = bookmarkDropdown;
	}

	public WebElement getNewBookmarkName() {
		return newBookmarkName;
	}

	public void setNewBookmarkName(WebElement newBookmarkName) {
		this.newBookmarkName = newBookmarkName;
	}

	public WebElement getStudyPeriodHeaderText() {
		return studyPeriodHeaderText;
	}

	public void setStudyPeriodHeaderText(WebElement studyPeriodHeaderText) {
		this.studyPeriodHeaderText = studyPeriodHeaderText;
	}

	public WebElement getFirstCorpEntityToggle() {
		return firstCorpEntityToggle;
	}

	public void setFirstCorpEntityToggle(WebElement firstCorpEntityToggle) {
		this.firstCorpEntityToggle = firstCorpEntityToggle;
	}

	public WebElement getFirstCorpEntityName() {
		return firstCorpEntityName;
	}

	public void setFirstCorpEntityName(WebElement firstCorpEntityName) {
		this.firstCorpEntityName = firstCorpEntityName;
	}

	public WebElement getSecondCorpEntityToggle() {
		return secondCorpEntityToggle;
	}

	public void setSecondCorpEntityToggle(WebElement secondCorpEntityToggle) {
		this.secondCorpEntityToggle = secondCorpEntityToggle;
	}

	public WebElement getSecondCorpEntityName() {
		return secondCorpEntityName;
	}

	public void setSecondCorpEntityName(WebElement secondCorpEntityName) {
		this.secondCorpEntityName = secondCorpEntityName;
	}

	public WebElement getRiskAdjustHeaderText() {
		return riskAdjustHeaderText;
	}

	public void setRiskAdjustHeaderText(WebElement riskAdjustHeaderText) {
		this.riskAdjustHeaderText = riskAdjustHeaderText;
	}

	public WebElement getRiskAdjustToggleSwitch() {
		return riskAdjustToggleSwitch;
	}

	public void setRiskAdjustToggleSwitch(WebElement riskAdjustToggleSwitch) {
		this.riskAdjustToggleSwitch = riskAdjustToggleSwitch;
	}

	public WebElement getCostTypeHeaderText() {
		return costTypeHeaderText;
	}

	public void setCostTypeHeaderText(WebElement costTypeHeaderText) {
		this.costTypeHeaderText = costTypeHeaderText;
	}

	public WebElement getCostTypeSplit() {
		return costTypeSplit;
	}

	public void setCostTypeSplit(WebElement costTypeSplit) {
		this.costTypeSplit = costTypeSplit;
	}

	public WebElement getCostTypeUnsplit() {
		return costTypeUnsplit;
	}

	public void setCostTypeUnsplit(WebElement costTypeUnsplit) {
		this.costTypeUnsplit = costTypeUnsplit;
	}

	public WebElement getDataTypeHeaderText() {
		return dataTypeHeaderText;
	}

	public void setDataTypeHeaderText(WebElement dataTypeHeaderText) {
		this.dataTypeHeaderText = dataTypeHeaderText;
	}

	public WebElement getDataTypeTotal() {
		return dataTypeTotal;
	}

	public void setDataTypeTotal(WebElement dataTypeTotal) {
		this.dataTypeTotal = dataTypeTotal;
	}

	public WebElement getDataTypeAverage() {
		return dataTypeAverage;
	}

	public void setDataTypeAverage(WebElement dataTypeAverage) {
		this.dataTypeAverage = dataTypeAverage;
	}

	public WebElement getLevelHeaderText() {
		return levelHeaderText;
	}

	public void setLevelHeaderText(WebElement levelHeaderText) {
		this.levelHeaderText = levelHeaderText;
	}

	public WebElement getLevelOne() {
		return levelOne;
	}

	public void setLevelOne(WebElement levelOne) {
		this.levelOne = levelOne;
	}

	public WebElement getLevelTwo() {
		return levelTwo;
	}

	public void setLevelTwo(WebElement levelTwo) {
		this.levelTwo = levelTwo;
	}

	public WebElement getLevelThree() {
		return levelThree;
	}

	public void setLevelThree(WebElement levelThree) {
		this.levelThree = levelThree;
	}

	public WebElement getLevelFour() {
		return levelFour;
	}

	public void setLevelFour(WebElement levelFour) {
		this.levelFour = levelFour;
	}

	public WebElement getLevelFive() {
		return levelFive;
	}

	public void setLevelFive(WebElement levelFive) {
		this.levelFive = levelFive;
	}

	public WebElement getSearchEpisodesHeaderText() {
		return searchEpisodesHeaderText;
	}

	public void setSearchEpisodesHeaderText(WebElement searchEpisodesHeaderText) {
		this.searchEpisodesHeaderText = searchEpisodesHeaderText;
	}

	public WebElement getSearchEpisodes() {
		return searchEpisodes;
	}

	public void setSearchEpisodes(WebElement searchEpisodes) {
		this.searchEpisodes = searchEpisodes;
	}

	public WebElement getAcuteTreeToggle() {
		return acuteTreeToggle;
	}

	public void setAcuteTreeToggle(WebElement acuteTreeToggle) {
		this.acuteTreeToggle = acuteTreeToggle;
	}

	

	public WebElement getAcuteText() {
		return acuteText;
	}

	public void setAcuteText(WebElement acuteText) {
		this.acuteText = acuteText;
	}

	public WebElement getChronicTreeToggle() {
		return chronicTreeToggle;
	}

	public void setChronicTreeToggle(WebElement chronicTreeToggle) {
		this.chronicTreeToggle = chronicTreeToggle;
	}

	

	public WebElement getChronicText() {
		return chronicText;
	}

	public void setChronicText(WebElement chronicText) {
		this.chronicText = chronicText;
	}

	public WebElement getOtherTreeToggle() {
		return otherTreeToggle;
	}

	public void setOtherTreeToggle(WebElement otherTreeToggle) {
		this.otherTreeToggle = otherTreeToggle;
	}

	

	public WebElement getOtherText() {
		return otherText;
	}

	public void setOtherText(WebElement otherText) {
		this.otherText = otherText;
	}

	public WebElement getProceduralTreeToggle() {
		return proceduralTreeToggle;
	}

	public void setProceduralTreeToggle(WebElement proceduralTreeToggle) {
		this.proceduralTreeToggle = proceduralTreeToggle;
	}

	

	public WebElement getProceduralText() {
		return proceduralText;
	}

	public void setProceduralText(WebElement proceduralText) {
		this.proceduralText = proceduralText;
	}

	public WebElement getSrfTreeToggle() {
		return srfTreeToggle;
	}

	public void setSrfTreeToggle(WebElement srfTreeToggle) {
		this.srfTreeToggle = srfTreeToggle;
	}

	

	public WebElement getSrfText() {
		return srfText;
	}

	public void setSrfText(WebElement srfText) {
		this.srfText = srfText;
	}

	public WebElement getSaveBookmarkButton() {
		return saveBookmarkButton;
	}

	public void setSaveBookmarkButton(WebElement saveBookmarkButton) {
		this.saveBookmarkButton = saveBookmarkButton;
	}

	public WebElement getSaveBookmarkName() {
		return saveBookmarkName;
	}

	public void setSaveBookmarkName(WebElement saveBookmarkName) {
		this.saveBookmarkName = saveBookmarkName;
	}

	public WebElement getToSaveBookmark() {
		return toSaveBookmark;
	}

	public void setToSaveBookmark(WebElement toSaveBookmark) {
		this.toSaveBookmark = toSaveBookmark;
	}

	public WebElement getToCancelBookmarkPopup() {
		return toCancelBookmarkPopup;
	}

	public void setToCancelBookmarkPopup(WebElement toCancelBookmarkPopup) {
		this.toCancelBookmarkPopup = toCancelBookmarkPopup;
	}

	public WebElement getToCloseBookmarkPopup() {
		return toCloseBookmarkPopup;
	}

	public void setToCloseBookmarkPopup(WebElement toCloseBookmarkPopup) {
		this.toCloseBookmarkPopup = toCloseBookmarkPopup;
	}

	public WebElement getBlankNameValidationText() {
		return blankNameValidationText;
	}

	public void setBlankNameValidationText(WebElement blankNameValidationText) {
		this.blankNameValidationText = blankNameValidationText;
	}

	public void setBookmarkHeaderText(WebElement bookmarkHeaderText) {
		this.bookmarkHeaderText = bookmarkHeaderText;
	}

	public WebElement getRiskAdjustOn() {
		return riskAdjustOn;
	}

	public void setRiskAdjustOn(WebElement riskAdjustOn) {
		this.riskAdjustOn = riskAdjustOn;
	}

	public WebElement getRiskAdjustOff() {
		return riskAdjustOff;
	}

	public void setRiskAdjustOff(WebElement riskAdjustOff) {
		this.riskAdjustOff = riskAdjustOff;
	}


	public WebElement getOutlierTypeTextHeader() {
		return outlierTypeTextHeader;
	}


	public void setOutlierTypeTextHeader(WebElement outlierTypeTextHeader) {
		this.outlierTypeTextHeader = outlierTypeTextHeader;
	}


	public WebElement getOutlierTypeEverything() {
		return outlierTypeEverything;
	}


	public void setOutlierTypeEverything(WebElement outlierTypeEverything) {
		this.outlierTypeEverything = outlierTypeEverything;
	}


	public WebElement getOutlierTypeBudget() {
		return outlierTypeBudget;
	}


	public void setOutlierTypeBudget(WebElement outlierTypeBudget) {
		this.outlierTypeBudget = outlierTypeBudget;
	}


	public WebElement getOutlierTypePerformance() {
		return outlierTypePerformance;
	}


	public void setOutlierTypePerformance(WebElement outlierTypePerformance) {
		this.outlierTypePerformance = outlierTypePerformance;
	}


	public WebElement getPracticeCountHeaderText() {
		return practiceCountHeaderText;
	}


	public void setPracticeCountHeaderText(WebElement practiceCountHeaderText) {
		this.practiceCountHeaderText = practiceCountHeaderText;
	}


	public WebElement getPracticeCountTen() {
		return practiceCountTen;
	}


	public void setPracticeCountTen(WebElement practiceCountTen) {
		this.practiceCountTen = practiceCountTen;
	}


	public WebElement getPracticeCountFifty() {
		return practiceCountFifty;
	}


	public void setPracticeCountFifty(WebElement practiceCountFifty) {
		this.practiceCountFifty = practiceCountFifty;
	}


	public WebElement getPracticeCountHundred() {
		return practiceCountHundred;
	}


	public void setPracticeCountHundred(WebElement practiceCountHundred) {
		this.practiceCountHundred = practiceCountHundred;
	}


	public WebElement getPracticeCountFiveHundred() {
		return practiceCountFiveHundred;
	}


	public void setPracticeCountFiveHundred(WebElement practiceCountFiveHundred) {
		this.practiceCountFiveHundred = practiceCountFiveHundred;
	}


	public WebElement getFirstEpisodeAcute() {
		return firstEpisodeAcute;
	}


	public void setFirstEpisodeAcute(WebElement firstEpisodeAcute) {
		this.firstEpisodeAcute = firstEpisodeAcute;
	}


	public WebElement getSecondEpisodeAcute() {
		return secondEpisodeAcute;
	}


	public void setSecondEpisodeAcute(WebElement secondEpisodeAcute) {
		this.secondEpisodeAcute = secondEpisodeAcute;
	}


	public WebElement getThirdEpisodeAcute() {
		return thirdEpisodeAcute;
	}


	public void setThirdEpisodeAcute(WebElement thirdEpisodeAcute) {
		this.thirdEpisodeAcute = thirdEpisodeAcute;
	}


	public WebElement getFourthEpisodeAcute() {
		return fourthEpisodeAcute;
	}


	public void setFourthEpisodeAcute(WebElement fourthEpisodeAcute) {
		this.fourthEpisodeAcute = fourthEpisodeAcute;
	}


	public WebElement getFirstEpisodeChronic() {
		return firstEpisodeChronic;
	}


	public void setFirstEpisodeChronic(WebElement firstEpisodeChronic) {
		this.firstEpisodeChronic = firstEpisodeChronic;
	}


	public WebElement getSecondEpisodeChronic() {
		return secondEpisodeChronic;
	}


	public void setSecondEpisodeChronic(WebElement secondEpisodeChronic) {
		this.secondEpisodeChronic = secondEpisodeChronic;
	}


	public WebElement getThirdEpisodeChronic() {
		return thirdEpisodeChronic;
	}


	public void setThirdEpisodeChronic(WebElement thirdEpisodeChronic) {
		this.thirdEpisodeChronic = thirdEpisodeChronic;
	}


	public WebElement getFourthEpisodeChronic() {
		return fourthEpisodeChronic;
	}


	public void setFourthEpisodeChronic(WebElement fourthEpisodeChronic) {
		this.fourthEpisodeChronic = fourthEpisodeChronic;
	}


	public WebElement getFirstEpisodeOther() {
		return firstEpisodeOther;
	}


	public void setFirstEpisodeOther(WebElement firstEpisodeOther) {
		this.firstEpisodeOther = firstEpisodeOther;
	}


	public WebElement getSecondtEpisodeOther() {
		return secondtEpisodeOther;
	}


	public void setSecondtEpisodeOther(WebElement secondtEpisodeOther) {
		this.secondtEpisodeOther = secondtEpisodeOther;
	}


	public WebElement getThirdEpisodeOther() {
		return thirdEpisodeOther;
	}


	public void setThirdEpisodeOther(WebElement thirdEpisodeOther) {
		this.thirdEpisodeOther = thirdEpisodeOther;
	}


	public WebElement getFourthEpisodeOther() {
		return fourthEpisodeOther;
	}


	public void setFourthEpisodeOther(WebElement fourthEpisodeOther) {
		this.fourthEpisodeOther = fourthEpisodeOther;
	}


	public WebElement getFirstEpisodeProcedural() {
		return firstEpisodeProcedural;
	}


	public void setFirstEpisodeProcedural(WebElement firstEpisodeProcedural) {
		this.firstEpisodeProcedural = firstEpisodeProcedural;
	}


	public WebElement getSecondEpisodeProcedural() {
		return secondEpisodeProcedural;
	}


	public void setSecondEpisodeProcedural(WebElement secondEpisodeProcedural) {
		this.secondEpisodeProcedural = secondEpisodeProcedural;
	}


	public WebElement getThirdEpisodeProcedural() {
		return thirdEpisodeProcedural;
	}


	public void setThirdEpisodeProcedural(WebElement thirdEpisodeProcedural) {
		this.thirdEpisodeProcedural = thirdEpisodeProcedural;
	}


	public WebElement getFourthEpisodeProcedural() {
		return fourthEpisodeProcedural;
	}


	public void setFourthEpisodeProcedural(WebElement fourthEpisodeProcedural) {
		this.fourthEpisodeProcedural = fourthEpisodeProcedural;
	}


	public WebElement getFirstEpisodeSrf() {
		return firstEpisodeSrf;
	}


	public void setFirstEpisodeSrf(WebElement firstEpisodeSrf) {
		this.firstEpisodeSrf = firstEpisodeSrf;
	}


	public WebElement getSecondEpisodeSrf() {
		return secondEpisodeSrf;
	}


	public void setSecondEpisodeSrf(WebElement secondEpisodeSrf) {
		this.secondEpisodeSrf = secondEpisodeSrf;
	}


	public WebElement getThirdEpisodeSrf() {
		return thirdEpisodeSrf;
	}


	public void setThirdEpisodeSrf(WebElement thirdEpisodeSrf) {
		this.thirdEpisodeSrf = thirdEpisodeSrf;
	}


	public WebElement getFourthEpisodeSrf() {
		return fourthEpisodeSrf;
	}


	public void setFourthEpisodeSrf(WebElement fourthEpisodeSrf) {
		this.fourthEpisodeSrf = fourthEpisodeSrf;
	}


	public WebElement getEpisodeVolumeFilterTextHeader() {
		return episodeVolumeFilterTextHeader;
	}


	public void setEpisodeVolumeFilterTextHeader(WebElement episodeVolumeFilterTextHeader) {
		this.episodeVolumeFilterTextHeader = episodeVolumeFilterTextHeader;
	}


	public WebElement getEpisodeVolumeMinFilter() {
		return episodeVolumeMinFilter;
	}


	public void setEpisodeVolumeMinFilter(WebElement episodeVolumeMinFilter) {
		this.episodeVolumeMinFilter = episodeVolumeMinFilter;
	}


	public WebElement getEpisodeVolumeMaxFilter() {
		return episodeVolumeMaxFilter;
	}


	public void setEpisodeVolumeMaxFilter(WebElement episodeVolumeMaxFilter) {
		this.episodeVolumeMaxFilter = episodeVolumeMaxFilter;
	}


	public WebElement getEpisodeAvgCostFilterTextHeader() {
		return episodeAvgCostFilterTextHeader;
	}


	public void setEpisodeAvgCostFilterTextHeader(WebElement episodeAvgCostFilterTextHeader) {
		this.episodeAvgCostFilterTextHeader = episodeAvgCostFilterTextHeader;
	}


	public WebElement getEpisodeAvgCostMinFilter() {
		return episodeAvgCostMinFilter;
	}


	public void setEpisodeAvgCostMinFilter(WebElement episodeAvgCostMinFilter) {
		this.episodeAvgCostMinFilter = episodeAvgCostMinFilter;
	}


	public WebElement getEpisodeAvgCostMaxFilter() {
		return episodeAvgCostMaxFilter;
	}


	public void setEpisodeAvgCostMaxFilter(WebElement episodeAvgCostMaxFilter) {
		this.episodeAvgCostMaxFilter = episodeAvgCostMaxFilter;
	}


	public WebElement getEpisodePacCostTextHeader() {
		return episodePacCostTextHeader;
	}


	public void setEpisodePacCostTextHeader(WebElement episodePacCostTextHeader) {
		this.episodePacCostTextHeader = episodePacCostTextHeader;
	}


	public WebElement getEpisodePacCostMinFilter() {
		return episodePacCostMinFilter;
	}


	public void setEpisodePacCostMinFilter(WebElement episodePacCostMinFilter) {
		this.episodePacCostMinFilter = episodePacCostMinFilter;
	}


	public WebElement getEpisodePacCostMaxFilter() {
		return episodePacCostMaxFilter;
	}


	public void setEpisodePacCostMaxFilter(WebElement episodePacCostMaxFilter) {
		this.episodePacCostMaxFilter = episodePacCostMaxFilter;
	}


	public int getTimeoutValue() {
		return timeoutValue;
	}


	public void setTimeoutValue(int timeoutValue) {
		this.timeoutValue = timeoutValue;
	}

	

}
