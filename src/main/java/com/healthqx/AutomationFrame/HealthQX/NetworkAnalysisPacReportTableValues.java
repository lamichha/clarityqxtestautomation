package com.healthqx.AutomationFrame.HealthQX;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NetworkAnalysisPacReportTableValues {
WebDriver driver;
WebElement element;
	@FindBy(xpath="//*[@id=\"complication-codes-datagrid\"]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody")
	private WebElement allTableData;
	
	public List<NetworkAnalysisPacReportTableHeaders> rowValues(){
		List <NetworkAnalysisPacReportTableHeaders> rowList = new ArrayList<NetworkAnalysisPacReportTableHeaders>();
		List<WebElement> tableData = allTableData.findElements(By.tagName("tr"));
		
		for(int rNum=0; rNum<tableData.size()-1; rNum++) {
			List<WebElement> data = tableData.get(rNum).findElements(By.tagName("td"));
			NetworkAnalysisPacReportTableHeaders header = new NetworkAnalysisPacReportTableHeaders();
			
			header.setGroup(data.get(0).getAttribute("innerText").trim());
			
			
			if(data.get(0).getAttribute("innerText").startsWith("-1")) {
				header.setGroupCode("-1");
			}
			else {
				header.setGroupCode(data.get(0).getAttribute("innerText").replaceAll("-.*", "").trim());
			}
			
			if(data.get(0).getAttribute("innerText").startsWith("-1")) {
				header.setGroupName("Uncategorized");
			}
			else {
			header.setGroupName(data.get(0).getAttribute("innerText").replaceAll(".*-", "").trim());
			}
			
			header.setDxCodeUi(data.get(1).getAttribute("innerText").trim());
			if(data.get(1).getAttribute("innerText").startsWith("-1")) {
				header.setDxCode("-1");
			}
			else {
			header.setDxCode(data.get(1).getAttribute("innerText").replaceAll("-.*", "").trim());
			}
			
			if(data.get(1).getAttribute("innerText").startsWith("-1")) {
				header.setDxName("Uncategorized");
			}
			else {
			header.setDxName(data.get(1).getAttribute("innerText").replaceAll(".*-", "").trim());
			}
			
			header.setMemberCount(new BigDecimal(data.get(2).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setGroupMembersWithPacPercent(new BigDecimal(data.get(3).getAttribute("innerText").replaceAll("[%]", "").trim()));
			header.setAverageCost(new BigDecimal(data.get(4).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setTotalCost(new BigDecimal(data.get(5).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			
			
			
			rowList.add(header);
		}
		return rowList;
	}
	
	

	public WebElement getAllTableData() {
		return allTableData;
	}

	public void setAllTableData(WebElement allTableData) {
		this.allTableData = allTableData;
	}
	
	//Constructor
	public NetworkAnalysisPacReportTableValues(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
}
