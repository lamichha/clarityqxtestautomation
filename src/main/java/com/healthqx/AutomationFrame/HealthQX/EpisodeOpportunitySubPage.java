/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author enebo4f
 *
 */
public class EpisodeOpportunitySubPage {
	WebDriver driver;
	
	
	//Sub Page Bread Crumb episode Name
	@FindBy(xpath="//*[@id=\"navigation-breadcrumbs\"]/li[4]")
	private WebElement subPageBreadCrumb;
	
	//Clinical Classification Tab
	@FindBy(xpath="//*[@id=\"details-sub-nav\"]/li[1]/a")
	private WebElement clinicalClassificationTab;
	
	//Claim Type Tab
	@FindBy(xpath="//*[@id=\"details-sub-nav\"]/li[2]/a")
	private WebElement claimTypeTab;
	
	//Place Of Service Tab
	@FindBy(xpath="//*[@id=\"details-sub-nav\"]/li[3]/a")
	private WebElement placeOfServiceTab;
	
	//Average Typical Cost
	@FindBy(xpath="//*[@id=\"graphical-container\"]/div/div[1]/div[1]/ul/li[1]")
	private WebElement averageTypicalCost;
	
	//Average Typical with Comp Cost
	@FindBy(xpath="//*[@id=\"graphical-container\"]/div/div[1]/div[1]/ul/li[2]")
	private WebElement averageTypicalWithCompCost;
	
	//Average PAC Cost
	@FindBy(xpath="//*[@id=\"graphical-container\"]/div/div[1]/div[1]/ul/li[3]")
	private WebElement averagePacCost;
	
	//Average Total Cost
	@FindBy(xpath="//*[@id=\"graphical-container\"]/div/div[1]/div[1]/ul/li[4]")
	private WebElement averageTotalCost;
	
	//Total Typical Cost
	@FindBy(xpath="//*[@id=\"graphical-container\"]/div/div[1]/div[2]/ul/li[1]")
	private WebElement totalTypicalCost;
	
	//Total Typical with Comp Cost
	@FindBy(xpath="//*[@id=\"graphical-container\"]/div/div[1]/div[2]/ul/li[2]")
	private WebElement totalTypicalWithCompCost;
	//Total PAC Cost
	@FindBy(xpath="//*[@id=\"graphical-container\"]/div/div[1]/div[2]/ul/li[3]")
	private WebElement totalPacCost;

	//Total Cost
	@FindBy(xpath="//*[@id=\"graphical-container\"]/div/div[1]/div[2]/ul/li[4]")
	private WebElement totalCost;
	
	//Total Episode Count
	@FindBy(xpath="//*[@id=\"graphical-container\"]/div/div[1]/div[2]/ul/li[5]")
	private WebElement totalEpisodeCount;
	
	//Grid values
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[9]/div")
	private WebElement totalTypicalGrid;
	
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[10]/div")
	private WebElement totalTypicalWithCompGrid;
	
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[11]/div")
	private WebElement totalPacGrid;
	
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[12]/div")
	private WebElement totalCostGrid;
	
	//Creating Constructor
	public EpisodeOpportunitySubPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getSubPageBreadCrumb() {
		return subPageBreadCrumb;
	}

	public void setSubPageBreadCrumb(WebElement subPageBreadCrumb) {
		this.subPageBreadCrumb = subPageBreadCrumb;
	}

	public WebElement getClinicalClassificationTab() {
		return clinicalClassificationTab;
	}

	public void setClinicalClassificationTab(WebElement clinicalClassificationTab) {
		this.clinicalClassificationTab = clinicalClassificationTab;
	}

	public WebElement getClaimTypeTab() {
		return claimTypeTab;
	}

	public void setClaimTypeTab(WebElement claimTypeTab) {
		this.claimTypeTab = claimTypeTab;
	}

	public WebElement getPlaceOfServiceTab() {
		return placeOfServiceTab;
	}

	public void setPlaceOfServiceTab(WebElement placeOfServiceTab) {
		this.placeOfServiceTab = placeOfServiceTab;
	}

	public WebElement getAverageTypicalCost() {
		return averageTypicalCost;
	}

	public void setAverageTypicalCost(WebElement averageTypicalCost) {
		this.averageTypicalCost = averageTypicalCost;
	}

	public WebElement getAverageTypicalWithCompCost() {
		return averageTypicalWithCompCost;
	}

	public void setAverageTypicalWithCompCost(WebElement averageTypicalWithCompCost) {
		this.averageTypicalWithCompCost = averageTypicalWithCompCost;
	}

	public WebElement getAveragePacCost() {
		return averagePacCost;
	}

	public void setAveragePacCost(WebElement averagePacCost) {
		this.averagePacCost = averagePacCost;
	}

	public WebElement getAverageTotalCost() {
		return averageTotalCost;
	}

	public void setAverageTotalCost(WebElement averageTotalCost) {
		this.averageTotalCost = averageTotalCost;
	}

	public WebElement getTotalTypicalCost() {
		return totalTypicalCost;
	}

	public void setTotalTypicalCost(WebElement totalTypicalCost) {
		this.totalTypicalCost = totalTypicalCost;
	}

	public WebElement getTotalTypicalWithCompCost() {
		return totalTypicalWithCompCost;
	}

	public void setTotalTypicalWithCompCost(WebElement totalTypicalWithCompCost) {
		this.totalTypicalWithCompCost = totalTypicalWithCompCost;
	}

	public WebElement getTotalPacCost() {
		return totalPacCost;
	}

	public void setTotalPacCost(WebElement totalPacCost) {
		this.totalPacCost = totalPacCost;
	}

	public WebElement getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(WebElement totalCost) {
		this.totalCost = totalCost;
	}

	public WebElement getTotalEpisodeCount() {
		return totalEpisodeCount;
	}

	public void setTotalEpisodeCount(WebElement totalEpisodeCount) {
		this.totalEpisodeCount = totalEpisodeCount;
	}

	public WebElement getTotalTypicalGrid() {
		return totalTypicalGrid;
	}

	public void setTotalTypicalGrid(WebElement totalTypicalGrid) {
		this.totalTypicalGrid = totalTypicalGrid;
	}

	public WebElement getTotalTypicalWithCompGrid() {
		return totalTypicalWithCompGrid;
	}

	public void setTotalCompGrid(WebElement totalCompGrid) {
		this.totalTypicalWithCompGrid = totalCompGrid;
	}

	public WebElement getTotalPacGrid() {
		return totalPacGrid;
	}

	public void setTotalPacGrid(WebElement totalPacGrid) {
		this.totalPacGrid = totalPacGrid;
	}

	public WebElement getTotalCostGrid() {
		return totalCostGrid;
	}

	public void setTotalCostGrid(WebElement totalCostGrid) {
		this.totalCostGrid = totalCostGrid;
	}
	
	//Setting getters for locators
	
	
	
	
}
