/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

/**
 * @author enebo4f
 *
 */
public class EpisodeOpportunityFilter {

	WebDriver driver;

	// Saved Bookmarks header text for bookmark drop-down
	@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Saved Bookmarks']/self::SPAN")
	private WebElement bookmarkHeaderText;

	// Saved Bookmark dropdown
	//@FindBy(xpath = "//*[@id=\"analysis-tool-queries-filter\"]/div[1]/div/div[1]")
	@FindBy(xpath = "//*[@id=\"analysis-tool-queries-filter\"]/div[1]/div/input")
	private WebElement bookmarkDropdown;
	
	//@FindBy(xpath="//DIV[@class='dx-item-content dx-list-item-content']")
	@FindBy(xpath="//DIV[@class='dx-item-content dx-list-item-content']")
	private WebElement newBookmarkName;

	// study period header text *******Need to come back because different clients environment will have different study periods
	@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Study Periods']/self::SPAN")
	private WebElement studyPeriodHeaderText;
	
	//first corporate entity Study Period tree view toggle
	@FindBy(xpath="//*[@id=\"corporate-entities-filter\"]/div/div/div[1]/ul/li[1]/div[2]")
	private WebElement firstCorpEntityToggle;
	
	//First Corporate entity Name
	@FindBy(xpath="//*[@id=\"corporate-entities-filter\"]/div/div/div[1]/ul/li[1]/div[1]")
	private WebElement firstCorpEntityName;
	
	//Second corporate entity Study Period tree view toggle
	@FindBy(xpath="//*[@id=\"corporate-entities-filter\"]/div/div/div[1]/ul/li[2]/div[2]")
	private WebElement secondCorpEntityToggle;
	
	
	//Second Corporate entity Name
	@FindBy(xpath="//*[@id=\"corporate-entities-filter\"]/div/div/div[1]/ul/li[2]/div[1]")
	private WebElement secondCorpEntityName;
	
	
	//Risk Adjust Text Header
	@FindBy(xpath="//*[@id=\"chart-filters\"]/div[3]/span")
	private WebElement riskAdjustHeaderText;
	
	//Risk Adjust On/Off switch
	//@FindBy(xpath="//*[@id=\"risk-adjust-filter\"]")
	@FindBy(id="risk-adjust-filter")
	private WebElement riskAdjustToggleSwitch;
	
	//Risk On Switch
	@FindBy(xpath="//*[@id=\"risk-adjust-filter\"]/div/div/div/div[1]")
	private WebElement riskAdjustOn;
	
	//Risk Off Switch
	@FindBy(xpath="//*[@id=\"risk-adjust-filter\"]/div/div/div/div[3]")
	private WebElement riskAdjustOff;

	// Cost Type Text Header
	@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Cost Type']/self::SPAN")
	private WebElement costTypeHeaderText;

	// Cost Type Split
	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='Split']/self::SPAN")
	@FindBy(xpath="//*[@id=\"cost-type-filter\"]/div/div[1]")
	private WebElement costTypeSplit;

	// Cost Type Un-split
	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='Unsplit']/self::SPAN")
	@FindBy(xpath="//*[@id=\"cost-type-filter\"]/div/div[2]")
	private WebElement costTypeUnsplit;

	// Data Type Text Header
	@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Data Type']/self::SPAN")
	private WebElement dataTypeHeaderText;

	// Data Type Total
	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='Total']/self::SPAN")
	@FindBy(xpath="//*[@id=\"data-type-filter\"]/div/div[1]")
	private WebElement dataTypeTotal;

	// Data Type Average
	//@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='Average']/self::SPAN")
	@FindBy(xpath="//*[@id=\"data-type-filter\"]/div/div[2]")
	private WebElement dataTypeAverage;

	// Level Text Header
	@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Level']/self::SPAN")
	private WebElement levelHeaderText;

	// Various Levels One through Five
	@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='1']/self::SPAN")
	private WebElement levelOne;

	@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='2']/self::SPAN")
	private WebElement levelTwo;

	@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='3']/self::SPAN")
	private WebElement levelThree;

	@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='4']/self::SPAN")
	private WebElement levelFour;

	@FindBy(xpath = "//SPAN[@class='dx-tab-text'][text()='5']/self::SPAN")
	private WebElement levelFive;

	// Search Episodes Text Header
	@FindBy(xpath = "//SPAN[@class='nav-header'][text()='Episodes']/self::SPAN")
	private WebElement searchEpisodesHeaderText;

	// Search Episodes Search Box
	@FindBy(xpath = "//*[@id=\"episodes-search-filter\"]/div/div/input")
	private WebElement searchEpisodes;

	// Episodes Categories
	// Acute Tree View Toggle
	//@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div[1]/ul/li[1]/div[3]")
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[1]/div[3]")
	private WebElement acuteTreeToggle;

	// Acute check box
	//@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div[1]/ul/li[1]/div[1]")
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[1]/div[1]")
	private WebElement acuteCheckBox;

	// Acute Text
	//@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div[1]/ul/li[1]/div[2]")
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[1]/div[2]")
	private WebElement acuteText;

	// Chronic Tree View Toggle
	//@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div[1]/ul/li[2]/div[3]")
	@FindBy(xpath="//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[3]/div[3]")
	private WebElement chronicTreeToggle;

	// Chronic check box
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[3]/div[1]")
	private WebElement chronicCheckBox;

	// Chronic Text
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[3]/div[2]")
	private WebElement chronicText;

	// Other Tree View Toggle
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[4]/div[3]")
	private WebElement otherTreeToggle;

	// Other check box
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[4]/div[1]")
	private WebElement otherCheckBox;

	// Other Text
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[4]/div[2]")
	private WebElement otherText;

	// Procedural Tree View Toggle
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[5]/div[3]")
	private WebElement proceduralTreeToggle;

	// Procedural check box
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[5]/div[1]")
	private WebElement proceduralCheckBox;

	// Procedural Text
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[5]/div[2]")
	private WebElement proceduralText;

	// SRF Tree View Toggle
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[6]/div[3]")
	private WebElement srfTreeToggle;

	// SRF check box
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[6]/div[1]")
	private WebElement srfCheckBox;

	// SRF Text
	@FindBy(xpath = "//*[@id=\"episodes-filter\"]/div/div/div/div[1]/ul/li[6]/div[2]")
	private WebElement srfText;

	// Save Bookmark Button
	@FindBy(xpath = "//SPAN[@class='dx-button-text'][text()='Save Bookmark']/self::SPAN")
	private WebElement saveBookmarkButton;

	// Save Bookmark Pop-Up -- Bookmark Name
	@FindBy(xpath = "//*[@id=\"analysis-tool-query-name-textbox\"]/div[1]/input")
	private WebElement saveBookmarkName;

	// Click Save Bookmark to save bookmark
	@FindBy(xpath = "//*[@id=\"view-model-base\"]/div[9]/div/div[2]/div/div[3]/div")
	private WebElement toSaveBookmark;

	// Save Bookmark popup cancel button
	@FindBy(xpath = "//*[@id=\"view-model-base\"]/div[9]/div/div[2]/div/div[4]")
	private WebElement toCancelBookmarkPopup;

	// Save Bookmark Pop up close "X"
	@FindBy(xpath = "//*[@id=\"view-model-base\"]/div[9]/div/div[1]/div/div[3]/div/div/div/div/i")
	private WebElement toCloseBookmarkPopup;

	// Save Bookmark Popup name required validation text
	@FindBy(xpath = "")
	private WebElement blankNameValidationText;
	
	//Bookmark Saved success sign
		@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div[1]/div[1]")
		private WebElement bookmarkSavedConfirmation;
		
		public WebElement getBookmarkSavedConfirmation() {
			return bookmarkSavedConfirmation;
		}


		public void setBookmarkSavedConfirmation(WebElement bookmarkSavedConfirmation) {
			this.bookmarkSavedConfirmation = bookmarkSavedConfirmation;
		}
		//This is for the notification bar where user can click on suggested level. This element works for Both Episode Opportunity and Network Analysis pages.
		@FindBy(xpath="//*[@id=\"completion-level-notifications\"]/span[1]/div/span")
		private WebElement changeLevelToAltarumDefault;
		
		

	public WebElement getChangeLevelToAltarumDefault() {
			return changeLevelToAltarumDefault;
		}


		public void setChangeLevelToAltarumDefault(WebElement changeLevelToAltarumDefault) {
			this.changeLevelToAltarumDefault = changeLevelToAltarumDefault;
		}

	// Create constructor
	public int timeoutValue = 30;
	public EpisodeOpportunityFilter(WebDriver driver) {
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, timeoutValue), this);
	}

	//setting returns for the elements
	public WebElement getBookmarkHeaderText() {
		return bookmarkHeaderText;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getBookmarkDropdown() {
		return bookmarkDropdown;
	}

	public void setBookmarkDropdown(WebElement bookmarkDropdown) {
		this.bookmarkDropdown = bookmarkDropdown;
	}

	public WebElement getNewBookmarkName() {
		return newBookmarkName;
	}

	public void setNewBookmarkName(WebElement newBookmarkName) {
		this.newBookmarkName = newBookmarkName;
	}

	public WebElement getStudyPeriodHeaderText() {
		return studyPeriodHeaderText;
	}

	public void setStudyPeriodHeaderText(WebElement studyPeriodHeaderText) {
		this.studyPeriodHeaderText = studyPeriodHeaderText;
	}

	public WebElement getFirstCorpEntityToggle() {
		return firstCorpEntityToggle;
	}

	public void setFirstCorpEntityToggle(WebElement firstCorpEntityToggle) {
		this.firstCorpEntityToggle = firstCorpEntityToggle;
	}

	public WebElement getFirstCorpEntityName() {
		return firstCorpEntityName;
	}

	public void setFirstCorpEntityName(WebElement firstCorpEntityName) {
		this.firstCorpEntityName = firstCorpEntityName;
	}

	public WebElement getSecondCorpEntityToggle() {
		return secondCorpEntityToggle;
	}

	public void setSecondCorpEntityToggle(WebElement secondCorpEntityToggle) {
		this.secondCorpEntityToggle = secondCorpEntityToggle;
	}

	public WebElement getSecondCorpEntityName() {
		return secondCorpEntityName;
	}

	public void setSecondCorpEntityName(WebElement secondCorpEntityName) {
		this.secondCorpEntityName = secondCorpEntityName;
	}

	public WebElement getRiskAdjustHeaderText() {
		return riskAdjustHeaderText;
	}

	public void setRiskAdjustHeaderText(WebElement riskAdjustHeaderText) {
		this.riskAdjustHeaderText = riskAdjustHeaderText;
	}

	public WebElement getRiskAdjustToggleSwitch() {
		return riskAdjustToggleSwitch;
	}

	public void setRiskAdjustToggleSwitch(WebElement riskAdjustToggleSwitch) {
		this.riskAdjustToggleSwitch = riskAdjustToggleSwitch;
	}

	public WebElement getCostTypeHeaderText() {
		return costTypeHeaderText;
	}

	public void setCostTypeHeaderText(WebElement costTypeHeaderText) {
		this.costTypeHeaderText = costTypeHeaderText;
	}

	public WebElement getCostTypeSplit() {
		return costTypeSplit;
	}

	public void setCostTypeSplit(WebElement costTypeSplit) {
		this.costTypeSplit = costTypeSplit;
	}

	public WebElement getCostTypeUnsplit() {
		return costTypeUnsplit;
	}

	public void setCostTypeUnsplit(WebElement costTypeUnsplit) {
		this.costTypeUnsplit = costTypeUnsplit;
	}

	public WebElement getDataTypeHeaderText() {
		return dataTypeHeaderText;
	}

	public void setDataTypeHeaderText(WebElement dataTypeHeaderText) {
		this.dataTypeHeaderText = dataTypeHeaderText;
	}

	public WebElement getDataTypeTotal() {
		return dataTypeTotal;
	}

	public void setDataTypeTotal(WebElement dataTypeTotal) {
		this.dataTypeTotal = dataTypeTotal;
	}

	public WebElement getDataTypeAverage() {
		return dataTypeAverage;
	}

	public void setDataTypeAverage(WebElement dataTypeAverage) {
		this.dataTypeAverage = dataTypeAverage;
	}

	public WebElement getLevelHeaderText() {
		return levelHeaderText;
	}

	public void setLevelHeaderText(WebElement levelHeaderText) {
		this.levelHeaderText = levelHeaderText;
	}

	public WebElement getLevelOne() {
		return levelOne;
	}

	public void setLevelOne(WebElement levelOne) {
		this.levelOne = levelOne;
	}

	public WebElement getLevelTwo() {
		return levelTwo;
	}

	public void setLevelTwo(WebElement levelTwo) {
		this.levelTwo = levelTwo;
	}

	public WebElement getLevelThree() {
		return levelThree;
	}

	public void setLevelThree(WebElement levelThree) {
		this.levelThree = levelThree;
	}

	public WebElement getLevelFour() {
		return levelFour;
	}

	public void setLevelFour(WebElement levelFour) {
		this.levelFour = levelFour;
	}

	public WebElement getLevelFive() {
		return levelFive;
	}

	public void setLevelFive(WebElement levelFive) {
		this.levelFive = levelFive;
	}

	public WebElement getSearchEpisodesHeaderText() {
		return searchEpisodesHeaderText;
	}

	public void setSearchEpisodesHeaderText(WebElement searchEpisodesHeaderText) {
		this.searchEpisodesHeaderText = searchEpisodesHeaderText;
	}

	public WebElement getSearchEpisodes() {
		return searchEpisodes;
	}

	public void setSearchEpisodes(WebElement searchEpisodes) {
		this.searchEpisodes = searchEpisodes;
	}

	public WebElement getAcuteTreeToggle() {
		return acuteTreeToggle;
	}

	public void setAcuteTreeToggle(WebElement acuteTreeToggle) {
		this.acuteTreeToggle = acuteTreeToggle;
	}

	public WebElement getAcuteCheckBox() {
		return acuteCheckBox;
	}

	public void setAcuteCheckBox(WebElement acuteCheckBox) {
		this.acuteCheckBox = acuteCheckBox;
	}

	public WebElement getAcuteText() {
		return acuteText;
	}

	public void setAcuteText(WebElement acuteText) {
		this.acuteText = acuteText;
	}

	public WebElement getChronicTreeToggle() {
		return chronicTreeToggle;
	}

	public void setChronicTreeToggle(WebElement chronicTreeToggle) {
		this.chronicTreeToggle = chronicTreeToggle;
	}

	public WebElement getChronicCheckBox() {
		return chronicCheckBox;
	}

	public void setChronicCheckBox(WebElement chronicCheckBox) {
		this.chronicCheckBox = chronicCheckBox;
	}

	public WebElement getChronicText() {
		return chronicText;
	}

	public void setChronicText(WebElement chronicText) {
		this.chronicText = chronicText;
	}

	public WebElement getOtherTreeToggle() {
		return otherTreeToggle;
	}

	public void setOtherTreeToggle(WebElement otherTreeToggle) {
		this.otherTreeToggle = otherTreeToggle;
	}

	public WebElement getOtherCheckBox() {
		return otherCheckBox;
	}

	public void setOtherCheckBox(WebElement otherCheckBox) {
		this.otherCheckBox = otherCheckBox;
	}

	public WebElement getOtherText() {
		return otherText;
	}

	public void setOtherText(WebElement otherText) {
		this.otherText = otherText;
	}

	public WebElement getProceduralTreeToggle() {
		return proceduralTreeToggle;
	}

	public void setProceduralTreeToggle(WebElement proceduralTreeToggle) {
		this.proceduralTreeToggle = proceduralTreeToggle;
	}

	public WebElement getProceduralCheckBox() {
		return proceduralCheckBox;
	}

	public void setProceduralCheckBox(WebElement proceduralCheckBox) {
		this.proceduralCheckBox = proceduralCheckBox;
	}

	public WebElement getProceduralText() {
		return proceduralText;
	}

	public void setProceduralText(WebElement proceduralText) {
		this.proceduralText = proceduralText;
	}

	public WebElement getSrfTreeToggle() {
		return srfTreeToggle;
	}

	public void setSrfTreeToggle(WebElement srfTreeToggle) {
		this.srfTreeToggle = srfTreeToggle;
	}

	public WebElement getSrfCheckBox() {
		return srfCheckBox;
	}

	public void setSrfCheckBox(WebElement srfCheckBox) {
		this.srfCheckBox = srfCheckBox;
	}

	public WebElement getSrfText() {
		return srfText;
	}

	public void setSrfText(WebElement srfText) {
		this.srfText = srfText;
	}

	public WebElement getSaveBookmarkButton() {
		return saveBookmarkButton;
	}

	public void setSaveBookmarkButton(WebElement saveBookmarkButton) {
		this.saveBookmarkButton = saveBookmarkButton;
	}

	public WebElement getSaveBookmarkName() {
		return saveBookmarkName;
	}

	public void setSaveBookmarkName(WebElement saveBookmarkName) {
		this.saveBookmarkName = saveBookmarkName;
	}

	public WebElement getToSaveBookmark() {
		return toSaveBookmark;
	}

	public void setToSaveBookmark(WebElement toSaveBookmark) {
		this.toSaveBookmark = toSaveBookmark;
	}

	public WebElement getToCancelBookmarkPopup() {
		return toCancelBookmarkPopup;
	}

	public void setToCancelBookmarkPopup(WebElement toCancelBookmarkPopup) {
		this.toCancelBookmarkPopup = toCancelBookmarkPopup;
	}

	public WebElement getToCloseBookmarkPopup() {
		return toCloseBookmarkPopup;
	}

	public void setToCloseBookmarkPopup(WebElement toCloseBookmarkPopup) {
		this.toCloseBookmarkPopup = toCloseBookmarkPopup;
	}

	public WebElement getBlankNameValidationText() {
		return blankNameValidationText;
	}

	public void setBlankNameValidationText(WebElement blankNameValidationText) {
		this.blankNameValidationText = blankNameValidationText;
	}

	public void setBookmarkHeaderText(WebElement bookmarkHeaderText) {
		this.bookmarkHeaderText = bookmarkHeaderText;
	}

	public WebElement getRiskAdjustOn() {
		return riskAdjustOn;
	}

	public void setRiskAdjustOn(WebElement riskAdjustOn) {
		this.riskAdjustOn = riskAdjustOn;
	}

	public WebElement getRiskAdjustOff() {
		return riskAdjustOff;
	}

	public void setRiskAdjustOff(WebElement riskAdjustOff) {
		this.riskAdjustOff = riskAdjustOff;
	}
	
	public void getLevelAndSetLevel() {
		EpisodeOpportunityFilter eFilter = new EpisodeOpportunityFilter(driver);
		
		try{
			eFilter.getChangeLevelToAltarumDefault().isDisplayed();
			String altarumLevel = eFilter.getChangeLevelToAltarumDefault().getAttribute("innerText").replaceAll("Change to level ", "");

			eFilter.getChangeLevelToAltarumDefault().click();
			
			   }
			catch(Exception e)
			{
				System.out.println("Altarum level not displayed");
				
			}
	}

	

}
