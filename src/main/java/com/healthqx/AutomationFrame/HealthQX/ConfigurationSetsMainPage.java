/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author enebo4f
 *
 */
public class ConfigurationSetsMainPage {

	WebDriver driver;
	
	//Table Headers
	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[1]")
	private WebElement hci3Version;
	
	
	public WebElement getHci3Version() {
		return hci3Version;
	}

	public void setHci3Version(WebElement hci3Version) {
		this.hci3Version = hci3Version;
	}

	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[2]")
	private WebElement sourceEpisodeCount;
	
	public WebElement getSourceEpisodeCount() {
		return sourceEpisodeCount;
	}

	public void setSourceEpisodeCount(WebElement sourceEpisodeCount) {
		this.sourceEpisodeCount = sourceEpisodeCount;
	}

	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[3]")
	private WebElement customizedEpisodeCount;
	
	public WebElement getCustomizedEpisodeCount() {
		return customizedEpisodeCount;
	}

	public void setCustomizedEpisodeCount(WebElement customizedEpisodeCount) {
		this.customizedEpisodeCount = customizedEpisodeCount;
	}

	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[4]")
	private WebElement setName;
	
	
	public WebElement getSetName() {
		return setName;
	}

	public void setSetName(WebElement setName) {
		this.setName = setName;
	}

	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[5]")
	private WebElement lastModifiedBy;
	
	public WebElement getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(WebElement lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[6]")
	private WebElement lastModifiedOn;
	
	public WebElement getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(WebElement lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[7]")
	private WebElement status;


	public WebElement getStatus() {
		return status;
	}

	public void setStatus(WebElement status) {
		this.status = status;
	}
	
	
	
	
	
	
}
