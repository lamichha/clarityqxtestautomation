/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Ashish L.
 *
 *
 *         This class will store all the locators and methods of login page.
 */
public class LoginPage {
	
	WebDriver driver;

	@FindBy(xpath = "//H1[@class='green font-25'][text()='Welcome to HealthQX!']/self::H1")
	private WebElement welcomeText;

	@FindBy(xpath = "//DIV[@class='legal']/self::DIV")
	private WebElement disclaimerText;

	//@FindBy(xpath = "//*[@id=\"footer-links\"]/div/div[2]/ul/li[1]/a")
	@FindBy(id="terms-and-conditions")
	private WebElement termsOfUse;

	
	//@FindBy(xpath = "//*[@id=\"footer-links\"]/div/div[2]/ul/li[2]/a")
	@FindBy(id="privacy-policy")
	private WebElement privacyNotice;

	//@FindBy(xpath = "//*[@id=\"footer-links\"]/div/div[2]/ul/li[3]/a")
	@FindBy(id="contact-us")
	private WebElement contactUs;

	//@FindBy(xpath = "//*[@id=\"footer-links\"]/div/div[1]")
	@FindBy(className="copyright")
	private WebElement copyRight;

	@FindBy(xpath = "//LABEL[@for='Email'][text()='Email']/self::LABEL")
	private WebElement emailLabel;

	@FindBy(xpath = "//INPUT[@id='Email']/self::INPUT")
	private WebElement emailTextBox;

	@FindBy(xpath = "//LABEL[@for='Password'][text()='Password']/self::LABEL")
	private WebElement passwordLabel;

	@FindBy(xpath = "//INPUT[@id='Password']/self::INPUT")
	private WebElement passwordTextBox;

	@FindBy(xpath = "//BUTTON[@type='submit'][text()='Sign in']/self::BUTTON")
	private WebElement signInButton;

	@FindBy(xpath = "//A[@href='/Account/ForgotPassword'][text()='Forgot your password?']/self::A")
	private WebElement forgotPwLink;
	
	@FindBy(xpath="//*[@id=\"terms-and-conditions-popup\"]/div/div/div[1]/h4")
	private WebElement termsOfUseHeader;
	
	@FindBy(xpath="//*[@id=\"terms-and-conditions-popup\"]/div/div/div[1]/button")
	private WebElement closeTermsOfUsePopup;
	
	@FindBy(xpath="//*[@id=\"privacy-policy-popup\"]/div/div/div[1]/h4")
	private WebElement privacyNoticeHeader;
	
	@FindBy(xpath="//*[@id=\"privacy-policy-popup\"]/div/div/div[1]/button")
	private WebElement closePrivacyNoticePopup;
	

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getDisclaimerText() {
		return disclaimerText;
	}

	public void setDisclaimerText(WebElement disclaimerText) {
		this.disclaimerText = disclaimerText;
	}

	public WebElement getTermsOfUseHeader() {
		return termsOfUseHeader;
	}

	public void setTermsOfUseHeader(WebElement termsOfUseHeader) {
		this.termsOfUseHeader = termsOfUseHeader;
	}

	public WebElement getCloseTermsOfUsePopup() {
		return closeTermsOfUsePopup;
	}

	public void setCloseTermsOfUsePopup(WebElement closeTermsOfUsePopup) {
		this.closeTermsOfUsePopup = closeTermsOfUsePopup;
	}

	public WebElement getPrivacyNoticeHeader() {
		return privacyNoticeHeader;
	}

	public void setPrivacyNoticeHeader(WebElement privacyNoticeHeader) {
		this.privacyNoticeHeader = privacyNoticeHeader;
	}

	public WebElement getClosePrivacyNoticePopup() {
		return closePrivacyNoticePopup;
	}

	public void setClosePrivacyNoticePopup(WebElement closePrivacyNoticePopup) {
		this.closePrivacyNoticePopup = closePrivacyNoticePopup;
	}

	public void setWelcomeText(WebElement welcomeText) {
		this.welcomeText = welcomeText;
	}

	public void setTermsOfUse(WebElement termsOfUse) {
		this.termsOfUse = termsOfUse;
	}

	public void setPrivacyNotice(WebElement privacyNotice) {
		this.privacyNotice = privacyNotice;
	}

	public void setContactUs(WebElement contactUs) {
		this.contactUs = contactUs;
	}

	public void setCopyRight(WebElement copyRight) {
		this.copyRight = copyRight;
	}

	public void setEmailLabel(WebElement emailLabel) {
		this.emailLabel = emailLabel;
	}

	public void setEmailTextBox(WebElement emailTextBox) {
		this.emailTextBox = emailTextBox;
	}

	public void setPasswordLabel(WebElement passwordLabel) {
		this.passwordLabel = passwordLabel;
	}

	public void setPasswordTextBox(WebElement passwordTextBox) {
		this.passwordTextBox = passwordTextBox;
	}

	public void setSignInButton(WebElement signInButton) {
		this.signInButton = signInButton;
	}

	public void setForgotPwLink(WebElement forgotPwLink) {
		this.forgotPwLink = forgotPwLink;
	}

	// Creating Constructor
	public LoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public WebElement getWelcomeText() {
		return welcomeText;
	}

	public WebElement getDisclaimerTexts() {

		return disclaimerText;
	}

	public WebElement getTermsOfUse() {
		return termsOfUse;
	}

	public WebElement getPrivacyNotice() {
		return privacyNotice;
	}
	public WebElement getContactUs() {
		return contactUs;
	}
	public WebElement getCopyRight() {
		return copyRight;
	}
	public WebElement getEmailLabel() {
		return emailLabel;
	}

	public WebElement getEmailTextBox() {
		return emailTextBox;
	}

	public WebElement getPasswordLabel() {
		return passwordLabel;
	}

	public WebElement getPasswordTextBox() {
		return passwordTextBox;
	}

	public WebElement getSignInButton() {
		return signInButton;
	}

	public WebElement getForgotPwLink() {
		return forgotPwLink;
	}

}
