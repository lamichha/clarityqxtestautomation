/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author enebo4f
 *
 */
public class AdministrationPage {
	
	WebDriver driver;
	
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void setOrganizations(WebElement organizations) {
		this.organizations = organizations;
	}

	public void setCorporateEntities(WebElement corporateEntities) {
		this.corporateEntities = corporateEntities;
	}

	public void setOrganizationsSso(WebElement organizationsSso) {
		this.organizationsSso = organizationsSso;
	}

	public void setCacheItems(WebElement cacheItems) {
		this.cacheItems = cacheItems;
	}

	public void setUsers(WebElement users) {
		this.users = users;
	}

	public void setRoles(WebElement roles) {
		this.roles = roles;
	}

	public void setClearCache(WebElement clearCache) {
		this.clearCache = clearCache;
	}

	//@FindBy(xpath="//A[@href='/Site-Administration/Organizations'][text()='Organizations']/self::A")
	@FindBy(xpath="//*[@id=\"sidebar\"]/div[1]/ul/li[1]/a")
	private WebElement organizations;
	
	//@FindBy(xpath="//A[@href='/Site-Administration/Corporate-Entities'][text()='Corporate Entities']/self::A")
	@FindBy(xpath="//*[@id=\"sidebar\"]/div[1]/ul/li[2]/a")
	private WebElement corporateEntities;
	
	//@FindBy(xpath="//A[@href='/Site-Administration/OrganizationSsos'][text()='Organization SSO']/self::A")
	@FindBy(xpath="//*[@id=\"sidebar\"]/div[1]/ul/li[3]/a")
	private WebElement organizationsSso;
	
	
	
	//@FindBy(xpath="//A[@href='/Site-Administration/Cache-Items'][text()='Cache Items']/self::A")
	@FindBy(xpath="//*[@id=\"sidebar\"]/div[1]/ul/li[4]/a")
	private WebElement cacheItems;
	
	//@FindBy(xpath="//A[@href='/Site-Administration/User'][text()='Users']/self::A")
	@FindBy(xpath="//*[@id=\"sidebar\"]/div[2]/ul/li[1]/a")
	private WebElement users;
	
	//@FindBy(xpath="//A[@href='/Site-Administration/Role'][text()='Roles']/self::A")
	@FindBy(xpath="//*[@id=\"sidebar\"]/div[2]/ul/li[2]/a")
	private WebElement roles;
	
	//@FindBy(xpath="//A[@class='dx-link'][text()='Click here to clear cache']/self::A")
	@FindBy(xpath="//*[@id=\"administration-home\"]/div/table/tbody/tr[1]/td[2]/a")
	private WebElement clearCache;
	
	
	//Constructor
	public AdministrationPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getOrganizations() {
		return organizations;
	}
	
	public WebElement getCorporateEntities() {
		return corporateEntities;
	}
	
	public WebElement getOrganizationsSso() {
		return organizationsSso;
	}
	
	public WebElement getCacheItems() {
		return cacheItems;
	}
	
	public WebElement getUsers() {
		return users;
	}
	
	public WebElement getRoles() {
		return roles;
	}
	
	public WebElement getClearCache() {
		return clearCache;
	}
	
	

}
