/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author enebo4f
 *
 */
public class BudgetsReconciliationMainPage {
	
	WebDriver driver;

	@FindBy(xpath="//SPAN[@class='dx-button-text'][text()='Create new Contract']/self::SPAN")
	private WebElement createNewContractLink;
	
	@FindBy(xpath="//*[@id=\"middle-column\"]/div/div[3]/div/div[1]/div[2]/div")
	private WebElement rankContractsLink ;
	
	@FindBy(xpath="//*[@id=\"middle-column\"]/div/div[3]/div/div[2]/div[2]")
	private WebElement corporateEntityFieldName;
	
	@FindBy(xpath="//*[@id=\"middle-column\"]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]")
	private WebElement corporateEntityDropDopwn;
	
	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[4]/div/div/div[1]/div/div/div/div")
	private WebElement groupedByEpisode;
	
	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[4]/div/div/div[3]/div[1]/div/div/div/i")
	private WebElement exportToExcel;
	
	@FindBy(xpath="//*[@id=\"items-datagrid\"]/div/div[4]/div/div/div[3]/div[2]/div/div/div/input")
	private WebElement contractSearchBox;
	
	//PPM Screen
	@FindBy(xpath="//*[@id=\"general-information-well\"]/div/div[2]/div[1]/div[1]/div[1]")
	private WebElement practiceNameFieldName;
	
	@FindBy(xpath="//*[@id=\"practiceNameTinAutoCompleteBox\"]/div[1]/div/input")
	private WebElement practiceNameTextBox;
	
	@FindBy(xpath="//*[@id=\"general-information-well\"]/div/div[2]/div[1]/div[2]/div[1]")
	private WebElement contractNameFieldName;
	
	@FindBy(xpath="//*[@id=\"general-information-well\"]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/input[1]")
	private WebElement contractNameTextBox;
	
	@FindBy(xpath="//DIV[@class='dx-item-content dx-list-item-content'][text()='AA1 (aa1)']/self::DIV")
	private WebElement practiceNameSelfCompleteFromDropDown;
	
	@FindBy(xpath="//*[@id=\"practice-information-well\"]/div/div[2]/div[1]/div/div[1]")
	private WebElement practiceNameFieldNamePracticeInformation;
	
	@FindBy(xpath="//*[@id=\"practice-information-well\"]/div/div[2]/div[1]/div/div[2]")
	private WebElement practiceNamePracticeInformation;
	
	
	
	@FindBy(xpath="//*[@id=\"practice-information-well\"]/div/div[2]/div[2]/div[1]/div[1]")
	private WebElement practiceTinFieldNamePracticeInformation;
	
	@FindBy(xpath="//*[@id=\"practice-information-well\"]/div/div[2]/div[2]/div[1]/div[2]")
	private WebElement practiceTinPracticeInformation;
	
	@FindBy(xpath="//*[@id=\"practice-information-well\"]/div/div[2]/div[2]/div[2]/div[1]")
	private WebElement historicalTinFieldNamePracticeInformation;
	
	@FindBy(xpath="//*[@id=\"practice-information-well\"]/div/div[2]/div[2]/div[2]/div[2]")
	private WebElement historicalTinPracticeInformation;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[3]/div/div[2]/div[1]/div[1]/div[1]")
	private WebElement configurationSetFieldName;
	
	@FindBy(xpath="//*[@id=\"configuration-set\"]/div/div/input")
	private WebElement configurationSetDropDown;
	
	//@FindBy(xpath="//body[@id='view-model-base']/div[10]//div[@class='dx-popup-content']/div[@role='listbox']//div[@class='dx-scrollview-content']/div[1]/div[@class='dx-item-content dx-list-item-content']")
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[10]/div/div[@class='dx-popup-content']/div[@role='listbox']//div[@class='dx-scrollview-content']/div[6]")
	private WebElement firstConfigurationSet;
	
	@FindBy(xpath="//body[@id='view-model-base']/div[10]//div[@class='dx-popup-content']/div[@role='listbox']//div[@role='option']/div[@class='dx-item-content dx-list-item-content']")
	private WebElement firstEpisodeName;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[3]/div/div[2]/div[1]/div[2]/div[1]")
	private WebElement episodeNameFieldName;
	
	//@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[3]/div/div[2]/div[1]/div[2]/div[2]/div[1]/div/input")
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[3]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]")
	private WebElement episodeNameDropdown;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[3]/div/div[2]/div[1]/div[3]/div[1]")
	private WebElement levelFieldName;
	
	@FindBy(xpath="//*[@id=\"test\"]/div[1]/div/input")
	private WebElement levelDropdown;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[4]/div/div[2]/div[1]/div[1]/div[1]")
	private WebElement sharedSavingRatioFieldName;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[4]/div/div[2]/div[1]/div[1]/div[2]/div/div/input")
	private WebElement sharedSavingRatioDropDown;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[4]/div/div[2]/div[1]/div[2]/div[1]")
	private WebElement relationshipCategoryFieldName;
	
	@FindBy(xpath="//*[@id=\"contract-categories-dropdown\"]/div[1]/div/input")
	private WebElement relationshipCategoryDropdown;
	
	
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[5]/div/div[2]/div[1]/div[1]/div[2]/div/div/input")
	private WebElement paymentCycleDropDown;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[5]/div/div[2]/div[1]/div[2]/div[2]/div/div/input")
	private WebElement riskPenaltyTypesDropDown;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[5]/div/div[2]/div[1]/div[3]/div[2]/div[1]/div/div[2]")
	private WebElement contractStartDatePicker ;
	
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[5]/div/div[2]/div[1]/div[4]/div[2]/div/div/div[2]")
	private WebElement contractEndDataPicker;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[5]/div/div[2]/div[1]/div[6]/div[2]/div/div/div[2]/div/div")
	private WebElement dateEleigibleForFirstPaymentDataPicker;
	
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[5]/div/div/div[2]/div[1]/div[2]/div/div[2]")
	private WebElement applyModifiersYes;
	
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[5]/div/div/div[2]/div[1]/div[2]/div/div[1]")
	private WebElement applyModifiersNo;
	
	
	//@FindBy(xpath="//*[@id=\"incentive-payment-grid\"]/div/div[6]/div/table/tbody/tr[1]/td[4]/div/div/div/input")
	//@FindBy(xpath="//div[@id='incentive-payment-grid']/div/div[6]//table[@role='grid']/tbody/tr[1]/td[4]//div[@class='dx-texteditor-container']/input[@role='spinbutton']")
	//@FindBy(xpath="//div[@id='incentive-payment-grid']/div/div[6]//table[@role='grid']/tbody/tr[1]/td[4]")
	@FindBy(xpath="//*[@id=\"incentive-payment-grid\"]/div/div[6]/div/table/tbody/tr[1]/td[4]")
	private WebElement contractAmount;
	
	//@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[2]/div/div/div/span")
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[8]/div/div[3]/div/div[3]/div[2]/div/div/div")
	private WebElement saveContractButton;
	
	//@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[3]/div/div/div")
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[8]/div/div[3]/div/div[3]/div[3]/div/div/div")
	private WebElement approveContractButton;
	
	//@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[6]/div/div/div")
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[8]/div/div[3]/div/div[3]/div[6]/div/div/div")
	private WebElement cancelContractButton;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[1]/div/div/div")
	private WebElement copyContractButton;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[5]/div/div/div")
	private WebElement terminateContractButton ;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[4]/div/div/div")
	private WebElement delteContractButton;
	
	@FindBy(xpath="//*[@id=\"contract-rank-episode-filter-popup\"]/div[1]/div/input")
	private WebElement episodeToRankDropDown;
	
	//@FindBy(xpath="//*[@class=\"dx-scrollview-content\"]/div")
	@FindBy(xpath="//*[@class=\"dx-scrollview-content\"]")
	private WebElement selectEpisodeToRank;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[1]/div/div")
	private WebElement startRanking;
	
	@FindBy(xpath="//*[@id=\"middle-column\"]/div/div[3]/div/div[1]/div[3]")
	private WebElement saveRanking;
	
	@FindBy(xpath="//div[@id='middle-column']/div/div[3]/div/div[1]/div[4]")
	private WebElement cancelRanking;
	
	//@FindBy(xpath="//div[@id='contract-popup-form']/div[5]/div[@class='dx-fieldset']/div[@class='row-fluid']/div[1]/div[3]/div[2]//input[@role='combobox']")
	@FindBy(xpath="//div[@id='contract-popup-form']/div[5]/div/div[@class='dx-fieldset']/div[2][@class='row']/div[1]/div[3]//input[@role='combobox']")
	private WebElement contractStartDateField;
	
	//@FindBy(xpath="//div[@id='contract-popup-form']/div[5]/div[@class='dx-fieldset']/div[@class='row-fluid']/div[1]/div[4]/div[2]//input[@role='combobox']")
	@FindBy(xpath="//div[@id='contract-popup-form']/div[5]/div/div[@class='dx-fieldset']/div[2][@class='row']/div[1]/div[4]//input[@role='combobox']")
	private WebElement contractEndDateField;
	
	//@FindBy(xpath="//div[@id='contract-popup-form']/div[5]/div[@class='dx-fieldset']/div[@class='row-fluid']/div[1]/div[5]/div[2]//input[@role='combobox']")
	@FindBy(xpath="//div[@id='contract-popup-form']/div[5]/div/div[@class='dx-fieldset']/div[2][@class='row']/div[1]/div[5]//input[@role='combobox']")
	private WebElement earliestEpisodeStartDateField;
	
	//@FindBy(xpath="//div[@id='contract-popup-form']/div[5]/div[@class='dx-fieldset']/div[@class='row-fluid']/div[1]/div[6]/div[2]//input[@role='combobox']")
	@FindBy(xpath="//div[@id='contract-popup-form']/div[5]/div/div[@class='dx-fieldset']/div[2][@class='row']/div[1]/div[6]//input[@role='combobox']")
	private WebElement dateEligibleForFirstPaymentField;
	
	
	
	
	public WebDriver getDriver() {
		return driver;
	}


	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}


	public WebElement getCreateNewContractLink() {
		return createNewContractLink;
	}


	public void setCreateNewContractLink(WebElement createNewContractLink) {
		this.createNewContractLink = createNewContractLink;
	}


	//Constructor
	public BudgetsReconciliationMainPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}


	public WebElement getRankContractsLink() {
		return rankContractsLink;
	}


	public void setRankContractsLink(WebElement rankContractsLink) {
		this.rankContractsLink = rankContractsLink;
	}


	public WebElement getCorporateEntityFieldName() {
		return corporateEntityFieldName;
	}


	public void setCorporateEntityFieldName(WebElement corporateEntityFieldName) {
		this.corporateEntityFieldName = corporateEntityFieldName;
	}


	public WebElement getCorporateEntityDropDopwn() {
		return corporateEntityDropDopwn;
	}


	public void setCorporateEntityDropDopwn(WebElement corporateEntityDropDopwn) {
		this.corporateEntityDropDopwn = corporateEntityDropDopwn;
	}


	public WebElement getGroupedByEpisode() {
		return groupedByEpisode;
	}


	public void setGroupedByEpisode(WebElement groupedByEpisode) {
		this.groupedByEpisode = groupedByEpisode;
	}


	public WebElement getExportToExcel() {
		return exportToExcel;
	}


	public void setExportToExcel(WebElement exportToExcel) {
		this.exportToExcel = exportToExcel;
	}


	public WebElement getContractSearchBox() {
		return contractSearchBox;
	}


	public void setContractSearchBox(WebElement contractSearchBox) {
		this.contractSearchBox = contractSearchBox;
	}


	public WebElement getPracticeNameFieldName() {
		return practiceNameFieldName;
	}


	public void setPracticeNameFieldName(WebElement practiceNameFieldName) {
		this.practiceNameFieldName = practiceNameFieldName;
	}


	public WebElement getPracticeNameTextBox() {
		return practiceNameTextBox;
	}


	public void setPracticeNameTextBox(WebElement practiceNameTextBox) {
		this.practiceNameTextBox = practiceNameTextBox;
	}


	public WebElement getContractNameFieldName() {
		return contractNameFieldName;
	}


	public void setContractNameFieldName(WebElement contractNameFieldName) {
		this.contractNameFieldName = contractNameFieldName;
	}


	public WebElement getContractNameTextBox() {
		return contractNameTextBox;
	}


	public void setContractNameTextBox(WebElement contractNameTextBox) {
		this.contractNameTextBox = contractNameTextBox;
	}


	public WebElement getPracticeNameFieldNamePracticeInformation() {
		return practiceNameFieldNamePracticeInformation;
	}


	public void setPracticeNameFieldNamePracticeInformation(WebElement practiceNameFieldNamePracticeInformation) {
		this.practiceNameFieldNamePracticeInformation = practiceNameFieldNamePracticeInformation;
	}


	public WebElement getPracticeNamePracticeInformation() {
		return practiceNamePracticeInformation;
	}


	public void setPracticeNamePracticeInformation(WebElement practiceNamePracticeInformation) {
		this.practiceNamePracticeInformation = practiceNamePracticeInformation;
	}


	public WebElement getPracticeTinFieldNamePracticeInformation() {
		return practiceTinFieldNamePracticeInformation;
	}


	public void setPracticeTinFieldNamePracticeInformation(WebElement practiceTinFieldNamePracticeInformation) {
		this.practiceTinFieldNamePracticeInformation = practiceTinFieldNamePracticeInformation;
	}


	public WebElement getPracticeTinPracticeInformation() {
		return practiceTinPracticeInformation;
	}


	public void setPracticeTinPracticeInformation(WebElement practiceTinPracticeInformation) {
		this.practiceTinPracticeInformation = practiceTinPracticeInformation;
	}


	public WebElement getHistoricalTinFieldNamePracticeInformation() {
		return historicalTinFieldNamePracticeInformation;
	}


	public void setHistoricalTinFieldNamePracticeInformation(WebElement historicalTinFieldNamePracticeInformation) {
		this.historicalTinFieldNamePracticeInformation = historicalTinFieldNamePracticeInformation;
	}


	public WebElement getHistoricalTinPracticeInformation() {
		return historicalTinPracticeInformation;
	}


	public void setHistoricalTinPracticeInformation(WebElement historicalTinPracticeInformation) {
		this.historicalTinPracticeInformation = historicalTinPracticeInformation;
	}


	public WebElement getConfigurationSetFieldName() {
		return configurationSetFieldName;
	}


	public void setConfigurationSetFieldName(WebElement configurationSetFieldName) {
		this.configurationSetFieldName = configurationSetFieldName;
	}


	public WebElement getConfigurationSetDropDown() {
		return configurationSetDropDown;
	}


	public void setConfigurationSetDropDown(WebElement configurationSetDropDown) {
		this.configurationSetDropDown = configurationSetDropDown;
	}


	public WebElement getContractStartDateField() {
		return contractStartDateField;
	}


	public void setContractStartDateField(WebElement contractStartDateField) {
		this.contractStartDateField = contractStartDateField;
	}


	public WebElement getContractEndDateField() {
		return contractEndDateField;
	}


	public void setContractEndDateField(WebElement contractEndDateField) {
		this.contractEndDateField = contractEndDateField;
	}


	public WebElement getEarliestEpisodeStartDateField() {
		return earliestEpisodeStartDateField;
	}


	public void setEarliestEpisodeStartDateField(WebElement earliestEpisodeStartDateField) {
		this.earliestEpisodeStartDateField = earliestEpisodeStartDateField;
	}


	public WebElement getDateEligibleForFirstPaymentField() {
		return dateEligibleForFirstPaymentField;
	}


	public void setDateEligibleForFirstPaymentField(WebElement dateEligibleForFirstPaymentField) {
		this.dateEligibleForFirstPaymentField = dateEligibleForFirstPaymentField;
	}


	public WebElement getEpisodeNameFieldName() {
		return episodeNameFieldName;
	}


	public void setEpisodeNameFieldName(WebElement episodeNameFieldName) {
		this.episodeNameFieldName = episodeNameFieldName;
	}


	public WebElement getEpisodeNameDropdown() {
		return episodeNameDropdown;
	}


	public void setEpisodeNameDropdown(WebElement episodeNameDropdown) {
		this.episodeNameDropdown = episodeNameDropdown;
	}


	public WebElement getLevelFieldName() {
		return levelFieldName;
	}


	public void setLevelFieldName(WebElement levelFieldName) {
		this.levelFieldName = levelFieldName;
	}


	public WebElement getLevelDropdown() {
		return levelDropdown;
	}


	public void setLevelDropdown(WebElement levelDropdown) {
		this.levelDropdown = levelDropdown;
	}


	public WebElement getSharedSavingRatioFieldName() {
		return sharedSavingRatioFieldName;
	}


	public void setSharedSavingRatioFieldName(WebElement sharedSavingRatioFieldName) {
		this.sharedSavingRatioFieldName = sharedSavingRatioFieldName;
	}


	public WebElement getSharedSavingRatioDropDown() {
		return sharedSavingRatioDropDown;
	}


	public void setSharedSavingRatioDropDown(WebElement sharedSavingRatioDropDown) {
		this.sharedSavingRatioDropDown = sharedSavingRatioDropDown;
	}


	public WebElement getRelationshipCategoryFieldName() {
		return relationshipCategoryFieldName;
	}


	public void setRelationshipCategoryFieldName(WebElement relationshipCategoryFieldName) {
		this.relationshipCategoryFieldName = relationshipCategoryFieldName;
	}


	public WebElement getRelationshipCategoryDropdown() {
		return relationshipCategoryDropdown;
	}


	public void setRelationshipCategoryDropdown(WebElement relationshipCategoryDropdown) {
		this.relationshipCategoryDropdown = relationshipCategoryDropdown;
	}


	public WebElement getPaymentCycleDropDown() {
		return paymentCycleDropDown;
	}


	public void setPaymentCycleDropDown(WebElement paymentCycleDropDown) {
		this.paymentCycleDropDown = paymentCycleDropDown;
	}


	public WebElement getRiskPenaltyTypesDropDown() {
		return riskPenaltyTypesDropDown;
	}


	public void setRiskPenaltyTypesDropDown(WebElement riskPenaltyTypesDropDown) {
		this.riskPenaltyTypesDropDown = riskPenaltyTypesDropDown;
	}


	public WebElement getContractStartDatePicker() {
		return contractStartDatePicker;
	}


	public void setContractStartDatePicker(WebElement contractStartDatePicker) {
		this.contractStartDatePicker = contractStartDatePicker;
	}


	public WebElement getContractEndDataPicker() {
		return contractEndDataPicker;
	}


	public void setContractEndDataPicker(WebElement contractEndDataPicker) {
		this.contractEndDataPicker = contractEndDataPicker;
	}


	public WebElement getDateEleigibleForFirstPaymentDataPicker() {
		return dateEleigibleForFirstPaymentDataPicker;
	}


	public void setDateEleigibleForFirstPaymentDataPicker(WebElement dateEleigibleForFirstPaymentDataPicker) {
		this.dateEleigibleForFirstPaymentDataPicker = dateEleigibleForFirstPaymentDataPicker;
	}


	public WebElement getApplyModifiersYes() {
		return applyModifiersYes;
	}


	public void setApplyModifiersYes(WebElement applyModifiersYes) {
		this.applyModifiersYes = applyModifiersYes;
	}


	public WebElement getApplyModifiersNo() {
		return applyModifiersNo;
	}


	public void setApplyModifiersNo(WebElement applyModifiersNo) {
		this.applyModifiersNo = applyModifiersNo;
	}


	public WebElement getContractAmount() {
		return contractAmount;
	}


	public void setContractAmount(WebElement contractAmount) {
		this.contractAmount = contractAmount;
	}


	public WebElement getPracticeNameSelfCompleteFromDropDown() {
		return practiceNameSelfCompleteFromDropDown;
	}


	public void setPracticeNameSelfCompleteFromDropDown(WebElement practiceNameSelfCompleteFromDropDown) {
		this.practiceNameSelfCompleteFromDropDown = practiceNameSelfCompleteFromDropDown;
	}


	public WebElement getSaveContractButton() {
		return saveContractButton;
	}


	public void setSaveContractButton(WebElement saveContractButton) {
		this.saveContractButton = saveContractButton;
	}


	public WebElement getApproveContractButton() {
		return approveContractButton;
	}


	public void setApproveContractButton(WebElement approveContractButton) {
		this.approveContractButton = approveContractButton;
	}


	public WebElement getCancelContractButton() {
		return cancelContractButton;
	}


	public void setCancelContractButton(WebElement cancelContractButton) {
		this.cancelContractButton = cancelContractButton;
	}


	public WebElement getCopyContractButton() {
		return copyContractButton;
	}


	public void setCopyContractButton(WebElement copyContractButton) {
		this.copyContractButton = copyContractButton;
	}


	public WebElement getTerminateContractButton() {
		return terminateContractButton;
	}


	public void setTerminateContractButton(WebElement terminateContractButton) {
		this.terminateContractButton = terminateContractButton;
	}


	public WebElement getDelteContractButton() {
		return delteContractButton;
	}


	public void setDelteContractButton(WebElement delteContractButton) {
		this.delteContractButton = delteContractButton;
	}
	
	//Method to select practice Name from drop down
	
/*	public void selectPracticeNameFromDropdown() {
		List <WebElement> practiceNames = driver.findElements(By.xpath("//*[@class=\"dx-scrollable dx-scrollview dx-scrollable-customizable-scrollbars dx-scrollable-vertical dx-scrollable-simulated dx-visibility-change-handler dx-list dx-widget dx-collection\"]/div[1]/div/div[2]/div"));
		
		
		
		//for(int i=1; i<practiceNames.size();i++) 
		for(WebElement practiceName : practiceNames)
		{
			//String practiceName = practiceNames.get(i).getText();
			if(practiceName.equals("abc")) {
				practiceName.click();
			}
		}
		
	}*/


	public WebElement getSelectEpisodeToRank() {
		return selectEpisodeToRank;
	}


	public void setSelectEpisodeToRank(WebElement selectEpisodeToRank) {
		this.selectEpisodeToRank = selectEpisodeToRank;
	}


	public WebElement getStartRanking() {
		return startRanking;
	}


	public void setStartRanking(WebElement startRanking) {
		this.startRanking = startRanking;
	}


	public WebElement getSaveRanking() {
		return saveRanking;
	}


	public void setSaveRanking(WebElement saveRanking) {
		this.saveRanking = saveRanking;
	}


	public WebElement getCancelRanking() {
		return cancelRanking;
	}


	public void setCancelRanking(WebElement cancelRanking) {
		this.cancelRanking = cancelRanking;
	}


	public WebElement getEpisodeToRankDropDown() {
		return episodeToRankDropDown;
	}


	public void setEpisodeToRankDropDown(WebElement episodeToRankDropDown) {
		this.episodeToRankDropDown = episodeToRankDropDown;
	}


	public WebElement getFirstConfigurationSet() {
		return firstConfigurationSet;
	}


	public void setFirstConfigurationSet(WebElement firstConfigurationSet) {
		this.firstConfigurationSet = firstConfigurationSet;
	}


	public WebElement getFirstEpisodeName() {
		return firstEpisodeName;
	}


	public void setFirstEpisodeName(WebElement firstEpisodeName) {
		this.firstEpisodeName = firstEpisodeName;
	}
	
	public String contractStartDate() {
		LocalDate date = LocalDate.now() ;
		String contractStartDate = DateTimeFormatter.ofPattern("M/d/yyyy").format(date);
		return contractStartDate;
	}
	
	public String contractEndDate() {
		LocalDate date = LocalDate.now().plusYears(2);
		String contractEndDate = DateTimeFormatter.ofPattern("M/d/yyyy").format(date);
		return contractEndDate;
	}
	
	public String earliestEpisodeStartDate() {
		LocalDate date = LocalDate.now();
		String earliestEpisodeStartDate = DateTimeFormatter.ofPattern("M/d/yyyy").format(date);
		return earliestEpisodeStartDate;
	}
	
	public String dateEligibleForFirstPayment() {
		LocalDate date = LocalDate.now().plusMonths(3);
		String dateEligibleForFirstPayment = DateTimeFormatter.ofPattern("M/d/yyyy").format(date);
		return dateEligibleForFirstPayment;
	}
	
	//@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[5]/div/div[2]/div[2]/div[3]")
	@FindBy(xpath="//*[@id=\"contract-popup-form\"]/div[5]/div/div/div[2]/div[2]/div[3]")
	private WebElement clickOutSideWithinPpmScreen;




	public WebElement getClickOutSideWithinPpmScreen() {
		return clickOutSideWithinPpmScreen;
	}


	public void setClickOutSideWithinPpmScreen(WebElement clickOutSideWithinPpmScreen) {
		this.clickOutSideWithinPpmScreen = clickOutSideWithinPpmScreen;
	}
	
	@FindBy(xpath="//div[@id='items-datagrid']/div[@class='dx-datagrid']/div[@class='dx-datagrid-headers dx-datagrid-nowrap']//table[@role='grid']/tbody/tr[2]/td[2]//input[@role='textbox']")
	private WebElement contractNameSearchBox;




	public WebElement getContractNameSearchBox() {
		return contractNameSearchBox;
	}


	public void setContractNameSearchBox(WebElement contractNameSearchBox) {
		this.contractNameSearchBox = contractNameSearchBox;
	}
	
	
	
	
}
