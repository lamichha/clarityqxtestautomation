/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Ashish L.
 *
 *
 *This class will store all locators and methods of Home/Main Page 
 */
public class NavigationTiles {

	WebDriver driver;
	
	@FindBy(xpath="//*[@id=\"content-inner\"]/div")
	private WebElement allTiles;
	
	public WebElement getAllTiles() {
		return allTiles;
	}



	public void setAllTiles(WebElement allTiles) {
		this.allTiles = allTiles;
	}


	//
	@FindBy(xpath="//A[@class='btn'][text()='Episode Opportunity']/following-sibling::DIV") 
	//@FindBy(xpath="//*[@id=\"content-inner\"]/div[1]/div/div/div[1]/a")
	private WebElement episodeOpportunityTile;
	
	//
	@FindBy(xpath="//A[@class='btn'][text()='Network Analysis']/self::A")
	private WebElement networkAnalysisTile;
	//
	@FindBy(xpath="//A[@class='btn'][text()='Episode Configuration']/self::A")
	private WebElement hci3DataSetTile;
	
	//
	@FindBy(xpath="//A[@class='btn'][contains(text(),'Budgets ')]/self::A")
	private WebElement budgetsReconciliation;
	
	//
	@FindBy(xpath="//IMG[@src='/Static/img/logo-hci3-certification-medium.png']/self::IMG")
	private WebElement hci3Stamp;
		
	
	public WebDriver getDriver() {
		return driver;
	}



	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}



	public WebElement getEpisodeOpportunityTile() {
		return episodeOpportunityTile;
	}



	public void setEpisodeOpportunityTile(WebElement episodeOpportunityTile) {
		this.episodeOpportunityTile = episodeOpportunityTile;
	}



	public WebElement getNetworkAnalysisTile() {
		return networkAnalysisTile;
	}



	public void setNetworkAnalysisTile(WebElement networkAnalysisTile) {
		this.networkAnalysisTile = networkAnalysisTile;
	}



	public WebElement getHci3DataSetTile() {
		return hci3DataSetTile;
	}



	public void setHci3DataSetTile(WebElement hci3DataSetTile) {
		this.hci3DataSetTile = hci3DataSetTile;
	}



	public WebElement getBudgetsReconciliation() {
		return budgetsReconciliation;
	}



	public void setBudgetsReconciliation(WebElement budgetsReconciliation) {
		this.budgetsReconciliation = budgetsReconciliation;
	}



	public WebElement getHci3Stamp() {
		return hci3Stamp;
	}



	public void setHci3Stamp(WebElement hci3Stamp) {
		this.hci3Stamp = hci3Stamp;
	}



	//Creating COnstructor
	public NavigationTiles(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	//method to navigate to episode Opportunity page
	public void navigateToEpisodeOpportunity() {
		NavigationTiles navigation = new NavigationTiles(driver);
		WebElement episodeOpportunityTile = navigation.getEpisodeOpportunityTile();
		
		for (int i=0 ;i<=2;i++){
			try{
			episodeOpportunityTile.click();
				
			break;
			}
			catch(Exception e){
				System.out.println(e.getMessage());
			}
			}
		
		}
	
	
	//method to navigate to Network Analysis page
		public void navigateToNetworkAnalysis() {
			NavigationTiles navigation = new NavigationTiles(driver);
			WebElement networkAnalysisTile = navigation.getNetworkAnalysisTile();
			
			for (int i=0 ;i<=2;i++){
				try{
					networkAnalysisTile.click();
					
				break;
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
				}
			
			}
		
	
	//method to navigate to Budgets & Reconciliation page
	public void navigateToBudgetsReconciliation() {
		NavigationTiles navigation = new NavigationTiles(driver);
		WebElement budgetsReconciliation = navigation.getBudgetsReconciliation();
		
		for (int i=0 ;i<=2;i++){
			try{
				budgetsReconciliation.click();
			break;
			}
			catch(Exception e){
				System.out.println(e.getMessage());
			}
			}
	}
	
	
	
	
}
