package com.healthqx.AutomationFrame.HealthQX;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NetworkAnalysisBenchmarkReportTableValues {
WebDriver driver;
WebElement element;
	//@FindBy(xpath="//*[@id=\"cost-breakdown-datagrid\"]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody")
	@FindBy(xpath="//*[@id=\"cost-breakdown-datagrid\"]/div/div[6]/div/table/tbody")
	private WebElement allTableData;
	
	public List<NetworkAnalysisBenchmarkReportTableHeaders> rowValues(){
		List <NetworkAnalysisBenchmarkReportTableHeaders> rowList = new ArrayList<NetworkAnalysisBenchmarkReportTableHeaders>();
		List<WebElement> tableData = allTableData.findElements(By.tagName("tr"));
		
		for(int rNum=0; rNum<tableData.size()-2; rNum++) {
			List<WebElement> data = tableData.get(rNum).findElements(By.tagName("td"));
			NetworkAnalysisBenchmarkReportTableHeaders header = new NetworkAnalysisBenchmarkReportTableHeaders();
			
			//This will work when practiceName does not have a parenthesis
			header.setPractice(data.get(1).getAttribute("innerText").substring(9).replaceAll("\\(.*", "").trim());
			
			header.setAverageCost(new BigDecimal(data.get(8).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			
			rowList.add(header);
		}
		return rowList;
	}
	
	

	public WebElement getAllTableData() {
		return allTableData;
	}

	public void setAllTableData(WebElement allTableData) {
		this.allTableData = allTableData;
	}
	
	//Constructor
	public NetworkAnalysisBenchmarkReportTableValues(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
}