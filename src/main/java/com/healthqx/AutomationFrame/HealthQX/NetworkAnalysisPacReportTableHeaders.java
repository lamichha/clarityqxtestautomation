package com.healthqx.AutomationFrame.HealthQX;

import java.math.BigDecimal;

public class NetworkAnalysisPacReportTableHeaders {

	private String group;
	private String groupCode;
	private String groupName;
	private String dxCodeUi;
	private String dxCode;
	private String dxName; 
	private BigDecimal memberCount;
	private BigDecimal groupMembersWithPacPercent;
	private BigDecimal averageCost;
	private BigDecimal totalCost;
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getDxCodeUi() {
		return dxCodeUi;
	}
	public void setDxCodeUi(String dxCodeUi) {
		this.dxCodeUi = dxCodeUi;
	}
	public String getDxCode() {
		return dxCode;
	}
	public void setDxCode(String dxCode) {
		this.dxCode = dxCode;
	}
	public String getDxName() {
		return dxName;
	}
	public void setDxName(String dxName) {
		this.dxName = dxName;
	}
	public BigDecimal getMemberCount() {
		return memberCount;
	}
	public void setMemberCount(BigDecimal memberCount) {
		this.memberCount = memberCount;
	}
	public BigDecimal getGroupMembersWithPacPercent() {
		return groupMembersWithPacPercent;
	}
	public void setGroupMembersWithPacPercent(BigDecimal groupMembersWithPacPercent) {
		this.groupMembersWithPacPercent = groupMembersWithPacPercent;
	}
	public BigDecimal getAverageCost() {
		return averageCost;
	}
	public void setAverageCost(BigDecimal averageCost) {
		this.averageCost = averageCost;
	}
	public BigDecimal getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}
	
	
	
	
}
