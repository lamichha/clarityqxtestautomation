/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author enebo4f
 *
 */
public class PartnerProfileManagementPage {

	WebDriver driver;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[1]/h4")
	private WebElement ppmScreenTitle;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[1]/h4/span[1]")
	private WebElement contractStatus;
	
	@FindBy(id="practiceNameTinAutoCompleteBox")
	private WebElement practiceNameTINInputBox;
	
	@FindBy(xpath="//*[@id=\"general-information-well\"]/div/div[2]/div[1]/div[2]/div[2]/div/input")
	private WebElement contractNameInputBox;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[1]/div/div")
	private WebElement saveContractButton;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[2]/div/div")
	private WebElement approveContractButton;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[3]/div/div")
	private WebElement deleteContractButton;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[4]/div/div")
	private WebElement terminateContractButton;
	
	@FindBy(xpath="//*[@id=\"view-model-base\"]/div[9]/div/div[3]/div/div[3]/div[5]/div/div")
	private WebElement cancelContractButton;
	
	
	
	
	//Creating Constructor
	public PartnerProfileManagementPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}




	public WebDriver getDriver() {
		return driver;
	}




	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}




	public WebElement getPpmScreenTitle() {
		return ppmScreenTitle;
	}




	public void setPpmScreenTitle(WebElement ppmScreenTitle) {
		this.ppmScreenTitle = ppmScreenTitle;
	}




	public WebElement getContractStatus() {
		return contractStatus;
	}




	public void setContractStatus(WebElement contractStatus) {
		this.contractStatus = contractStatus;
	}




	public WebElement getPracticeNameTINInputBox() {
		return practiceNameTINInputBox;
	}




	public void setPracticeNameTINInputBox(WebElement practiceNameTINInputBox) {
		this.practiceNameTINInputBox = practiceNameTINInputBox;
	}




	public WebElement getContractNameInputBox() {
		return contractNameInputBox;
	}




	public void setContractNameInputBox(WebElement contractNameInputBox) {
		this.contractNameInputBox = contractNameInputBox;
	}




	public WebElement getSaveContractButton() {
		return saveContractButton;
	}




	public void setSaveContractButton(WebElement saveContractButton) {
		this.saveContractButton = saveContractButton;
	}




	public WebElement getApproveContractButton() {
		return approveContractButton;
	}




	public void setApproveContractButton(WebElement approveContractButton) {
		this.approveContractButton = approveContractButton;
	}




	public WebElement getDeleteContractButton() {
		return deleteContractButton;
	}




	public void setDeleteContractButton(WebElement deleteContractButton) {
		this.deleteContractButton = deleteContractButton;
	}




	public WebElement getTerminateContractButton() {
		return terminateContractButton;
	}




	public void setTerminateContractButton(WebElement terminateContractButton) {
		this.terminateContractButton = terminateContractButton;
	}




	public WebElement getCancelContractButton() {
		return cancelContractButton;
	}




	public void setCancelContractButton(WebElement cancelContractButton) {
		this.cancelContractButton = cancelContractButton;
	}
	
	
}
