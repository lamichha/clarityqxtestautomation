package com.healthqx.AutomationFrame.HealthQX;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NetworkAnalysisTableValuesRiskOn {
	
	WebDriver driver;
	WebElement element;
	@FindBy(xpath="//*[@id=\"grid\"]/div[1]/div[6]//table/tbody") private WebElement allTableData;
	
	public List<NetworkAnalysisTableHeaders> rowValues() {
	List<NetworkAnalysisTableHeaders> rowList = new ArrayList<NetworkAnalysisTableHeaders>();
			
	List<WebElement> tableData = allTableData.findElements(By.tagName("tr")) ;
	for(int rNum=0; rNum<tableData.size()-1;  rNum++ ) { 
		List<WebElement> data = tableData.get(rNum).findElements(By.tagName("td")) ;
		
		NetworkAnalysisTableHeaders header = new NetworkAnalysisTableHeaders();
		
		header.setPracticeName(data.get(0).getAttribute("innerText").trim());
		header.setTin(data.get(1).getAttribute("innerText").trim());
		header.setEpisodeCounts(new BigDecimal(data.get(2).getAttribute("innerText").trim()));
		header.setAverageTypical(new BigDecimal(data.get(3).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		header.setAverageCompCost(new BigDecimal(data.get(4).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		header.setAveragePacCost(new BigDecimal(data.get(5).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		header.setAverageCost(new BigDecimal(data.get(6).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		if(!data.get(7).getAttribute("innerText").trim().contains("N/A")) {
			header.setRiskAverage(new BigDecimal(data.get(7).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		}
		else {
			
			header.setRiskAverage(new BigDecimal(0.00));
		}
		
		
		header.setTotalTypicalCost(new BigDecimal(data.get(8).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		header.setTotalCompCost(new BigDecimal(data.get(9).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		header.setTotalPacCost(new BigDecimal(data.get(10).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		header.setTotalCost(new BigDecimal(data.get(11).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		
		if(!data.get(12).getAttribute("innerText").replaceAll("[$,]", "").trim().contains("N/A")) {
			header.setRiskTotal(new BigDecimal(data.get(12).getAttribute("innerText").replaceAll("[$,]", "").trim()));;
			
		}
		else {
			header.setRiskTotal(new BigDecimal(0.00));;
		}
		
		
		//header.setPotentialSavings(new BigDecimal(data.get(13).getAttribute("innerText").replaceAll("[$,]", "").trim()));
		
		header.setContract(data.get(13).getAttribute("innerText").trim());
		
		//header.setContract(data.get(13).getCssValue("font-weight"));
		
		
		rowList.add(header);
		
		
		
	}
	return rowList;
	
	}

	public WebElement getAllTableData() {
		return allTableData;
	}

	public void setAllTableData(WebElement allTableData) {
		this.allTableData = allTableData;
	}
	
	//constructor
	public NetworkAnalysisTableValuesRiskOn(WebDriver driver)
	{
		//this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	

}
