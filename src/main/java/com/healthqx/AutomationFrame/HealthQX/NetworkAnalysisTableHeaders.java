package com.healthqx.AutomationFrame.HealthQX;

import java.math.BigDecimal;


public class NetworkAnalysisTableHeaders {

	private String practiceName;
	
	private String tin;
	
	private BigDecimal episodeCounts;
	
	private BigDecimal averageTypical;
	
	private BigDecimal averageCompCost;
	
	private BigDecimal averagePacCost;
	
	private BigDecimal averageCost;
	
	private BigDecimal totalTypicalCost;
	
	private BigDecimal totalCompCost;
	
	private BigDecimal totalPacCost;
	
	private BigDecimal totalCost;
	
	private BigDecimal potentialTargetCost;
	
	private BigDecimal potentialSavings;
	
	private String contract ;
	
	private BigDecimal riskAverage;
	
	private BigDecimal riskTotal;
	

	

	public String getPracticeName() {
		return practiceName;
	}

	public void setPracticeName(String episodeName) {
		this.practiceName = episodeName;
	}

	public String getTin() {
		return tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public BigDecimal getEpisodeCounts() {
		return episodeCounts;
	}

	public void setEpisodeCounts(BigDecimal episodes) {
		this.episodeCounts = episodes;
	}

	public BigDecimal getAverageTypical() {
		return averageTypical;
	}

	public void setAverageTypical(BigDecimal averageTypical) {
		this.averageTypical = averageTypical;
	}

	public BigDecimal getAverageCompCost() {
		return averageCompCost;
	}

	public void setAverageCompCost(BigDecimal averageCompCost) {
		this.averageCompCost = averageCompCost;
	}

	public BigDecimal getAveragePacCost() {
		return averagePacCost;
	}

	public void setAveragePacCost(BigDecimal averagePacCost) {
		this.averagePacCost = averagePacCost;
	}

	public BigDecimal getAverageCost() {
		return averageCost;
	}

	public void setAverageCost(BigDecimal averageCost) {
		this.averageCost = averageCost;
	}

	public BigDecimal getTotalTypicalCost() {
		return totalTypicalCost;
	}

	public void setTotalTypicalCost(BigDecimal totalTypicalCost) {
		this.totalTypicalCost = totalTypicalCost;
	}

	public BigDecimal getTotalCompCost() {
		return totalCompCost;
	}

	public void setTotalCompCost(BigDecimal totalCompCost) {
		this.totalCompCost = totalCompCost;
	}

	public BigDecimal getTotalPacCost() {
		return totalPacCost;
	}

	public void setTotalPacCost(BigDecimal totalPacCost) {
		this.totalPacCost = totalPacCost;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public BigDecimal getPotentialTargetCost() {
		return potentialTargetCost;
	}

	public void setPotentialTargetCost(BigDecimal potentialTargetCost) {
		this.potentialTargetCost = potentialTargetCost;
	}

	public BigDecimal getPotentialSavings() {
		return potentialSavings;
	}

	public void setPotentialSavings(BigDecimal potentialSavings) {
		this.potentialSavings = potentialSavings;
	}

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public BigDecimal getRiskAverage() {
		return riskAverage;
	}

	public void setRiskAverage(BigDecimal riskAverage) {
		this.riskAverage = riskAverage;
	}

	public BigDecimal getRiskTotal() {
		return riskTotal;
	}

	public void setRiskTotal(BigDecimal riskTotal) {
		this.riskTotal = riskTotal;
	} 
	
	
}
