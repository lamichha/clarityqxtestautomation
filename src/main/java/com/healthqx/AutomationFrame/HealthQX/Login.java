/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.healthqx.AutomationFrame.Utility.DataBaseConnector;

/**
 * @author Ashish L.
 *
 *
 *         This class will store all the locators and methods of login page.
 */
public class Login {

	WebDriver driver;
	
	DataBaseConnector dbConnector;

	//By username = By.xpath("//INPUT[@id='Email']/self::INPUT");
	By username = By.id("Email");
	//By password = By.xpath("//INPUT[@id='Password']/self::INPUT");
	By password = By.id("Password");
	By loginbutton = By.xpath("//BUTTON[@type='submit'][text()='Sign in']/self::BUTTON");
	By codeBox = By.xpath("//INPUT[@id='Code']/self::INPUT");
	By verifyCodeButton = By.xpath("//BUTTON[@type='submit'][contains(text(),'Verify Code ')]/self::BUTTON");

	
	// Creating Constructor
	public Login(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// Creating method to login to application
	public void loginToApp(String uid, String pwd) {

		driver.findElement(username).sendKeys(uid);
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(loginbutton).click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	// for 2FA code is under construction
	public void twoFactorAuth() {
		
		String SqlQuery = "SELECT Token FROM dbo.[User] where USERNAME='ashish.lamichhane@mckesson.com';";
		String verifCode = dbConnector.executeQuerySingleValue(SqlQuery);
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElement(codeBox).sendKeys(verifCode);
		driver.findElement(verifyCodeButton).click();
	}

	
}
