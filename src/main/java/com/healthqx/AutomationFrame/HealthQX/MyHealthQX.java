/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author enebo4f
 *
 */
public class MyHealthQX {
	WebDriver driver;
	
	@FindBy(xpath="//SPAN[@class='dx-tab-text'][text()='My Preferences']/self::SPAN")
	private WebElement myPrefences;
	
	@FindBy(xpath="//SPAN[@class='dx-tab-text'][text()='My Bookmarks']/self::SPAN")
	private WebElement myBookmarks;
	
	@FindBy(xpath="//H5[@data-bind='text: $data.Name'][text()='AakomlaviIsNotSmart']/self::H5")
	private WebElement newBookmark;
	
	//constructor
	public MyHealthQX(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getMyPrefences() {
		return myPrefences;
	}

	public void setMyPrefences(WebElement myPrefences) {
		this.myPrefences = myPrefences;
	}

	public WebElement getMyBookmarks() {
		return myBookmarks;
	}

	public void setMyBookmarks(WebElement myBookmarks) {
		this.myBookmarks = myBookmarks;
	}

	public WebElement getNewBookmark() {
		return newBookmark;
	}

	public void setNewBookmark(WebElement newBookmark) {
		this.newBookmark = newBookmark;
	}
	
	
	

}
