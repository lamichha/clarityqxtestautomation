package com.healthqx.AutomationFrame.HealthQX;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NetworkAnalysisCostPerMemberTableValues {
WebDriver driver;
WebElement element;
	@FindBy(xpath="//*[@id=\"cost-breakdown-datagrid\"]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody")
	private WebElement allTableData;
	
	public List<NetworkAnalysisCostPerMemberTableHeaders> rowValues(){
		List <NetworkAnalysisCostPerMemberTableHeaders> rowList = new ArrayList<NetworkAnalysisCostPerMemberTableHeaders>();
		List<WebElement> tableData = allTableData.findElements(By.tagName("tr"));
		
		for(int rNum=0; rNum<tableData.size()-1; rNum++) {
			List<WebElement> data = tableData.get(rNum).findElements(By.tagName("td"));
			NetworkAnalysisCostPerMemberTableHeaders header = new NetworkAnalysisCostPerMemberTableHeaders();
			
			header.setMember(data.get(0).getAttribute("innerText").trim());
			header.setProvider(data.get(1).getAttribute("innerText").trim());
			header.setCcsGroup(data.get(2).getAttribute("innerText").trim());
			header.setCcsName(data.get(3).getAttribute("innerText").trim());
			header.setClaimType(data.get(4).getAttribute("innerText").trim());
			header.setPlaceOfService(data.get(5).getAttribute("innerText").trim());
			
			header.setAveragePreTrigger(new BigDecimal(data.get(6).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setPreTriggerPacPercent(new BigDecimal(data.get(7).getAttribute("innerText").replaceAll("[%]", "").trim()));
			header.setAverageTrigger(new BigDecimal(data.get(8).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setTriggerPacPercent(new BigDecimal(data.get(9).getAttribute("innerText").replaceAll("[%]", "").trim()));
			header.setAveragePostTrigger(new BigDecimal(data.get(10).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setPostTriggerPacPercent(new BigDecimal(data.get(11).getAttribute("innerText").replaceAll("[%]", "").trim()));
			header.setAverageCost(new BigDecimal(data.get(12).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setPacPercent(new BigDecimal(data.get(13).getAttribute("innerText").replaceAll("[%]", "").trim()));
			
			
			rowList.add(header);
		}
		return rowList;
	}
	
	

	public WebElement getAllTableData() {
		return allTableData;
	}

	public void setAllTableData(WebElement allTableData) {
		this.allTableData = allTableData;
	}
	
	//Constructor
	public NetworkAnalysisCostPerMemberTableValues(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
}
