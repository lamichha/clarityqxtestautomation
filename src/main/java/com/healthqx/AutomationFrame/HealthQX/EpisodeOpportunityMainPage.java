/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;

import java.util.List;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

/**
 * @author Ashish Lamichhane
 *
 */
public class EpisodeOpportunityMainPage {
	WebDriver driver;

	
	
	@FindBys(@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[10]/div[1]/div"))
	private List <WebElement> paginationNumberOfRecords;
	
	

	public List<WebElement> getPaginationNumberOfRecords() {
		return paginationNumberOfRecords;
	}


	public void setPaginationNumberOfRecords(List<WebElement> paginationNumberOfRecords) {
		this.paginationNumberOfRecords = paginationNumberOfRecords;
	}


	//// Total Costs Donut Starts////////
	// Total costs donut
	@FindBy(xpath = "//*[@id=\"episode-types-piechart\"]/svg/rect")
	private WebElement totalCostsDonut;

	public WebDriver getDriver() {
		return driver;
	}


	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}


	public WebElement getTotalCostsDonut() {
		return totalCostsDonut;
	}


	public void setTotalCostsDonut(WebElement totalCostsDonut) {
		this.totalCostsDonut = totalCostsDonut;
	}


	public WebElement getTotalCostOneCategory() {
		return totalCostOneCategory;
	}


	public void setTotalCostOneCategory(WebElement totalCostOneCategory) {
		this.totalCostOneCategory = totalCostOneCategory;
	}


	public WebElement getChronicTotalCostPercent() {
		return ChronicTotalCostPercent;
	}


	public void setChronicTotalCostPercent(WebElement chronicTotalCostPercent) {
		ChronicTotalCostPercent = chronicTotalCostPercent;
	}


	public WebElement getProceduralTotalCostPercent() {
		return proceduralTotalCostPercent;
	}


	public void setProceduralTotalCostPercent(WebElement proceduralTotalCostPercent) {
		this.proceduralTotalCostPercent = proceduralTotalCostPercent;
	}


	public WebElement getOtherTotalCostPercent() {
		return otherTotalCostPercent;
	}


	public void setOtherTotalCostPercent(WebElement otherTotalCostPercent) {
		this.otherTotalCostPercent = otherTotalCostPercent;
	}


	public WebElement getAcuteTotalCostPercent() {
		return acuteTotalCostPercent;
	}


	public void setAcuteTotalCostPercent(WebElement acuteTotalCostPercent) {
		this.acuteTotalCostPercent = acuteTotalCostPercent;
	}


	public WebElement getSrfTotalCostPercent() {
		return srfTotalCostPercent;
	}


	public void setSrfTotalCostPercent(WebElement srfTotalCostPercent) {
		this.srfTotalCostPercent = srfTotalCostPercent;
	}


	public WebElement getMemberCountsDonut() {
		return memberCountsDonut;
	}


	public void setMemberCountsDonut(WebElement memberCountsDonut) {
		this.memberCountsDonut = memberCountsDonut;
	}


	public WebElement getTotalMemberOneCategory() {
		return totalMemberOneCategory;
	}


	public void setTotalMemberOneCategory(WebElement totalMemberOneCategory) {
		this.totalMemberOneCategory = totalMemberOneCategory;
	}


	public WebElement getChronicMemberCountPercent() {
		return chronicMemberCountPercent;
	}


	public void setChronicMemberCountPercent(WebElement chronicMemberCountPercent) {
		this.chronicMemberCountPercent = chronicMemberCountPercent;
	}


	public WebElement getProceduralMemberCountPercent() {
		return proceduralMemberCountPercent;
	}


	public void setProceduralMemberCountPercent(WebElement proceduralMemberCountPercent) {
		this.proceduralMemberCountPercent = proceduralMemberCountPercent;
	}


	public WebElement getAcuteMemberCountPercent() {
		return acuteMemberCountPercent;
	}


	public void setAcuteMemberCountPercent(WebElement acuteMemberCountPercent) {
		this.acuteMemberCountPercent = acuteMemberCountPercent;
	}


	public WebElement getOtherMemberCountPercent() {
		return otherMemberCountPercent;
	}


	public void setOtherMemberCountPercent(WebElement otherMemberCountPercent) {
		this.otherMemberCountPercent = otherMemberCountPercent;
	}


	public WebElement getSrfMemberCountPercent() {
		return srfMemberCountPercent;
	}


	public void setSrfMemberCountPercent(WebElement srfMemberCountPercent) {
		this.srfMemberCountPercent = srfMemberCountPercent;
	}


	public WebElement getCostsListTable() {
		return costsListTable;
	}


	public void setCostsListTable(WebElement costsListTable) {
		this.costsListTable = costsListTable;
	}


	public WebElement getTypicalCostFromList() {
		return typicalCostFromList;
	}


	public void setTypicalCostFromList(WebElement typicalCostFromList) {
		this.typicalCostFromList = typicalCostFromList;
	}


	public WebElement getTypicalWithCompCostFromList() {
		return typicalWithCompCostFromList;
	}


	public void setTypicalWithCompCostFromList(WebElement typicalWithCompCostFromList) {
		this.typicalWithCompCostFromList = typicalWithCompCostFromList;
	}


	public WebElement getPacCostFromList() {
		return pacCostFromList;
	}


	public void setPacCostFromList(WebElement pacCostFromList) {
		this.pacCostFromList = pacCostFromList;
	}


	public WebElement getTotalCostFromList() {
		return totalCostFromList;
	}


	public void setTotalCostFromList(WebElement totalCostFromList) {
		this.totalCostFromList = totalCostFromList;
	}


	public WebElement getSearchEpisodesFromTable() {
		return searchEpisodesFromTable;
	}


	public void setSearchEpisodesFromTable(WebElement searchEpisodesFromTable) {
		this.searchEpisodesFromTable = searchEpisodesFromTable;
	}


	public WebElement getExportToExcelIcon() {
		return exportToExcelIcon;
	}


	public void setExportToExcelIcon(WebElement exportToExcelIcon) {
		this.exportToExcelIcon = exportToExcelIcon;
	}


	public WebElement getTableHeader() {
		return tableHeader;
	}


	public void setTableHeader(WebElement tableHeader) {
		this.tableHeader = tableHeader;
	}


	public WebElement getEpisodeTableHeader() {
		return episodeTableHeader;
	}


	public void setEpisodeTableHeader(WebElement episodeTableHeader) {
		this.episodeTableHeader = episodeTableHeader;
	}


	public WebElement getCategoryTableHeader() {
		return categoryTableHeader;
	}


	public void setCategoryTableHeader(WebElement categoryTableHeader) {
		this.categoryTableHeader = categoryTableHeader;
	}


	public WebElement getMemberTableHeader() {
		return memberTableHeader;
	}


	public void setMemberTableHeader(WebElement memberTableHeader) {
		this.memberTableHeader = memberTableHeader;
	}


	public WebElement getProvidersTableHeader() {
		return providersTableHeader;
	}


	public void setProvidersTableHeader(WebElement providersTableHeader) {
		this.providersTableHeader = providersTableHeader;
	}


	public WebElement getAverageTypicalTableHeader() {
		return averageTypicalTableHeader;
	}


	public void setAverageTypicalTableHeader(WebElement averageTypicalTableHeader) {
		this.averageTypicalTableHeader = averageTypicalTableHeader;
	}


	public WebElement getAverageCompTableHeader() {
		return averageCompTableHeader;
	}


	public void setAverageCompTableHeader(WebElement averageCompTableHeader) {
		this.averageCompTableHeader = averageCompTableHeader;
	}


	public WebElement getAveragePacTableHeader() {
		return averagePacTableHeader;
	}


	public void setAveragePacTableHeader(WebElement averagePacTableHeader) {
		this.averagePacTableHeader = averagePacTableHeader;
	}


	public WebElement getAverageTableHeader() {
		return averageTableHeader;
	}


	public void setAverageTableHeader(WebElement averageTableHeader) {
		this.averageTableHeader = averageTableHeader;
	}


	public WebElement getRiskAverageTableHeader() {
		return riskAverageTableHeader;
	}


	public void setRiskAverageTableHeader(WebElement riskAverageTableHeader) {
		this.riskAverageTableHeader = riskAverageTableHeader;
	}


	public WebElement getTotalTypicalTableHeader() {
		return totalTypicalTableHeader;
	}


	public void setTotalTypicalTableHeader(WebElement totalTypicalTableHeader) {
		this.totalTypicalTableHeader = totalTypicalTableHeader;
	}


	public WebElement getTotalCompTableHeader() {
		return totalCompTableHeader;
	}


	public void setTotalCompTableHeader(WebElement totalCompTableHeader) {
		this.totalCompTableHeader = totalCompTableHeader;
	}


	public WebElement getTotalPacTableHeader() {
		return totalPacTableHeader;
	}


	public void setTotalPacTableHeader(WebElement totalPacTableHeader) {
		this.totalPacTableHeader = totalPacTableHeader;
	}


	public WebElement getTotalTableHeader() {
		return totalTableHeader;
	}


	public void setTotalTableHeader(WebElement totalTableHeader) {
		this.totalTableHeader = totalTableHeader;
	}


	public WebElement getRiskTotalTableHeader() {
		return riskTotalTableHeader;
	}


	public void setRiskTotalTableHeader(WebElement riskTotalTableHeader) {
		this.riskTotalTableHeader = riskTotalTableHeader;
	}


	public WebElement getTableTotalNumberOfEpisodes() {
		return tableTotalNumberOfEpisodes;
	}


	public void setTableTotalNumberOfEpisodes(WebElement tableTotalNumberOfEpisodes) {
		this.tableTotalNumberOfEpisodes = tableTotalNumberOfEpisodes;
	}


	public WebElement getTableTotalNumberOfMembers() {
		return tableTotalNumberOfMembers;
	}


	public void setTableTotalNumberOfMembers(WebElement tableTotalNumberOfMembers) {
		this.tableTotalNumberOfMembers = tableTotalNumberOfMembers;
	}


	public WebElement getTableTotalNumberOfProviders() {
		return tableTotalNumberOfProviders;
	}


	public void setTableTotalNumberOfProviders(WebElement tableTotalNumberOfProviders) {
		this.tableTotalNumberOfProviders = tableTotalNumberOfProviders;
	}


	public WebElement getTableTotalAverageTypical() {
		return tableTotalAverageTypical;
	}


	public void setTableTotalAverageTypical(WebElement tableTotalAverageTypical) {
		this.tableTotalAverageTypical = tableTotalAverageTypical;
	}


	public WebElement getTableTotalAverageComp() {
		return tableTotalAverageComp;
	}


	public void setTableTotalAverageComp(WebElement tableTotalAverageComp) {
		this.tableTotalAverageComp = tableTotalAverageComp;
	}


	public WebElement getTableTotalAveragePac() {
		return tableTotalAveragePac;
	}


	public void setTableTotalAveragePac(WebElement tableTotalAveragePac) {
		this.tableTotalAveragePac = tableTotalAveragePac;
	}


	public WebElement getTableTotalAverage() {
		return tableTotalAverage;
	}


	public void setTableTotalAverage(WebElement tableTotalAverage) {
		this.tableTotalAverage = tableTotalAverage;
	}


	public WebElement getTableTotalRiskAverage() {
		return tableTotalRiskAverage;
	}


	public void setTableTotalRiskAverage(WebElement tableTotalRiskAverage) {
		this.tableTotalRiskAverage = tableTotalRiskAverage;
	}


	public WebElement getTableTotalTypical() {
		return tableTotalTypical;
	}


	public void setTableTotalTypical(WebElement tableTotalTypical) {
		this.tableTotalTypical = tableTotalTypical;
	}


	public WebElement getTableTotalComp() {
		return tableTotalComp;
	}


	public void setTableTotalComp(WebElement tableTotalComp) {
		this.tableTotalComp = tableTotalComp;
	}


	public WebElement getTableTotalPac() {
		return tableTotalPac;
	}


	public void setTableTotalPac(WebElement tableTotalPac) {
		this.tableTotalPac = tableTotalPac;
	}


	public WebElement getTableTotalCost() {
		return tableTotalCost;
	}


	public void setTableTotalCost(WebElement tableTotalCost) {
		this.tableTotalCost = tableTotalCost;
	}


	public WebElement getTableTotalRisk() {
		return tableTotalRisk;
	}


	public void setTableTotalRisk(WebElement tableTotalRisk) {
		this.tableTotalRisk = tableTotalRisk;
	}


	public WebElement getFirstEpisodeFromBarChart() {
		return firstEpisodeFromBarChart;
	}


	public void setFirstEpisodeFromBarChart(WebElement firstEpisodeFromBarChart) {
		this.firstEpisodeFromBarChart = firstEpisodeFromBarChart;
	}


	public WebElement getFirstEpisodeFromTable() {
		return firstEpisodeFromTable;
	}


	public void setFirstEpisodeFromTable(WebElement firstEpisodeFromTable) {
		this.firstEpisodeFromTable = firstEpisodeFromTable;
	}


	// Only one category selected
	@FindBy(css = "#episode-types-piechart > svg > g.dxc-labels-group > g > g > g > text")
	private WebElement totalCostOneCategory;

	// chronic Total Cost Percentage
	@FindBy(css = "#episode-types-piechart > svg > g.dxc-labels-group > g > g:nth-child(2) > g > text")
	private WebElement ChronicTotalCostPercent;

	// Procedural Total Cost Percentage
	@FindBy(css = "#episode-types-piechart > svg > g.dxc-labels-group > g > g:nth-child(1) > g > text")
	private WebElement proceduralTotalCostPercent;

	// Other Total Cost Percentage
	@FindBy(css = "#episode-types-piechart > svg > g.dxc-labels-group > g > g:nth-child(3) > g > text")
	private WebElement otherTotalCostPercent;

	// Acute Total Cost Percentage
	@FindBy(css = "#episode-types-piechart > svg > g.dxc-labels-group > g > g:nth-child(4) > g > text")
	private WebElement acuteTotalCostPercent;

	// SRF Total Cost Percentage
	@FindBy(css = "#episode-types-piechart > svg > g.dxc-labels-group > g > g:nth-child(5) > g > text")
	private WebElement srfTotalCostPercent;

	/////// Total Costs Donut Ends/////////////

	//////// Member Count Donut Starts//////
	// Member count donut
	@FindBy(xpath = "//*[@id=\"member-counts-piechart\"]/svg/rect")
	private WebElement memberCountsDonut;

	// Only one category selected
	@FindBy(css = "#member-counts-piechart > svg > g.dxc-labels-group > g > g > g > text")
	private WebElement totalMemberOneCategory;

	// Chronic Total Member Counts Percentage
	@FindBy(css = "#member-counts-piechart > svg > g.dxc-labels-group > g > g:nth-child(2) > g > text")
	private WebElement chronicMemberCountPercent;

	// Procedural Total Member Counts Perentage
	@FindBy(css = "#member-counts-piechart > svg > g.dxc-labels-group > g > g:nth-child(1) > g > text")
	private WebElement proceduralMemberCountPercent;

	// Acute Total Member Counts Percentage
	@FindBy(css = "#member-counts-piechart > svg > g.dxc-labels-group > g > g:nth-child(4) > g > text")
	private WebElement acuteMemberCountPercent;

	// Other Total Member Counts Percentage
	@FindBy(css = "#member-counts-piechart > svg > g.dxc-labels-group > g > g:nth-child(3) > g > text")
	private WebElement otherMemberCountPercent;

	// SRF Total Member Count Percentage
	@FindBy(css = "#member-counts-piechart > svg > g.dxc-labels-group > g > g:nth-child(5) > g > text")
	private WebElement srfMemberCountPercent;

	//////// Member Count Donut Ends//////////////

	// Cost List Table Starts/////////////////

	// Entire Cost List Table
	@FindBy(xpath = "//*[@id=\"graphical-container\"]/div[1]/div[2]/ul")
	private WebElement costsListTable;

	@FindBy(xpath = "//*[@id=\"graphical-container\"]/div[1]/div[2]/ul/li[1]")
	private WebElement typicalCostFromList;

	@FindBy(xpath = "//*[@id=\"graphical-container\"]/div[1]/div[2]/ul/li[2]")
	private WebElement typicalWithCompCostFromList;

	@FindBy(xpath = "//*[@id=\"graphical-container\"]/div[1]/div[2]/ul/li[3]")
	private WebElement pacCostFromList;

	@FindBy(xpath = "//*[@id=\"graphical-container\"]/div[1]/div[2]/ul/li[4]")
	private WebElement totalCostFromList;

	//////// Cost List Table Ends/////////////////

	/// Tabular form data Starts///
	// Episode Search box for Table
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[4]/div/div/div[3]/div[2]/div/div/div/input")
	private WebElement searchEpisodesFromTable;

	// Table Export to Excel
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[4]/div/div/div[3]/div[1]/div/div/div/i")
	private WebElement exportToExcelIcon;

	// Table Header values
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]")
	private WebElement tableHeader;

	// Episode Column in table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[1]")
	private WebElement episodeTableHeader;

	// Category Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[2]")
	private WebElement categoryTableHeader;

	// Members Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[3]")
	private WebElement memberTableHeader;

	// Providers Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[4]")
	private WebElement providersTableHeader;

	// Average Typical Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[5]")
	private WebElement averageTypicalTableHeader;

	// Average Comp Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[6]")
	private WebElement averageCompTableHeader;

	// Average PAC Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[7]")
	private WebElement averagePacTableHeader;

	// Average Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[8]")
	private WebElement averageTableHeader;
	
	//Risk Average Column in Table header
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[9]")
	private WebElement riskAverageTableHeader;

	// Total Typical Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[10]")
	private WebElement totalTypicalTableHeader;

	// Total Comp Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[11]")
	private WebElement totalCompTableHeader;

	// Total PAC Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[12]")
	private WebElement totalPacTableHeader;

	// Total Column in Table header
	@FindBy(xpath = "//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[13]")
	private WebElement totalTableHeader;
	
	//Risk Total Column Header
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[5]/div/table/tbody/tr[1]/td[14]")
	private WebElement riskTotalTableHeader;
	
	
	


	public int getTimeoutValue() {
		return timeoutValue;
	}


	public void setTimeoutValue(int timeoutValue) {
		this.timeoutValue = timeoutValue;
	}


	//Total Number of Episodes in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[1]/div")
	private WebElement tableTotalNumberOfEpisodes;
	
	//Total Number of Members in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[3]/div")
	private WebElement tableTotalNumberOfMembers;
	
	//Total Number of Providers in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[4]/div")
	private WebElement tableTotalNumberOfProviders;
	
	//Total of Average Typical Cost in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[5]/div")
	private WebElement tableTotalAverageTypical;
	
	//Total Average Comp cost in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[6]/div")
	private WebElement tableTotalAverageComp;
	
	//Total Average PAC cost in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[7]/div")
	private WebElement tableTotalAveragePac;
	
	//Total Average cost in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[8]/div")
	private WebElement tableTotalAverage;
	
	//Total Risk Average Cost in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[9]/div")
	private WebElement tableTotalRiskAverage;
	
	//Total Typical Cost in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[10]/div")
	private WebElement tableTotalTypical;
	
	//Total Total Comp cost in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[11]/div")
	private WebElement tableTotalComp;
	
	//Total Total PAC Cost in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[12]/div")
	private WebElement tableTotalPac;
	
	//Total Total Cost in Table
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[13]/div")
	//@FindBy(css="#costs-datagrid > div > div.dx-datagrid-total-footer.dx-datagrid-nowrap > div > table > tbody > tr > td:nth-child(13) > div")
	private WebElement tableTotalCost;
	
	//Total Risk Total Cost in Table
		@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[8]/div/table/tbody/tr/td[14]/div")
		private WebElement tableTotalRisk;
	
		
	//Navigate to Sub page by clicking on bar
	@FindBy(css="")
	private WebElement firstEpisodeFromBarChart;
	//Navigate to Sub page by clicking on the first episode
	//@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[6]/div/div[1]/div/table/tbody/tr[1]/td[1]/a")
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div/div[6]/div/div[1]/div[1]/div/table/tbody/tr[1]/td[1]/a")
	private WebElement firstEpisodeFromTable;
	
	
	// Creating constructor
	
	public int timeoutValue = 30;
	public EpisodeOpportunityMainPage(WebDriver driver) {
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, timeoutValue), this);
	}
	
}