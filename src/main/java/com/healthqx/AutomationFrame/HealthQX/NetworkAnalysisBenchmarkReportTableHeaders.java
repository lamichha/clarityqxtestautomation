package com.healthqx.AutomationFrame.HealthQX;

import java.math.BigDecimal;

public class NetworkAnalysisBenchmarkReportTableHeaders {

	private String practice ;
	private BigDecimal avergaePreTrigger;
	private BigDecimal averageTrigger;
	private BigDecimal averagePostTrigger;
	private BigDecimal averageCost;
	
	public String getPractice() {
		return practice;
	}
	public void setPractice(String practice) {
		this.practice = practice;
	}
	public BigDecimal getAvergaePreTrigger() {
		return avergaePreTrigger;
	}
	public void setAvergaePreTrigger(BigDecimal avergaePreTrigger) {
		this.avergaePreTrigger = avergaePreTrigger;
	}
	public BigDecimal getAverageTrigger() {
		return averageTrigger;
	}
	public void setAverageTrigger(BigDecimal averageTrigger) {
		this.averageTrigger = averageTrigger;
	}
	public BigDecimal getAveragePostTrigger() {
		return averagePostTrigger;
	}
	public void setAveragePostTrigger(BigDecimal averagePostTrigger) {
		this.averagePostTrigger = averagePostTrigger;
	}
	public BigDecimal getAverageCost() {
		return averageCost;
	}
	public void setAverageCost(BigDecimal averageCost) {
		this.averageCost = averageCost;
	}
	
	
	
}
