/**
 * 
 */
package com.healthqx.AutomationFrame.HealthQX;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Ashish L.
 *
 */
public class NavigationHeader {

	WebDriver driver;
	
	//HealthQX logo, helps user navigate to the main page from any pages,  by clicking on it
	@FindBy(id="btn-home")
	//@FindBy(xpath="//*[@id=\"blue-header\"]/nav[1]/div[1]/div[1]/a[1]/img[1]"); //New logo with CHC
	private WebElement healthQXLogo;
	
	//Complete Navigation Bar
	@FindBy(xpath="//DIV[@id='navbar']/self::DIV")
	private WebElement navigationBar;
	
	//Strategic Insights Drop-down
	@FindBy(id="lnk-strategic-insights")
	////A[@href='#']/self::A
	private WebElement strategicInsights;
	
	//Episode Opportunity from Strategic Insights drop-down
	//@FindBy(xpath="//A[@data-track-filters='true'][text()='Episode Opportunity']/self::A")
	@FindBy(id="lnk-episode-opportunity")
	private WebElement episodeOpportunity;
	
	//Network Analysis from drop-down
	//@FindBy(xpath="//A[@id='network-analysis-link']/self::A")
	@FindBy(id="lnk-network-analysis")
	private WebElement networkAnalysis;
	
	//Episode Configuration Drop-down
	//@FindBy(xpath="//*[@id=\\\"navbar\\\"]/ul[1]/li[2]/a")
	//@FindBy(xpath="((//A[@href='#'])[1]/../..//A[@href='#'])[2]")
	@FindBy(id="lnk-episode-configuration")
	private WebElement episodeConfiguration;
	
	//HCI3 Datasets from Drop-down
	//@FindBy(xpath="//A[@data-track-filters='true'][text()='HCi3 Datasets']/self::A")
	@FindBy(id="lnk-hci3-datasets")
	private WebElement hci3DataSets;
	
	//Configuration Sets from Drop-down
	//@FindBy(xpath="//A[@data-track-filters='true'][text()='Configuration Sets']/self::A")
	@FindBy(id="lnk-configuration-sets")
	private WebElement configurationSets;
	
	//Budgets & Reconciliation ****Need to populate locator
	//@FindBy(xpath="//A[@href='#'][contains(text(),'Budgets ')]/self::A")
	@FindBy(id="lnk-budget-reconciliation")
	private WebElement budgetsReconciliation ;
	
	//Contract Overview from Drop-down
	//@FindBy(xpath="//A[@data-track-filters='true'][text()='Contract Overview']/self::A")
	@FindBy(id="lnk-contract-overview")
	private WebElement contractOverview;

	//Search Box to search for a practice using Practice Name or TIN#
	//@FindBy(xpath="//INPUT[@type='text']/self::INPUT")
	@FindBy(id="practice-search")
	private WebElement searchBox;
	
	//Search button, Upon click search is triggered
	//@FindBy(xpath="//BUTTON[@type='submit'][text()='Search']/self::BUTTON")
	@FindBy(id="btn-search")
	private WebElement searchButton;
	
	//My HealtdQX icon -- Upon click navigates user to the user profile
	//@FindBy(xpath="//SPAN[@class='glyphicon glyphicon-user']/self::SPAN")
	@FindBy(id="btn-my-healthqx")
	private WebElement myHealthQX;
	
	//Administration icon -- Once clicked navigates user to the Administraion page
	//@FindBy(xpath="//SPAN[@class='glyphicon glyphicon-cog']/self::SPAN")
	@FindBy(id="btn-administration")
	private WebElement administration;
	
	//Log out button
	@FindBy(id="btn-logout")
	private WebElement logOut;
	
	//Help link -- upon click triggers another page open
	//@FindBy(xpath="//SPAN[@class='glyphicon glyphicon-question-sign']/self::SPAN")
	@FindBy(id="btn-help")
	private WebElement help;
	
///// Bread Crumbs Starts//////
	// Complete Bread crumb trail
	//@FindBy(xpath = "//OL[@id='navigation-breadcrumbs']/self::OL")
	@FindBy(id="navigation-breadcrumbs")
	private WebElement completeBreadCrumb;

	// Home breadcrumb
	//@FindBy(xpath = "//A[@id='home']/self::A")
	@FindBy(xpath = "//*[@id=\"navigation-breadcrumbs\"]/li[1]")
	private WebElement homeBreadCrumb;

	// Strategic Insights Bread Crumb
	//@FindBy(xpath = "//A[@id='strategic-insights']/self::A")
	@FindBy(xpath = "//*[@id=\"navigation-breadcrumbs\"]/li[2]")
	private WebElement mainModuleBreadCrumb;

	// Episode Opportunity main page BreadCrumb
	//@FindBy(xpath = "//LI[text()='Episode Opportunity']/self::LI")
	@FindBy(xpath = "//*[@id=\"navigation-breadcrumbs\"]/li[3]")
	private WebElement subModuleBreadCrumb;
	
	//Sub Page Bread crumb
	@FindBy(xpath="//*[@id=\"navigation-breadcrumbs\"]/li[4]")
	private WebElement subPageBreadCrumb;
	
	//SubPage episodeName breadCrumb
	@FindBy(xpath="//*[@id=\"navigation-breadcrumbs\"]/li[5]")
	private WebElement subPageEpisodeNameBreadCrumb;
	
	
	///////// Bread Crumbs Ends/////////
	
//////Study Period & BarChart-Tabular-Expand quick links//////
	//@FindBy(xpath = "//SPAN[@id='corporate-entity-title']/self::SPAN")
	@FindBy(id="corporate-entity-title")
	private WebElement studyPeriodCorporateEntity;

	//@FindBy(xpath = "//SPAN[@id='study-period-title']/self::SPAN")
	@FindBy(id="study-period-title")
	private WebElement studyPeriodTitle;

	//@FindBy(xpath = "//I[@class='fa fa-bar-chart']/self::I")
	//@FindBy(xpath="//*[@id=\"content-actions\"]/li[4]/a/i")
	@FindBy(id="icon-graph")
	private WebElement barChartQuickLink;

	//@FindBy(xpath = "//I[@class='fa fa-table']/self::I")
	//@FindBy(xpath = "//*[@id=\"content-actions\"]/li[3]/a/i")
	@FindBy(id="icon-table")
	private WebElement tableQuickLink;

	
	//@FindBy(xpath = "//*[@id=\"content-actions\"]/li[2]/a/i")
	//@FindBy(xpath="//*[@id=\"content-actions\"]/li[4]/a[1]/i[1]")
	//@FindBy(xpath="//ol[@id='content-actions']//a[@title='Expand']/i")
	@FindBy(id="icon-expand")
	private WebElement expandQuickLink;

	////////// Study Period & BarChart/Tabular/Expand quick links Ends//////
	
	
	
	
	//Creating constructor
	//public int timeoutValue = 30;
	public NavigationHeader(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
		//PageFactory.initElements(new AjaxElementLocatorFactory(driver, timeoutValue), this);
	}
	
	/*
	 * public int timeoutValue = 30;
	public EpisodeOpportunityMainPage(WebDriver driver) {
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, timeoutValue), this);
	}
	
	 */
	
	
	
	// 
		
		
		//Method to navigate to Episode Opportunity page from dropdown
		
		public void navigateToEpisodeOpportunityFromDropDown() {
			NavigationHeader navigation = new NavigationHeader(driver);
		
			
			//navigation.episodeOpportunityTile.click();
			
			for (int i=0 ;i<=2;i++){
				try{
					navigation.getStrategicInsights().click();
					navigation.getEpisodeOpportunity().click();
				break;
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
				}
		}
		
		
		//Method to navigate to Network Analysis page from dropdown
		
				public void navigateToNetworkAnalysisFromDropDown() {
					NavigationHeader navigation = new NavigationHeader(driver);
				
					
					
					
					for (int i=0 ;i<=2;i++){
						try{
							navigation.getStrategicInsights().click();
							navigation.getNetworkAnalysis().click();
						break;
						}
						catch(Exception e){
							System.out.println(e.getMessage());
						}
						}
				}

				//Method to navigate to Contract Overview page from dropdown
				
				public void navigateToBudgetsAndReconciliationFromDropDown() {
					NavigationHeader navigation = new NavigationHeader(driver);
					for (int i=0 ;i<=2;i++){
						try{
							navigation.getBudgetsReconciliation().click();
							navigation.getContractOverview().click();
						break;
						}
						catch(Exception e){
							System.out.println(e.getMessage());
						}
						}
				}


		public WebDriver getDriver() {
			return driver;
		}




		public void setDriver(WebDriver driver) {
			this.driver = driver;
		}




		public WebElement getHealthQXLogo() {
			return healthQXLogo;
		}




		public void setHealthQXLogo(WebElement healthQXLogo) {
			this.healthQXLogo = healthQXLogo;
		}




		public WebElement getNavigationBar() {
			return navigationBar;
		}




		public void setNavigationBar(WebElement navigationBar) {
			this.navigationBar = navigationBar;
		}




		public WebElement getStrategicInsights() {
			return strategicInsights;
		}




		public void setStrategicInsights(WebElement strategicInsights) {
			this.strategicInsights = strategicInsights;
		}




		public WebElement getEpisodeOpportunity() {
			return episodeOpportunity;
		}




		public void setEpisodeOpportunity(WebElement episodeOpportunity) {
			this.episodeOpportunity = episodeOpportunity;
		}




		public WebElement getNetworkAnalysis() {
			return networkAnalysis;
		}




		public void setNetworkAnalysis(WebElement networkAnalysis) {
			this.networkAnalysis = networkAnalysis;
		}




		public WebElement getEpisodeConfiguration() {
			return episodeConfiguration;
		}




		public void setEpisodeConfiguration(WebElement episodeConfiguration) {
			this.episodeConfiguration = episodeConfiguration;
		}




		public WebElement getHci3DataSets() {
			return hci3DataSets;
		}




		public void setHci3DataSets(WebElement hci3DataSets) {
			this.hci3DataSets = hci3DataSets;
		}




		public WebElement getConfigurationSets() {
			return configurationSets;
		}




		public void setConfigurationSets(WebElement configurationSets) {
			this.configurationSets = configurationSets;
		}




		public WebElement getBudgetsReconciliation() {
			return budgetsReconciliation;
		}




		public void setBudgetsReconciliation(WebElement budgetsReconciliation) {
			this.budgetsReconciliation = budgetsReconciliation;
		}




		public WebElement getContractOverview() {
			return contractOverview;
		}




		public void setContractOverview(WebElement contractOverview) {
			this.contractOverview = contractOverview;
		}




		public WebElement getSearchBox() {
			return searchBox;
		}




		public void setSearchBox(WebElement searchBox) {
			this.searchBox = searchBox;
		}




		public WebElement getSearchButton() {
			return searchButton;
		}




		public void setSearchButton(WebElement searchButton) {
			this.searchButton = searchButton;
		}




		public WebElement getMyHealthQX() {
			return myHealthQX;
		}




		public void setMyHealthQX(WebElement myHealthQX) {
			this.myHealthQX = myHealthQX;
		}




		public WebElement getAdministration() {
			return administration;
		}




		public void setAdministration(WebElement administration) {
			this.administration = administration;
		}




		public WebElement getLogOut() {
			return logOut;
		}




		public void setLogOut(WebElement logOut) {
			this.logOut = logOut;
		}




		public WebElement getHelp() {
			return help;
		}




		public void setHelp(WebElement help) {
			this.help = help;
		}




		public WebElement getCompleteBreadCrumb() {
			return completeBreadCrumb;
		}




		public void setCompleteBreadCrumb(WebElement completeBreadCrumb) {
			this.completeBreadCrumb = completeBreadCrumb;
		}




		public WebElement getHomeBreadCrumb() {
			return homeBreadCrumb;
		}




		public void setHomeBreadCrumb(WebElement homeBreadCrumb) {
			this.homeBreadCrumb = homeBreadCrumb;
		}




		public WebElement getMainModuleBreadCrumb() {
			return mainModuleBreadCrumb;
		}




		public void setMainModuleBreadCrumb(WebElement mainModuleBreadCrumb) {
			this.mainModuleBreadCrumb = mainModuleBreadCrumb;
		}




		public WebElement getSubModuleBreadCrumb() {
			return subModuleBreadCrumb;
		}




		public void setSubModuleBreadCrumb(WebElement subModuleBreadCrumb) {
			this.subModuleBreadCrumb = subModuleBreadCrumb;
		}




		public WebElement getSubPageBreadCrumb() {
			return subPageBreadCrumb;
		}




		public void setSubPageBreadCrumb(WebElement subPageBreadCrumb) {
			this.subPageBreadCrumb = subPageBreadCrumb;
		}




		public WebElement getStudyPeriodCorporateEntity() {
			return studyPeriodCorporateEntity;
		}




		public void setStudyPeriodCorporateEntity(WebElement studyPeriodCorporateEntity) {
			this.studyPeriodCorporateEntity = studyPeriodCorporateEntity;
		}




		public WebElement getStudyPeriodTitle() {
			return studyPeriodTitle;
		}




		public void setStudyPeriodTitle(WebElement studyPeriodTitle) {
			this.studyPeriodTitle = studyPeriodTitle;
		}




		public WebElement getBarChartQuickLink() {
			return barChartQuickLink;
		}




		public void setBarChartQuickLink(WebElement barChartQuickLink) {
			this.barChartQuickLink = barChartQuickLink;
		}




		public WebElement getTableQuickLink() {
			return tableQuickLink;
		}




		public void setTableQuickLink(WebElement tableQuickLink) {
			this.tableQuickLink = tableQuickLink;
		}




		public WebElement getExpandQuickLink() {
			return expandQuickLink;
		}




		public void setExpandQuickLink(WebElement expandQuickLink) {
			this.expandQuickLink = expandQuickLink;
		}

		public WebElement getSubPageEpisodeNameBreadCrumb() {
			return subPageEpisodeNameBreadCrumb;
		}

		public void setSubPageEpisodeNameBreadCrumb(WebElement subPageEpisodeNameBreadCrumb) {
			this.subPageEpisodeNameBreadCrumb = subPageEpisodeNameBreadCrumb;
		}
		
		
	
}
