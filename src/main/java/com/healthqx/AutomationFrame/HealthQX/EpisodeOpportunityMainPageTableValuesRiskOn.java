package com.healthqx.AutomationFrame.HealthQX;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EpisodeOpportunityMainPageTableValuesRiskOn {
	WebDriver driver;
	WebElement element;
	
	//@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div[1]/div[6]/div[1]/div[1]/div[1]/table[1]/tbody") private WebElement allTableData;
	@FindBy(xpath="//*[@id=\"costs-datagrid\"]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody") private WebElement allTableData;
	public List<EpisodeOpportunityTableHeaders> rowValues(){
		List<EpisodeOpportunityTableHeaders> rowList = new ArrayList<EpisodeOpportunityTableHeaders>();
		List<WebElement> tableData = allTableData.findElements(By.tagName("tr"));
		
		for(int rNum=0; rNum<tableData.size()-1 ; rNum++) {
			List<WebElement> data = tableData.get(rNum).findElements(By.tagName("td"));
			EpisodeOpportunityTableHeaders header = new EpisodeOpportunityTableHeaders();
			
			if(!data.get(0).getAttribute("innerText").replaceAll("[$,]", "").trim().contains("'")) {
				header.setEpisodeName(data.get(0).getAttribute("innerText").replaceAll("[$,]", "").trim());
			}
			else {
				
				header.setEpisodeName(data.get(0).getAttribute("innerText").replaceAll("[$,]", "").replaceAll("'", "''").trim());
			}
			header.setEpisodeCategory(data.get(1).getAttribute("innerText").replaceAll("[$,]", "".trim()));
			header.setMemberCount(new BigDecimal(data.get(2).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setProviderCount(new BigDecimal(data.get(3).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setAverageTypicalCost(new BigDecimal(data.get(4).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setAverageTypicalComplicationCost(new BigDecimal(data.get(5).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setAveragePacCost(new BigDecimal(data.get(6).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setAverageCost(new BigDecimal(data.get(7).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			
			if(!data.get(8).getAttribute("innerText").replaceAll("[$,]", "").trim().contains("N/A")) {
			header.setRiskAdjustedAverageCost(new BigDecimal(data.get(8).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			}
			else {
				header.setRiskAdjustedAverageCost(new BigDecimal(0.00));
			}
			
			header.setTotalTypicalCost(new BigDecimal(data.get(9).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setTotalTypicalComplicationCost(new BigDecimal(data.get(10).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setTotalPacCost(new BigDecimal(data.get(11).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			header.setTotalCost(new BigDecimal(data.get(12).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			if(!data.get(13).getAttribute("innerText").replaceAll("[$,]", "").trim().contains("N/A")) {
				header.setRiskAdjustedTotalCost(new BigDecimal(data.get(13).getAttribute("innerText").replaceAll("[$,]", "").trim()));
			}
			else {
				
				header.setRiskAdjustedTotalCost(new BigDecimal(0.00));
			}
			
			
			rowList.add(header);
		}
		
		
		
		
		
		return rowList;
		
	}
	
	
	

	public WebElement getAllTableData() {
		return allTableData;
	}

	public void setAllTableData(WebElement allTableData) {
		this.allTableData = allTableData;
	}
	
	public EpisodeOpportunityMainPageTableValuesRiskOn(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
}
