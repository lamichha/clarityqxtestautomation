package com.healthqx.AutomationFrame.HealthQX;

import java.math.BigDecimal;

public class NetworkAnalysisCostBreakDownTableHeaders {

	private String ccsGroup;
	private String ccsName;
	private String claimType;
	private String placeOfService;
	
	private BigDecimal averagePreTrigger;
	private BigDecimal  preTriggerPacPercent;
	private BigDecimal  averageTrigger;
	private BigDecimal  triggerPacPercent;
	private BigDecimal  averagePostTrigger;
	private BigDecimal  postTriggerPacPercent;
	private BigDecimal  averageCost;
	private BigDecimal  pacPercent;
	public String getCcsGroup() {
		return ccsGroup;
	}
	public void setCcsGroup(String ccsGroup) {
		this.ccsGroup = ccsGroup;
	}
	public String getCcsName() {
		return ccsName;
	}
	public void setCcsName(String ccsName) {
		this.ccsName = ccsName;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getPlaceOfService() {
		return placeOfService;
	}
	public void setPlaceOfService(String placeOfService) {
		this.placeOfService = placeOfService;
	}
	public BigDecimal getAveragePreTrigger() {
		return averagePreTrigger;
	}
	public void setAveragePreTrigger(BigDecimal averagePreTrigger) {
		this.averagePreTrigger = averagePreTrigger;
	}
	public BigDecimal getPreTriggerPacPercent() {
		return preTriggerPacPercent;
	}
	public void setPreTriggerPacPercent(BigDecimal preTriggerPacPercent) {
		this.preTriggerPacPercent = preTriggerPacPercent;
	}
	public BigDecimal getAverageTrigger() {
		return averageTrigger;
	}
	public void setAverageTrigger(BigDecimal averageTrigger) {
		this.averageTrigger = averageTrigger;
	}
	public BigDecimal getTriggerPacPercent() {
		return triggerPacPercent;
	}
	public void setTriggerPacPercent(BigDecimal triggerPacPercent) {
		this.triggerPacPercent = triggerPacPercent;
	}
	public BigDecimal getAveragePostTrigger() {
		return averagePostTrigger;
	}
	public void setAveragePostTrigger(BigDecimal averagePostTrigger) {
		this.averagePostTrigger = averagePostTrigger;
	}
	public BigDecimal getPostTriggerPacPercent() {
		return postTriggerPacPercent;
	}
	public void setPostTriggerPacPercent(BigDecimal postTriggerPac) {
		this.postTriggerPacPercent = postTriggerPac;
	}
	public BigDecimal getAverageCost() {
		return averageCost;
	}
	public void setAverageCost(BigDecimal averageCost) {
		this.averageCost = averageCost;
	}
	public BigDecimal getPacPercent() {
		return pacPercent;
	}
	public void setPacPercent(BigDecimal pacPercentage) {
		pacPercent = pacPercentage;
	}
}
